﻿using GlobalPortal.Api.Client.Patients;
using GlobalPortal.Api.Models.Patients;
using MVE.PatientPortal.API.Models;
using MVE.PatientPortal.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GlobalPortal.Api.Models.AppointmentsModel;
using GlobalPortal.Api.Client.Appointments;

namespace MVE.PatientPortal.API.Controllers
{
    public class AppointmentsController : ApiController
    {
        [HttpPost]
        public string New(AppointmentEntity objAppointment)
        {
            string retStatus = "0";
            PortalQueue pq = new PortalQueue();
            AppointmentModel model = GenerateModels.GenerateAppointmentModel(objAppointment);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objAppointment.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(80);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objAppointment.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objAppointment.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objAppointment.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = AppointmentProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success.ExternalId != null)
                    {
                        retStatus = EnumTypes.ResponseStatus.Updated.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        retStatus = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    retStatus = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                retStatus = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return retStatus;
        }

        [HttpPost]
        public string Update(AppointmentEntity objAppointment)
        {
            string retStatus = "0";
            PortalQueue pq = new PortalQueue();
            AppointmentModel model = GenerateModels.GenerateAppointmentModel(objAppointment);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objAppointment.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(84);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objAppointment.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objAppointment.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objAppointment.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = AppointmentProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success.ExternalId != null)
                    {
                        retStatus = EnumTypes.ResponseStatus.Updated.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        retStatus = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    retStatus = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                retStatus = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return retStatus;
        }

        [HttpPost]
        public string Delete(AppointmentEntity objAppointment)
        {
            string retStatus = "0";
            PortalQueue pq = new PortalQueue();
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(objAppointment);
            pq.PracticeToken = objAppointment.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(81);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objAppointment.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objAppointment.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objAppointment.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = AppointmentProvider.Delete(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), objAppointment.ExternalId);
                    if (success.ToString().ToLower() == "true")
                    {
                        retStatus = EnumTypes.ResponseStatus.Updated.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        retStatus = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    retStatus = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                retStatus = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return retStatus;
        }
    }
}
