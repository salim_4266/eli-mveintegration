﻿using System;
using System.Linq;
using System.Web.Http;
using MVE.PatientPortal.API.Models;
using MVE.PatientPortal.API.Utilities;
using GlobalPortal.Api.Models.Patients;
using GlobalPortal.Api.Client.Patients;
using GlobalPortal.Api.Client.ExternalHealthInformationLinks;
using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.ExternalHealthInformationLinks;
using GlobalPortal.Api.Models.PatientEducationMaterials;
using GlobalPortal.Api.Client.PatientEducationMaterials;
using GlobalPortal.Api.Client.PatientSharedFiles;
using GlobalPortal.Api.Models.Patients.PatientSharedFiles;

namespace MVE.PatientPortal.API.Controllers
{
    public class PatientController : ApiController
    {
        [HttpPost]
        public ResponseEntity Register(PatientEntity objPatient)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            PortalQueue pq = new PortalQueue();
            objPatient.UserName = objPatient.LastName + objPatient.FirstName + objPatient.ExternalId;
            objPatient.Password = Guid.NewGuid().ToString().Substring(1, 10);
            //objPatient.UserName = objPatient.UserName;
            //objPatient.Password = objPatient.Password;
            PatientModel model = GenerateModels.GeneratePatientModel(objPatient, true);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objPatient.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(97);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatient.ExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatient.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatient.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = PatientProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null)
                    {
                        objResponseEntity.ExternalId = success.Id.ToString();
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();

                        PatientEmailEntity objPatientEmail = new PatientEmailEntity()
                        {
                            PracticeToken = objPatient.PracticeToken,
                            PatientEmailAddress = objPatient.EmailAddresses,
                            PatientName = objPatient.FirstName + " " + objPatient.LastName,
                            UserName = objPatient.UserName,
                            PracticeERP = OauthToken.PracticeAccountNumber,
                            Password = objPatient.Password,
                            PracticeDisplayName = OauthToken.PracticeDisplayName
                        };

                        EmailUtility objemailUtility = new EmailUtility();
                        objemailUtility.SendEmailViaAPI(objPatientEmail, pq.PatientId);

                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity New(PatientEntity objPatient)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            PortalQueue pq = new PortalQueue();
            PatientModel model = GenerateModels.GeneratePatientModel(objPatient, true);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objPatient.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(97);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatient.ExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatient.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatient.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = PatientProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null)
                    {
                        objResponseEntity.ExternalId = success.Id.ToString();
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();

                        PatientEmailEntity objPatientEmail = new PatientEmailEntity()
                        {
                            PracticeToken = objPatient.PracticeToken,
                            PatientEmailAddress = objPatient.EmailAddresses,
                            PatientName = objPatient.FirstName + " " + objPatient.LastName,
                            UserName = objPatient.UserName,
                            PracticeERP = OauthToken.PracticeAccountNumber,
                            Password = objPatient.Password,
                            PracticeDisplayName = OauthToken.PracticeDisplayName
                        };

                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity SendCredentials(CredentialEntity objCredentials)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            try
            {
                EmailUtility objEmailUtility = new EmailUtility();
                bool retStatus = objEmailUtility.SendCredentials(objCredentials);
                if (retStatus)
                {
                    objResponseEntity.ExternalId = objCredentials.ExternalId;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();
                }
                else
                {
                    objResponseEntity.ExternalId = objCredentials.ExternalId;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                }
            }
            catch 
            {
                objResponseEntity.ExternalId = objCredentials.ExternalId;
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity EmailCredentials(EmailEntity objEmailEntity)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            try
            {
                PatientEmailEntity objPatientEmailEntity = new PatientEmailEntity(objEmailEntity);
                bool retStatus = new EmailUtility().SendCredentials(objPatientEmailEntity);
                if (retStatus)
                {
                    objResponseEntity.ExternalId = "Done";
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();
                }
                else                                                                          
                {
                    objResponseEntity.ExternalId = "Failed";
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                }
            }
            catch
            {
                objResponseEntity.ExternalId = "";
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity UpdateDemographics(PatientEntity objPatient)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            string retStatus = "0";
            PortalQueue pq = new PortalQueue();
            PatientModel model = GenerateModels.GeneratePatientModel(objPatient, false);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objPatient.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(97);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatient.ExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatient.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatient.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = PatientProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null)
                    {
                        objResponseEntity.ExternalId = success.Id.ToString();
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Updated.ToString();

                        #region Send Email on Update
                        if (objPatient.IsSendEmailViaAPI)
                        {
                            PatientEmailEntity objPatientEmail = new PatientEmailEntity()
                            {
                                PracticeToken = objPatient.PracticeToken,
                                PatientEmailAddress = objPatient.EmailAddresses,
                                PatientName = objPatient.FirstName + " " + objPatient.LastName,
                                UserName = objPatient.UserName,
                                PracticeERP = OauthToken.PracticeAccountNumber,
                                Password = objPatient.Password,
                                PracticeDisplayName = OauthToken.PracticeDisplayName
                            };

                            EmailUtility objemailUtility = new EmailUtility();
                            objemailUtility.SendEmailViaAPI(objPatientEmail, pq.PatientId); 
                        }
                        Utility.InsertDataIntoPortalQueueResponse(pq); 
                        #endregion

                        if (objPatient.RepersentativeDetail != null && objPatient.RepersentativeDetail.RepresentativeId != "")
                        {
                            if (objPatient.RepersentativeDetail.UserName != "" && objPatient.RepersentativeDetail.Password != "")
                            {
                                PatientEmailEntity objPatientEmails = new PatientEmailEntity()
                                {
                                    PracticeToken = objPatient.PracticeToken,
                                    PatientEmailAddress = objPatient.RepersentativeDetail.Email,
                                    PatientName = objPatient.RepersentativeDetail.FirstName + " " + objPatient.RepersentativeDetail.LastName,
                                    UserName = objPatient.RepersentativeDetail.UserName,
                                    PracticeERP = OauthToken.PracticeAccountNumber,
                                    Password = objPatient.RepersentativeDetail.Password,
                                    PracticeDisplayName = OauthToken.PracticeDisplayName
                                };
                                pq.PatientId = objPatient.ExternalId;
                                pq.ExternalId = objPatient.RepersentativeDetail.RepresentativeId;
                                pq.ActionTypeLookupId = 94;
                                pq.MessageData = Newtonsoft.Json.JsonConvert.SerializeObject(objPatientEmails);
                                Utility.InsertDataIntoPortalQueueResponse(pq);
                            }
                        }
                    }
                    else
                    {
                        retStatus = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    retStatus = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                retStatus = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseContentEntity ExternalLink(PatientLinkEntity objPatient)
        {
            ResponseContentEntity objResponseEntity = new ResponseContentEntity();
            PortalQueue pq = new PortalQueue();
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(objPatient);
            pq.PracticeToken = objPatient.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(135);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatient.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatient.PatientExternalId;
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatient.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    ListModel<ExternalHealthInformationLinkModel> Model = ExternalHealthInformationLinkProvider.Search(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), objPatient.PatientExternalId, null, 1, 10);
                    if (Model.Rows.Count() != 0)
                    {
                        objResponseEntity = GenerateModels.GenerateResponseModelExternalLink(Model, EnumTypes.ResponseStatus.Success.ToString());
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        pq.Exception = "Model is null";
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseContentEntity SharedFiles(PatientLinkEntity objPatient)
        {
            ResponseContentEntity objResponseEntity = new ResponseContentEntity();
            PortalQueue pq = new PortalQueue();
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(objPatient);
            pq.PracticeToken = objPatient.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(136);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatient.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatient.PatientExternalId;
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatient.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    ListModel<PatientSharedFileModel> Model = PatientSharedFileProvider.Search(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), objPatient.PatientExternalId, "", null, null, "", "", "", "", false, 1, 20);
                    if (Model.Rows.Count() != 0)
                    {
                        objResponseEntity = GenerateModels.GenerateResponseModelSharedFiles(Model, EnumTypes.ResponseStatus.Success.ToString());
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        pq.Exception = "Model is null";
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseContentEntity DownloadFile(PatientPGHDFileId objPatient)
        {
            ResponseContentEntity objResponseEntity = new ResponseContentEntity();
            PortalQueue pq = new PortalQueue();
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(objPatient);
            pq.PracticeToken = objPatient.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(137);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatient.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatient.ExternalId;
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatient.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    FileModel Model = PatientSharedFileProvider.GetFile(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), objPatient.ExternalId);
                    if (Model.Content != null)
                    {
                        objResponseEntity = GenerateModels.GenerateResponseModelFileDetail(Model, EnumTypes.ResponseStatus.Success.ToString());
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        pq.Exception = "Model is null";
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = "Username and Password is Invalid";
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity EducationMaterial(PatientEducationMaterial objPatientEducationMaterial)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            PortalQueue pq = new PortalQueue();
            PatientEducationMaterialModel model = GenerateModels.GeneratePatientEducationModel(objPatientEducationMaterial);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objPatientEducationMaterial.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendPatientEducationMaterial);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objPatientEducationMaterial.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objPatientEducationMaterial.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objPatientEducationMaterial.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = PatientEducationMaterialProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null)
                    {
                        objResponseEntity.ExternalId = success.Id.ToString();
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity FileRecieved(PatientPGHDFileId objContent)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            PortalQueue pq = new PortalQueue();
            IdModel model = GenerateModels.GenerateModelForRecievedFile(objContent);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objContent.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(135);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objContent.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objContent.ExternalId;
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objContent.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = PatientSharedFileProvider.MarkAsReceived(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null)
                    {
                        objResponseEntity.ExternalId = success.Id.ToString();
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }

        [HttpPost]
        public ResponseEntity DeleteFile(PatientPGHDFileId objContent)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            PortalQueue pq = new PortalQueue();
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(objContent);
            pq.PracticeToken = objContent.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(138);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objContent.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objContent.ExternalId;
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objContent.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = PatientSharedFileProvider.Delete(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), objContent.ExternalId);
                    if (success)
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();
                    }
                    else
                    {
                        objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    objResponseEntity.ResponseType = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
            }
            catch (Exception ex)
            {
                objResponseEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResponseEntity;
        }
    }
}
