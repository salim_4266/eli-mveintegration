﻿using GlobalPortal.Api.Client.Hl7CDAs;
using GlobalPortal.Api.Models.Hl7CDAs;
using MVE.PatientPortal.API.Models;
using MVE.PatientPortal.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MVE.PatientPortal.API.Controllers
{
    public class CDAMessageController : ApiController
    {
        [HttpPost]
        public string UpdateHealthSummary(PatientHealthSummary objHealthSummary)
        {
            string retStatus = "0";
            PortalQueue pq = new PortalQueue();
            Hl7CDAModel model = GenerateModels.GenerateHealthSummaryModel(objHealthSummary);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objHealthSummary.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(19);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objHealthSummary.PatientExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objHealthSummary.PatientExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objHealthSummary.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = Hl7CDAProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null && success.Id != null)
                    {
                        pq.ExternalId = success.Id.ToString();
                        retStatus = EnumTypes.ResponseStatus.Success.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        retStatus = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    retStatus = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.ToString().Contains("The patient is invalid External ID"))
                {
                    retStatus = EnumTypes.ResponseStatus.InvalidPatient.ToString();
                }
                else 
                {
                    retStatus = EnumTypes.ResponseStatus.Exception.ToString();
                }
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return retStatus;
        }
    }
}
