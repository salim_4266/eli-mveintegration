﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MVE.PatientPortal.API.Models;
using System.Security.Cryptography;
using System.Security.Claims;
using System.Xml;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace MVE.PatientPortal.API.Controllers
{
    public class ACIController : ApiController
    {
        [HttpPost]
        public ResponseTokenEntity GenerateToken(RequestTokenEntity objToken)
        {
            ResponseTokenEntity objResponseEntity = new ResponseTokenEntity();
            try
            {
                string xmlString = objToken.PairKey;
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048);
                RSAParameters rsaParameters = GetRsaParametersFromXmlString(xmlString);
                rsa.ImportParameters(rsaParameters);
                DateTime now = DateTime.UtcNow;
                Claim[] claims = new[]
                 {
                      new Claim("user_metadata","{\"Username\":\""+objToken.UserName+"\",\"Role\":\""+objToken.Role+"\",\"ProductKey\":\""+objToken.ProductKey+"\"}")
                 };
                JwtSecurityToken jwt = new JwtSecurityToken(
                    issuer: "http://www.emrxxxxxxsample.com",
                    audience: "http://localhost:54288/",
                    //notBefore: now,
                    claims: claims,
                    expires: now.AddMinutes(600), // amount of minutes for token validity
                    signingCredentials: new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256Signature)
                    );
                jwt.Payload["iat"] = now;
                string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                objResponseEntity.TokenDetail = encodedJwt;
                objResponseEntity.TokenStatus = "1";

            }
            catch (Exception ex)
            {
                objResponseEntity.TokenDetail = ex.Message;
                objResponseEntity.TokenStatus = "0";
            }
            return objResponseEntity;
        }

        private static RSAParameters GetRsaParametersFromXmlString(string xmlString)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            XmlNode nodeRsaKeyValue = xmlDoc.DocumentElement;
            XmlNodeList xnList = nodeRsaKeyValue.ChildNodes;
            Dictionary<string, string> dicKeyData = new Dictionary<string, string>();
            foreach (XmlNode node in xnList)
            {
                dicKeyData.Add(node.Name, node.InnerText);
            }
            RSAParameters rsaParameters = new RSAParameters();
            string modulus = dicKeyData["Modulus"];
            string exponent = dicKeyData["Exponent"];
            rsaParameters.Modulus = Convert.FromBase64String(dicKeyData["Modulus"]);
            rsaParameters.Exponent = Convert.FromBase64String(dicKeyData["Exponent"]);
            rsaParameters.P = Convert.FromBase64String(dicKeyData["P"]);
            rsaParameters.Q = Convert.FromBase64String(dicKeyData["Q"]);
            rsaParameters.DP = Convert.FromBase64String(dicKeyData["DP"]);
            rsaParameters.DQ = Convert.FromBase64String(dicKeyData["DQ"]);
            rsaParameters.InverseQ = Convert.FromBase64String(dicKeyData["InverseQ"]);
            rsaParameters.D = Convert.FromBase64String(dicKeyData["D"]);
            return rsaParameters;
        }
    }
}
