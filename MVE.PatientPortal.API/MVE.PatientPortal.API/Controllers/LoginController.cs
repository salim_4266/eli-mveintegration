﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVE.PatientPortal.API.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection FC)
        {
            string UserName = FC["username"].ToString();
            string password = FC["password"].ToString();
            if (UserName == "portaladmin" && password == "iopracticeware@")
            {
                FormsAuthentication.SetAuthCookie("portaladmin", false);
                return RedirectToAction("Dashboard", "PortalAdmin", null);
            }
            else 
            {
                return RedirectToAction("Index", "Login", null);
            }
        }
    }
}
