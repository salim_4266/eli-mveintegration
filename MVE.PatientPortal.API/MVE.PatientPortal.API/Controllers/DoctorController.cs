﻿using GlobalPortal.Api.Client.Doctors;
using GlobalPortal.Api.Models.Doctors;
using MVE.PatientPortal.API.Models;
using MVE.PatientPortal.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MVE.PatientPortal.API.Controllers
{
    public class DoctorController : ApiController
    {
        [HttpPost]
        public ResponseEntity Post(DoctorEntity objEntity)
        {
            ResponseEntity objResEntity = new ResponseEntity();
            PortalQueue pq = new PortalQueue();
            DoctorModel model = GenerateModels.GenerateDoctorModel(objEntity);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objEntity.PracticeToken;
            pq.ActionTypeLookupId = 12;
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = objEntity.ExternalId;
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objEntity.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objEntity.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = DoctorProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success != null)
                    {
                        objResEntity.ExternalId = success.Id.ToString();
                        objResEntity.ResponseType = EnumTypes.ResponseStatus.Success.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        objResEntity.ResponseType = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
            }
            catch (Exception ex)
            {
                objResEntity.ResponseType = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return objResEntity;
        }
    }
}
