﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVE.PatientPortal.API.Utilities;
using MVE.PatientPortal.API.Models;

namespace MVE.PatientPortal.API.Controllers
{
    [Authorize]
    public class PortalAdminController : Controller
    {
        public ActionResult Dashboard()
        {
            ViewBag.PracticeList = PortalAdmin.LoadPractices();
            return View();
        }

        public ActionResult Practice(string PracticeName, string PracticeToken)
        {
            ViewBag.SelectedPracticeAlias = PracticeName;
            ViewBag.SelectedPracticeToken = PracticeToken;
            ViewBag.PracticeList = PortalAdmin.LoadPractices();
            return View("Dashboard");
        }

        [HttpPost]
        public ActionResult DoctorRegistration(FormCollection FC)
        {
            string PracticeToken = FC["PracticeToken"].ToString();
            string PracticeAliasName = FC["PracticeName"].ToString();
            string DoctorFirstName = FC["DoctorFirstName"].ToString();
            string DoctorLastName = FC["DoctorLastName"].ToString();
            string DoctorExternalId = FC["DoctorExternalId"].ToString();
            string DoctorAliasName = FC["DoctorAliasName"].ToString();
            string LocationExternalId = FC["LocationExternalId"].ToString();

            Doctor objDoctor = new Doctor()
            {
                AliasName = DoctorAliasName,
                ExternalId = DoctorExternalId,
                FirstName = DoctorFirstName,
                LastName = DoctorLastName,
                PracticeToken = PracticeToken,
                LocationExternalId = LocationExternalId
            };

            string RegistartionStatus = PortalAdmin.RegisterDoctor(objDoctor);

            if (RegistartionStatus == "Success")
            {
                ViewBag.Message = "Registration successfully completed";
                ViewBag.regStatus = "1";
            }
            else 
            {
                ViewBag.Message = "There is some issue, Please try after some time";
                ViewBag.regStatus = "0";
            }
            ViewBag.SelectedPracticeAlias = PracticeAliasName;
            ViewBag.SelectedPracticeToken = PracticeToken;
            ViewBag.PracticeList = PortalAdmin.LoadPractices();
            return View("Dashboard");
        }

    }
}