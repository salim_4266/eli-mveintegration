﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UrlRewriting.Controllers
{
    public class PatientPortalController : Controller
    {
        public ActionResult Login()
        {
            string param = string.Empty;
            if (this.Request.QueryString["n"] != null)
            {
                param = this.Request.QueryString["n"];
                string MVE_Id = ConvertMEdflowtoMVE(param);
                if (MVE_Id != "NA")
                {
                    ViewBag.NewURL = "https://portal.eyereachpatients.com/Patients/" + MVE_Id.ToString();
                }
            }
            return View("Index");
        }

        public string ConvertMEdflowtoMVE(string MedflowId)
        {
            string retStatus = string.Empty;
            switch (MedflowId.ToLower())
            {
                case "1002":
                    retStatus = "ERP54";
                    break;
                case "1005":
                    retStatus = "ERP24";
                    break;
                case "1006":
                    retStatus = "ERP46";
                    break;
                case "1007":
                    retStatus = "ERP22";
                    break;
                case "1019":
                    retStatus = "ERP25";
                    break;
                case "1075":
                    retStatus = "ERP87";
                    break;
                case "1011":
                    retStatus = "ERP84";
                    break;
                case "1028":
                    retStatus = "ERP45";
                    break;
                case "1013":
                    retStatus = "ERP26";
                    break;
                case "1015":
                    retStatus = "ERP23";
                    break;
                case "1017":
                    retStatus = "ERP88";
                    break;
                case "1018":
                    retStatus = "ERP57";
                    break;
                case "1024":
                    retStatus = "ERP21";
                    break;
                case "1025":
                    retStatus = "ERP75";
                    break;
                case "1026":
                    retStatus = "ERP70";
                    break;
                case "1027":
                    retStatus = "ERP79";
                    break;
                case "1012":
                    retStatus = "ERP68";
                    break;
                case "1029":
                    retStatus = "ERP65";
                    break;
                case "1031":
                    retStatus = "ERP85";
                    break;
                case "1032":
                    retStatus = "ERP62";
                    break;
                case "1034":
                    retStatus = "ERP43";
                    break;
                case "1036":
                    retStatus = "ERP32";
                    break;
                case "1040":
                    retStatus = "ERP71";
                    break;
                case "1041":
                    retStatus = "ERP41";
                    break;
                case "1045":
                    retStatus = "ERP40";
                    break;
                case "1046":
                    retStatus = "ERP49";
                    break;
                case "1049":
                    retStatus = "ERP48";
                    break;
                case "1054":
                    retStatus = "ERP44";
                    break;
                case "1055":
                    retStatus = "ERP39";
                    break;
                case "1057":
                    retStatus = "ERP47";
                    break;
                case "1058":
                    retStatus = "ERP50";
                    break;
                case "1059":
                    retStatus = "ERP73";
                    break;
                case "1060":
                    retStatus = "ERP51";
                    break;
                case "1063":
                    retStatus = "ERP83";
                    break;
                case "1067":
                    retStatus = "ERP27";
                    break;
                case "1068":
                    retStatus = "ERP67";
                    break;
                case "1071":
                    retStatus = "ERP38";
                    break;
                case "1072":
                    retStatus = "ERP37";
                    break;
                case "1073":
                    retStatus = "ERP42";
                    break;
                case "1074":
                    retStatus = "ERP36";
                    break;
                case "1076":
                    retStatus = "ERP66";
                    break;
                case "1078":
                    retStatus = "ERP28";
                    break;
                case "1079":
                    retStatus = "ERP58";
                    break;
                case "1083":
                    retStatus = "ERP61";
                    break;
                case "1090":
                    retStatus = "ERP35";
                    break;
                case "1093":
                    retStatus = "NA";
                    break;
                case "1094":
                    retStatus = "ERP52";
                    break;
                case "1099":
                    retStatus = "ERP33";
                    break;
                case "1100":
                    retStatus = "ERP63";
                    break;
                case "1101":
                    retStatus = "ERP78";
                    break;
                case "1105":
                    retStatus = "ERP20";
                    break;
                case "1106":
                    retStatus = "ERP69";
                    break;
                case "1108":
                    retStatus = "ERP31";
                    break;
                case "1109":
                    retStatus = "ERP56";
                    break;
                case "1110":
                    retStatus = "ERP55";
                    break;
                case "1113":
                    retStatus = "ERP30";
                    break;
                case "1118":
                    retStatus = "ERP64";
                    break;
                case "1121":
                    retStatus = "ERP77";
                    break;
                case "1123":
                    retStatus = "ERP29";
                    break;
                case "1124":
                    retStatus = "ERP53";
                    break;
                case "1128":
                    retStatus = "ERP72";
                    break;
                case "1129":
                    retStatus = "ERP80";
                    break;
                case "1130":
                    retStatus = "ERP59";
                    break;
                case "1097":
                    retStatus = "ERP97";
                    break;
                case "1122":
                    retStatus = "ERP89";
                    break;
                case "1037":
                    retStatus = "ERP91";
                    break;
                case "1050":
                    retStatus = "ERP94";
                    break;
                case "1084":
                    retStatus = "ERP95";
                    break;
                case "1016":
                    retStatus = "ERP90";
                    break;
                case "1085":
                    retStatus = "ERP98";
                    break;
                case "1020":
                    retStatus = "ERP93";
                    break;
                case "1089":
                    retStatus = "ERP92";
                    break;
                case "1086":
                    retStatus = "ERP96";
                    break;
                case "1038":
                    retStatus = "ERP86";
                    break;
                case "1042":
                    retStatus = "ERP103";
                    break;
                case "1080":
                    retStatus = "ERP104";
                    break;
                case "1082":
                    retStatus = "ERP101";
                    break;
                case "1104":
                    retStatus = "ERP99";
                    break;
                case "1039":
                    retStatus = "ERP107";
                    break;
                case "1047":
                    retStatus = "ERP106";
                    break;
                case "1096":
                    retStatus = "ERP105";
                    break;
                case "1043":
                    retStatus = "ERP100";
                    break;
                case "1127":
                    retStatus = "ERP113";
                    break;
                case "1131":
                    retStatus = "ERP112";
                    break;
                case "1003":
                    retStatus = "ERP128";
                    break;
                case "1111":
                    retStatus = "ERPd51ad40e113c4eb0ae7cb4a104ec43b8";
                    break;
                case "1048":
                    retStatus = "ERP55651bb6baf44cce8d422eb5b06b2bf5";
                    break;
                case "1030":
                    retStatus = "ERP0e5c620da59f4001876a32c9f93fcb0a";
                    break;
                case "1102":
                    retStatus = "ERPdd6acd290cbe4ea19a515531c02cc7b0";
                    break;
                case "1117":
                    retStatus = "ERP114";
                    break;
                case "1061":
                    retStatus = "ERPf8143402f7254642af339af5b9d46cf4";
                    break;
                default:
                    retStatus = "NA";
                    break;
            }
            return retStatus;
        }
    }
}