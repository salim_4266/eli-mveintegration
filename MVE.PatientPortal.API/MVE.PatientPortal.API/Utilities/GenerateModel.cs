﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVE.PatientPortal.API.Models;
using GlobalPortal.Api.Models.Patients;
using GlobalPortal.Api.Models.Contacts;
using GlobalPortal.Api.Models.Hl7CDAs;
using GlobalPortal.Api.Models.AppointmentsModel;
using GlobalPortal.Api.Models.Doctors;
using GlobalPortal.Api.Models.ExternalHealthInformationLinks;
using GlobalPortal.Api.Models.PatientEducationMaterials;
using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.Patients.PatientSharedFiles;
using GlobalPortal.Api.Client.PatientSharedFiles;

namespace MVE.PatientPortal.API.Utilities
{
    public class GenerateModels
    {
        public static PatientModel GeneratePatientModel(PatientEntity objPatient, bool CredentialsFlag)
        {
            PatientModel model = new PatientModel();
            ContactModel contactmodel = new ContactModel();
            EmailAddressModel EAModelitem = new EmailAddressModel();
            PhoneNumberModel PhoneModelNos = new PhoneNumberModel();
            PostalAddressModel PostalAddressitem = new PostalAddressModel();
            List<string> listLocations = new List<string>();
            List<EmailAddressModel> EAModel = new List<EmailAddressModel>();
            List<PhoneNumberModel> PhoneModel = new List<PhoneNumberModel>();
            List<PostalAddressModel> PostalAddress = new List<PostalAddressModel>();
            try
            {
                if (objPatient != null)
                {
                    contactmodel.Id = Guid.NewGuid();
                    contactmodel.FirstName = objPatient.FirstName.ToString();
                    contactmodel.LastName = objPatient.LastName.ToString();
                    contactmodel.MiddleName = objPatient.MiddleName == null ? "" : objPatient.MiddleName;
                    contactmodel.Suffix = objPatient.Suffix == null ? "" : objPatient.Suffix;
                    contactmodel.Prefix = objPatient.Prefix == null ? "" : objPatient.Prefix;
                    contactmodel.FullName = objPatient.FirstName + " " + objPatient.MiddleName == null ? "" : objPatient.MiddleName + " " + objPatient.LastName;
                    contactmodel.CompanyName = "";
                    contactmodel.JobTitle = "";

                    listLocations.Add(objPatient.LocationID.ToString());
                    model.Active = true;

                    EAModelitem.Address = objPatient.EmailAddresses.ToString();
                    EAModelitem.Default = true;
                    EAModel.Add(EAModelitem);
                    model.Contact.EmailAddresses = EAModel;
                    PhoneModelNos.Number = objPatient.PhoneNumbers.ToString();
                    PhoneModel.Add(PhoneModelNos);
                    model.Contact.PhoneNumbers = PhoneModel;
                    if (objPatient.Address1 != null)
                    {
                        PostalAddressitem.Address1 = objPatient.Address1 == null ? "" : objPatient.Address1;
                    }
                    if (objPatient.Address2 != null)
                    {
                        PostalAddressitem.Address2 = objPatient.Address2 == null ? "" : objPatient.Address2;
                    }
                    if (objPatient.City != null)
                    {
                        PostalAddressitem.City = objPatient.City == null ? "" : objPatient.City;
                    }
                    if (objPatient.State != null)
                    {
                        PostalAddressitem.State = objPatient.State == null ? "" : objPatient.State;
                    }
                    if (objPatient.Country != null)
                    {
                        PostalAddressitem.CountryName = objPatient.Country == null ? "" : objPatient.Country;
                    }
                    if (objPatient.Zip != null)
                    {
                        PostalAddressitem.Zip = objPatient.Zip == "" ? "" : objPatient.Zip.Substring(0, 5);
                    }
                    PostalAddressitem.Default = true;
                    PostalAddress.Add(PostalAddressitem);
                    model.Contact.FirstName = objPatient.FirstName;
                    model.Contact.LastName = objPatient.LastName;
                    model.Contact.Id = Guid.NewGuid();
                    model.Contact.PostalAddresses = PostalAddress;
                    model.Birthday = Convert.ToDateTime(objPatient.BirthDate);
                    model.Notes = "";
                    model.Active = true;
                    model.LanguageExternalId = "";
                    model.Locations = listLocations;
                    //model.Contact = contactmodel;
                    model.ExternalId = Convert.ToString(objPatient.ExternalId);
                    model.ContactLensesReorderDate = null;
                    model.ContactLensesReorderName = "";
                    model.RecallDate1 = null;
                    model.RecallDate2 = null;
                    model.AllowEmailReminders = false;
                    model.AllowEmailBirthday = false;
                    model.AllowEmailThankYou = false;
                    model.AllowEmailEyeWear = false;
                    model.AllowEmailPromotions = false;
                    model.AllowSmsReminders = false;
                    model.AllowSmsBirthday = false;
                    model.AllowSmsThankYou = false;
                    model.AllowSmsEyewear = false;
                    model.AllowSmsPromotions = false;
                    model.AllowVoiceReminders = false;
                    model.AllowVoiceBirthday = false;
                    model.AllowVoiceThankYou = false;
                    model.AllowVoiceEyewear = false;
                    model.AllowVoicePromotions = false;
                    model.InvitedToPortal = true;
                    if (CredentialsFlag)
                    {
                        model.NextLogOnSecurityVerificationPatient = true;
                        model.NextLogOnSecurityVerificationRepresentative = true;
                        if (string.IsNullOrEmpty(objPatient.UserName))
                        {
							model.Username = objPatient.LastName + "_" + objPatient.ExternalId;
                        }
                        else
                        {
							model.Username = objPatient.UserName;
						}

						if (string.IsNullOrEmpty(objPatient.Password))
                        {
							model.Password = objPatient.LastName + "_" + objPatient.BirthDate.Replace("-", "");
						}
						else
                        {
							model.Password = objPatient.Password.Replace(" ", "");
                        }
                        if (objPatient.RepersentativeDetail != null)
                        {
                            if (objPatient.RepersentativeDetail.RepresentativeId != "0")
                            {
                                model.Representative.FirstName = objPatient.RepersentativeDetail.FirstName;
                                model.Representative.LastName = objPatient.RepersentativeDetail.LastName;
                                model.Representative.ExternalId = objPatient.RepersentativeDetail.RepresentativeId;
                                model.Representative.Username = objPatient.RepersentativeDetail.UserName;
                                model.Representative.Password = objPatient.RepersentativeDetail.Password;
                                model.Representative.Email = objPatient.RepersentativeDetail.Email;
                            }
                        }
                    }
                    else
                    {
                        model.NextLogOnSecurityVerificationPatient = null;
                        model.NextLogOnSecurityVerificationRepresentative = null;
                        if (objPatient.RepersentativeDetail != null)
                        {
                            if (objPatient.RepersentativeDetail.RepresentativeId != "0")
                            {
                                model.Representative.FirstName = objPatient.RepersentativeDetail.FirstName;
                                model.Representative.LastName = objPatient.RepersentativeDetail.LastName;
                                model.Representative.ExternalId = objPatient.RepersentativeDetail.RepresentativeId;
                                model.Representative.Username = objPatient.RepersentativeDetail.UserName;
                                model.Representative.Password = objPatient.RepersentativeDetail.Password;
                                model.Representative.Email = objPatient.RepersentativeDetail.Email;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }

        public static Hl7CDAModel GenerateHealthSummaryModel(PatientHealthSummary objHealthSummary)
        {
            Hl7CDAModel model = new Hl7CDAModel();
            try
            {
                if (objHealthSummary != null)
                {
                    model.LocationExternalId = objHealthSummary.LocationExternalId;
                    model.PatientExternalId = objHealthSummary.PatientExternalId;
                    model.CDAXml = objHealthSummary.CDAXml;
                    model.Type = "AmbulatorySummary";
                    model.DoctorExternalId = objHealthSummary.DoctorExternalId;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }

        public static AppointmentModel GenerateAppointmentModel(AppointmentEntity objTemp)
        {
            AppointmentModel model = new AppointmentModel();
            try
            {
                if (objTemp != null)
                {
                    model.LocationExternalId = objTemp.LocationExternalId;
                    model.PatientExternalId = objTemp.PatientExternalId;
                    model.DoctorExternalId = objTemp.DoctorExternalId;
                    model.AppointmentStatusExternalId = objTemp.AppointmentStatusExternalId;
                    model.CancelReason = objTemp.CancelReason;
                    model.ExternalId = objTemp.ExternalId;
                    model.Start = objTemp.Start;
                    model.End = objTemp.End;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }

        public static DoctorModel GenerateDoctorModel(Doctor objTemp)
        {
            DoctorModel model = new DoctorModel();
            try
            {
                if (objTemp != null)
                {
                    model.FirstName = objTemp.FirstName;
                    model.ExternalId = objTemp.ExternalId;
                    model.LastName = objTemp.LastName;
                    model.Alias = objTemp.AliasName;
                    model.LocationExternalId = objTemp.LocationExternalId;
                    model.Active = true;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }

        public static ResponseContentEntity GenerateResponseModelExternalLink(ListModel<ExternalHealthInformationLinkModel> objContent, string responseType)
        {
            ResponseContentEntity objRCE = new ResponseContentEntity();
            foreach (ExternalHealthInformationLinkModel e in objContent.Rows)
            {
                PatientExternalLink objTemp = new PatientExternalLink()
                {
                    PatientExternalId = e.PatientExternalId,
                    PatientId = e.PatientId.ToString(),
                    AlreadySent = e.AlreadySent,
                    Comment = e.Comment,
                    PatientLink = e.Link
                };
                objRCE.PatientItems = objTemp;
                objRCE.ResponseType = responseType;
            }
            return objRCE;
        }

        public static ResponseContentEntity GenerateResponseModelSharedFiles(ListModel<PatientSharedFileModel> objContent, string responseType)
        {
            ResponseContentEntity objRCE = new ResponseContentEntity();
            List<PatientSharedFileEntity> objFileShared = new List<PatientSharedFileEntity>();
            foreach (PatientSharedFileModel e in objContent.Rows)
            {
                PatientSharedFileEntity objTemp = new PatientSharedFileEntity()
                {
                    PatientExternalId = e.PatientExternalId,
                    DisplayName = e.DisplayName,
                    FileName = e.FileName,
                    Id = e.Id.ToString(),
                    PatientComments = e.PatientComments,
                    PortalCreated = e.PortalCreated.ToString(),
                    Status = e.Status
                };
                objFileShared.Add(objTemp);
            }
            objRCE.PatientItems = objFileShared;
            objRCE.ResponseType = responseType;
            return objRCE;
        }

        public static ResponseContentEntity GenerateResponseModelFileDetail(FileModel objContent, string responseType)
        {
            ResponseContentEntity objRCE = new ResponseContentEntity();
            FileDetail objTemp = new FileDetail()
            {
                FileName = objContent.FileName,
                Content = objContent.Content
            };
            objRCE.PatientItems = objTemp;
            objRCE.ResponseType = responseType;
            return objRCE;
        }

        public static PatientEducationMaterialModel GeneratePatientEducationModel(PatientEducationMaterial objPatientEducationMaterial)
        {
            PatientEducationMaterialModel model = new PatientEducationMaterialModel();
            try
            {
                if (objPatientEducationMaterial != null)
                {
                    model.DoctorExternalId = objPatientEducationMaterial.DoctorExternalId;
                    model.PatientExternalId = objPatientEducationMaterial.PatientExternalId;
                    model.Reviews = "1";
                    model.URL = objPatientEducationMaterial.DocumentURL;
                    model.Type = objPatientEducationMaterial.DocumentType;
                    model.Name = objPatientEducationMaterial.DocumentName;
                    model.ExternalId = objPatientEducationMaterial.ExternalId;
                    model.Id = Guid.NewGuid();
                    model.Active = true;
                    model.DateAddedUtc = DateTime.Now;
                }
            }
            catch
            {
                model = null;
            }
            return model;
        }

        public static IdModel GenerateModelForRecievedFile(PatientPGHDFileId objContent)
        {
            IdModel model = new IdModel();
            try
            {
                if (objContent != null)
                {
                    model.Id = objContent.ExternalId;
                }
            }
            catch
            {
                model = null;
            }
            return model;
        }

        public static DoctorModel GenerateDoctorModel(DoctorEntity objEntity)
        {
            DoctorModel model = new DoctorModel();
            if (objEntity.ExternalId != "")
            {
                model.Active = objEntity.Active.ToString() == "false" ? true : false;
                model.Alias = "";
                model.ExternalId = objEntity.ExternalId;
                model.FirstName = objEntity.FirstName;
                model.LastName = objEntity.LastName;
                model.InHouse = true;
                model.SecureRecipientExternalId = objEntity.SecureRecipientExternalId;
                model.LocationExternalId = objEntity.LocationExternalId;
                List<string> location = new List<string>(new string[] { "" });
                model.Locations = null; //location;
            }
            return model;
        }
    }
}