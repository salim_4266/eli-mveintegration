﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using MVE.PatientPortal.API.Models;
using GlobalPortal.Api.Client.Doctors;
using GlobalPortal.Api.Models.Doctors;

namespace MVE.PatientPortal.API.Utilities
{
    public class PortalAdmin
    {
        public static List<Practice> LoadPractices()
        {
            List<Practice> practiceList = new List<Practice>();
            string _selectQuery = "Select PracticeToken, PracticeAlias From MVE.PP_PracticeToken Order by PracticeAlias";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(_selectQuery, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Practice _practice = new Practice()
                    {
                        PracticeToken = rdr["PracticeToken"].ToString(),
                        PracticeAlias = rdr["PracticeAlias"].ToString()
                    };
                    practiceList.Add(_practice);
                }
                con.Close();
            }
            return practiceList;
        }

        public static string RegisterDoctor(Doctor objTemp)
        {
            string retStatus = "0";
            PortalQueue pq = new PortalQueue();
            DoctorModel model = GenerateModels.GenerateDoctorModel(objTemp);
            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            pq.PracticeToken = objTemp.PracticeToken;
            pq.ActionTypeLookupId = Convert.ToInt32(12);
            pq.IsActive = true;
            pq.MessageData = jsonObject;
            pq.PatientId = "";
            pq.ProcessedDate = System.DateTime.Now;
            pq.ExternalId = objTemp.ExternalId;
            int requestId = Utility.InsertDataIntoPortalQueue(pq);
            try
            {
                AuthenticationEntity OauthToken = Helpers.SetAuthentication(objTemp.PracticeToken);
                if (OauthToken.SyncUserName != null && OauthToken.SyncPassword != "")
                {
                    var success = DoctorProvider.Save(Helpers.GetAuthenticator(OauthToken.SyncUserName, OauthToken.SyncPassword, OauthToken.RedirectionLink), model);
                    if (success.ToString().ToLower() == "true")
                    {
                        retStatus = EnumTypes.ResponseStatus.Success.ToString();
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                    }
                    else
                    {
                        retStatus = EnumTypes.ResponseStatus.Error.ToString();
                        Utility.InsertDataIntoPortalQueueException(pq);
                    }
                }
                else
                {
                    pq.Exception = OauthToken.RedirectionLink;
                    retStatus = EnumTypes.ResponseStatus.InvalidCredentials.ToString();
                    Utility.InsertDataIntoPortalQueueException(pq);
                }

            }
            catch (Exception ex)
            {
                retStatus = EnumTypes.ResponseStatus.Exception.ToString();
                pq.Exception = ex.Message;
                Utility.InsertDataIntoPortalQueueException(pq);
            }
            return retStatus;
        }
    }
}