﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Utilities
{
    public class EmailResponseModel
    {
        public string Success { get; set; }
        public DataModel Data { get; set; }
    }

    public class DataModel
    {
        public string TransactionId { get; set; }
        public string MessageId { get; set; }
    }
}