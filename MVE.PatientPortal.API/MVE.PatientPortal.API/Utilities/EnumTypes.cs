﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace MVE.PatientPortal.API.Utilities
{
    public class EnumTypes
    {
        public class StringValueAttribute : System.Attribute
        {
            private string _value;
            public string Value
            {
                get { return _value; }
            }
            public StringValueAttribute(string value)
            {
                _value = value;
            }
        }
        
        public enum ActionTypeLookUps
        {
            NewAccountCreation = 1,
            SendUserInformation = 2,
            DeleteUserInformation = 3,
            GetUserInformation = 4,
            GetUserList = 5,
            SendLocationInformation = 6,
            GetLocationInformation = 7,
            GetLocationList = 8,
            DeleteLocationInformation = 9,
            SendAccountSettingsInformation = 10,
            GetAccountSettingsInformation = 11,
            SendDoctorInformation = 12,
            DeleteDoctorInformation = 13,
            GetDoctorInformation = 14,
            SendDoctorAvailabilitySlotInformation = 15,
            DeleteDoctorAvailabilitySlotInformation = 16,
            SendDosageFormInformation = 17,
            DeleteDosageFormInformation = 18,
            SendHealthReviewInformation = 19,
            SendInsuranceInformation = 20,
            DeleteInusranceInformation = 21,
            SendFamilyHistoryInformation = 22,
            DeleteFamilyInformation = 23,
            DeleteFamilyRelationshipInformation = 24,
            SendFamilyRelationshipInformation = 25,
            SendFamilyMemberInformation = 26,
            DeleteFamilyMemberInformation = 27,
            SendMedicationInformation = 28,
            DeleteMedicationInformation = 29,
            SendSurgeryInformation = 30,
            DeleteSurgeryInformation = 31,
            GetSurgeryInformation = 32,
            SendExamFieldMappingInformation = 33,
            DeleteExamFieldMappingInformation = 34,
            GetExamFieldMappingInformation = 35,
            SendExamFieldMappingModelDetailInformation = 36,
            DeleteExamFieldMappingModelDetailInformation = 37,
            SendSocialHistoryAlcoholInformation = 38,
            DeleteSocialHistoryAlcoholInformation = 39,
            GetSocialHistoryAlcoholInformation = 40,
            SendSocialHistoryDrugInformation = 41,
            DeleteSocialHistoryDrugInformation = 42,
            GetSocialHistoryDrugInformation = 43,
            GetPortalPatientInformation = 44,
            GetPortalPatientList = 45,
            UpdatePortalPatientInformation = 46,
            SendOpenFormsInformationToClose = 47,
            GetOpenFormInformation = 48,
            GetClosedFormInformation = 49,
            DeleteHealthReviewInformation = 50,
            GetHealthReviewInformation = 51,
            SendAllergiesInformation = 52,
            DeleteAllergyInformation = 53,
            GetAllergyInformation = 54,
            SendPrefixInformation = 55,
            DeletePrefixInformation = 56,
            SendSuffixInformation = 57,
            DeleteSuffixInformation = 58,
            SendOrderInformation = 59,
            DeleteOrderInformation = 60,
            GetOrderInformation = 70,
            GetOrderList = 71,
            SendOrderStatusInformation = 72,
            DeleteOrderStatusInformation = 73,
            GetOrderStatusInformation = 74,
            GetOrderStatusList = 75,
            UpdateOrdertInformation = 76,
            GetOrderListChangedThroughCommunication = 77,
            SendAppointmentStatusInformation = 78,
            DeleteAppointmentStatusInformation = 79,
            SendAppointmentInformation = 80,
            DeleteAppointmentInformation = 81,
            GetAppointmentInformation = 82,
            GetAppointmentList = 83,
            UpdateAppointmentInformation = 84,
            SendAppointmentRequest = 85,
            SendAppointmentCancelRequest = 86,
            GetAppointmentCancelRequestList = 87,
            SetAppointmentCancelRequestSent = 88,
            GetAppointmentListOfNewPatient = 89,
            SendAppointmentResultInformation = 90,
            GetAppointmentListChangedThroughCommunication = 91,
            GetPatientInformation = 92,
            DeletePatientInformation = 93,
            GetPateintList = 94,
            GetPateintCommunicationList = 94,
            UpdatePatientInformation = 95,
            GetPatientListChangedThroughCommunication = 96,
            SendPatientInformation = 97,
            GetFormList=98,
            SendPatientEducationMaterial = 136,
        }

        public enum ResponseStatus
        {
            Success = 1,
            Error = 2,
            Exception = 3,
            InvalidCredentials = 4,
            Updated = 5,
            InvalidPatient = 6
        }
        
    }
    public static class StringEnum
    {
        public static string GetStringValue(this Enum value)
        {
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(value.ToString());
            EnumTypes.StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(EnumTypes.StringValueAttribute), false) as EnumTypes.StringValueAttribute[];
            return attribs.Length > 0 ? attribs[0].Value : null;
        }
    }
}
