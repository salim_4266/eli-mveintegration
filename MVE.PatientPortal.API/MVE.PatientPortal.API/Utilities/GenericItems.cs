﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Utilities
{
    public static class GenericItems
    {
		//public static string APIKey = "1f614c42-13d1-4003-bc47-5ea45507a6e7"; 
		public static string APIKey = "7d0df22c-9316-4369-b189-83e34765f973"; 

		public static string AddressLink = "https://api.elasticemail.com/v2/email/send";

        public static string StatusUrl = "https://api.elasticemail.com/v2/email/getstatus?apikey={0}&transactionID={1}&showdelivered=true";

        public static string PortalUrl = "https://portal.eyereachpatients.com/Patients/{0}";
    }
}