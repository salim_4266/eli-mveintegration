﻿using GlobalPortal.Api.Client.ClientAuthentication;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MVE.PatientPortal.API.Models
{
    public class Helpers
    {
        static Helpers()
        {

        }

        public static AuthenticationEntity SetAuthentication(string PracticeToken)
        {
            AuthenticationEntity OauthToken = new AuthenticationEntity();
            try
            {
                string selectQry = "Select SyncUserName, SyncPassword, RedirectionLink, PracticeAccountNumber, PracticeDisplayName From MVE.PP_PracticeToken with (nolock) Where Cast(PracticeToken as nvarchar(100)) = '" + PracticeToken + "'";
                using (SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
                {
                    Conn.Open();
                    SqlCommand cmd = new SqlCommand(selectQry, Conn);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        OauthToken.RedirectionLink = rdr["RedirectionLink"].ToString();
                        OauthToken.SyncUserName = rdr["SyncUserName"].ToString();
                        OauthToken.SyncPassword = rdr["SyncPassword"].ToString();
                        OauthToken.PracticeAccountNumber = rdr["PracticeAccountNumber"].ToString();
                        OauthToken.PracticeDisplayName = rdr["PracticeDisplayName"].ToString();
                    }
                    else
                    {

                    }
                    Conn.Close();
                }
            }
            catch (Exception ee)
            {
                OauthToken.RedirectionLink = ee.ToString();
            }
            return OauthToken;
        }

        public static IServerAuthentication GetAuthenticator()
        {
            var username = "e5c57412-bad3-486a-a6a7-cd552b9c1ccf";
            var password = "62094869-7bb2-4834-9d51-12d1765027ea";
            var WebApiAddress = "https://preproductionapi.eyereachpatients.com/api";
            return GlobalPortalRestClient.GetAuthenticator(username, password, WebApiAddress);
        }

        public static IServerAuthentication GetAuthenticator(string username, string password, string WebApiAddress)
        {
            return GlobalPortalRestClient.GetAuthenticator(username, password, WebApiAddress);
        }
    }
}
