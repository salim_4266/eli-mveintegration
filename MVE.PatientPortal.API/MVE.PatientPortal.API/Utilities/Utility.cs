﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using AppointmentAPI.DAO;
using MVE.PatientPortal.API.Models;
using System.Configuration;
namespace MVE.PatientPortal.API.Utilities
{
    public class Utility
    {
        public Utility() { }
        
        public static string validateDate(DateTime startdate, DateTime enddate, DateTime todaydate)
        {
            string msg = String.Empty;
            if (startdate > todaydate)
            {
                if (msg == "")
                    msg = "Start Date can't be future date.";
                else
                    msg = msg + "\n Start Date can't be future date.";
            }
            if (enddate > todaydate)
            {
                if (msg == "")
                    msg = "End Date can't be future date.";
                else
                    msg = msg + "\n End Date can't be future date.";
            }
            if (startdate > enddate)
            {
                if (msg == "")
                    msg = "Start Date can't be greater than End Date.";
                else
                    msg = msg + "\n Start Date can't be greater than End Date.";
            }
            return msg;
        }
        public static int InsertDataIntoPortalQueue(PortalQueue pq)
        {
            int status = -1;
            try
            {
                string query = "[MVE].PP_InsertPortalQueue";
                SqlParameter[] parameter = new SqlParameter[7];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@PracticeToken", pq.PracticeToken.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@MessageData", pq.MessageData.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@ActionTypeLookupId", pq.ActionTypeLookupId);
                parameter[++columnIndex] = new SqlParameter("@ProcessedDate", pq.ProcessedDate);
                parameter[++columnIndex] = new SqlParameter("@PatientNo", pq.PatientId);
                parameter[++columnIndex] = new SqlParameter("@IsActive", pq.IsActive);
                parameter[++columnIndex] = new SqlParameter("@RequestId", 0);
                parameter[columnIndex].Direction = ParameterDirection.Output;
                DataAccessObjects.SetConnectionString(ConfigurationManager.ConnectionStrings["PracticeRepository"].ToString());
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
                if (status > 0) {
                    status = Convert.ToInt32(parameter[6].SqlValue.ToString());
                }
            }
            catch
            {
                throw;
            }
            return status;
        }
        public static int InsertDataIntoPortalQueueResponse(PortalQueue pq)
        {
            int status = -1;
            try
            {
                string query = "[MVE].PP_InsertPortalQueueResponse";
                SqlParameter[] parameter = new SqlParameter[7];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@PracticeToken", pq.PracticeToken.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@MessageData", pq.MessageData.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@ActionTypeLookupId", pq.ActionTypeLookupId);
                parameter[++columnIndex] = new SqlParameter("@ProcessedDate", pq.ProcessedDate);
                parameter[++columnIndex] = new SqlParameter("@PatientNo", pq.PatientId);
                parameter[++columnIndex] = new SqlParameter("@IsActive", pq.IsActive);
                parameter[++columnIndex] = new SqlParameter("@ExternalId", pq.ExternalId);
                DataAccessObjects.SetConnectionString(ConfigurationManager.ConnectionStrings["PracticeRepository"].ToString());
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch
            {
                throw;
            }
            return status;
        }
        public static int InsertDataIntoPortalQueueException(PortalQueue pq)
        {
            int status = -1;
            try
            {
                string query = "[MVE].PP_InsertPortalQueueException";
                SqlParameter[] parameter = new SqlParameter[8];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@PracticeToken", pq.PracticeToken.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@MessageData", pq.MessageData.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@ActionTypeLookupId", pq.ActionTypeLookupId);
                parameter[++columnIndex] = new SqlParameter("@ProcessedDate", pq.ProcessedDate);
                parameter[++columnIndex] = new SqlParameter("@PatientNo", pq.PatientId);
                parameter[++columnIndex] = new SqlParameter("@IsActive", pq.IsActive);
                parameter[++columnIndex] = new SqlParameter("@ExternalId", pq.ExternalId);
                parameter[++columnIndex] = new SqlParameter("@Exception", pq.Exception);
                DataAccessObjects.SetConnectionString(ConfigurationManager.ConnectionStrings["PracticeRepository"].ToString());
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch
            {
                throw;
            }
            return status;
        }
    }
}
