﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Utilities
{
    public class StatusResponseModel
    {
        public string Success { get; set; }
        public StatusDataModel Data { get; set; }
    }

    public class StatusDataModel
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string RecipientsCount { get; set; }
        public string Failed { get; set; }
        public string FailedCount { get; set; }
        public string Sent { get; set; }
        public string SentCount { get; set; }
        public string[] Delivered { get; set; }
        public string DeliveredCount { get; set; }
        public string Pending { get; set; }
        public string PendingCount { get; set; }
        public string Opened { get; set; }
        public string OpenedCount { get; set; }
        public string Clicked { get; set; }
        public string ClickedCount { get; set; }
        public string Unsubscribed { get; set; }
        public string UnsubscribedCount { get; set; }
        public string AbuseReports { get; set; }
        public string AbuseReportsCount { get; set; }
        public string MessageIds { get; set; }

    }
}