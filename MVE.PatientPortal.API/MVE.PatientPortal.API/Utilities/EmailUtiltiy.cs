﻿using MVE.PatientPortal.API.Models;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Threading;

namespace MVE.PatientPortal.API.Utilities
{
    public class EmailUtility
    {
        public string Email_To { get; set; }
        public string Email_Subject { get; set; }
        public string Email_Body { get; set; }
       
        public bool SendEmailViaAPI(PatientEmailEntity objPatientEmail, string ExternalId)
        {
            NameValueCollection values = new NameValueCollection();
            bool retStatus = false;
            try
            {
                PracticeEmailTemplate objEmailTemplate = new PracticeEmailTemplate(objPatientEmail.PracticeToken, "1");
                this.Email_Body = CreateHTMLTemplate(objPatientEmail, objEmailTemplate.Email_Body, ExternalId);

                values.Add("apikey", GenericItems.APIKey);
                values.Add("from", "help@iopracticeware.com");
                values.Add("to", objPatientEmail.PatientEmailAddress);
                values.Add("subject", objEmailTemplate.Email_Subject);
                values.Add("bodyHtml", this.Email_Body);
                values.Add("fromName", objPatientEmail.PracticeDisplayName);
                values.Add("replyTo", objEmailTemplate.ReplyEmail);
                values.Add("charsetBodyHtml","UTF-8");
                string responseData = Send(GenericItems.AddressLink, values);
                EmailResponseModel _responseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailResponseModel>(responseData);
                if (_responseModel.Success == "true")
                {
                    string transactionId = _responseModel.Data.TransactionId;
                    string messageId = _responseModel.Data.MessageId;
                    InsertDataIntoEmailStatus(objPatientEmail, ExternalId.Replace("-", ""), transactionId, messageId, false);
                    Thread.Sleep(8000);
                    string statusUrl = string.Format(GenericItems.StatusUrl, GenericItems.APIKey, transactionId);
                    string statusResponse = Send(statusUrl, values);
                    StatusResponseModel _statusModel= Newtonsoft.Json.JsonConvert.DeserializeObject<StatusResponseModel>(statusResponse);
                    if (_statusModel.Success == "true")
                    {
                        retStatus = true;
                        int count = int.Parse(_statusModel.Data.DeliveredCount.ToString());
                        if(count == 1)
                        {
                            UpdateDeliveryStatus(ExternalId.Replace("-", ""), retStatus, transactionId);
                        }
                    }
                }
            }
            catch
            {
                retStatus = false;
            }
            return retStatus;
        }
        public static string Send(string address, NameValueCollection values)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    byte[] apiResponse = client.UploadValues(address, values);
                    return Encoding.UTF8.GetString(apiResponse);
                }
                catch (Exception ex)
                {
                    return "Exception caught: " + ex.Message + "\n" + ex.StackTrace;
                }
            }
        }
        //Email Logs
        private void InsertDataIntoEmailStatus(PatientEmailEntity obj, string patientId, string transactionId, string messageId, bool status)
        {
            string insQry = "Insert into [MVE].[PP_EmailStatus](PracticeToken, PatientId, EmailAddress, DeliveryStatus, CreatedDate, TransactionId, MessageId) Values ('" + obj.PracticeToken + "','" + patientId + "','" + obj.PatientEmailAddress + "','" + status + "','" + DateTime.Now + "','" + transactionId + "','" + messageId + "')";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(insQry, conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = insQry;
                cmd.ExecuteNonQuery();
            }
        }

        private void UpdateDeliveryStatus(string patid, bool emailDlvryStatus,string transactionId)
        {
            string updateQry = "Update [MVE].[PP_EmailStatus] Set DeliveryStatus = '" + emailDlvryStatus + "' Where Transactionid='"+ transactionId + "' and PatientId = '" + patid + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(updateQry, conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = updateQry;
                cmd.ExecuteNonQuery();
            }
        }

        public bool SendCredentials(CredentialEntity objCredentials)
        {
            AuthenticationEntity OauthToken = Helpers.SetAuthentication(objCredentials.PracticeToken);
            PatientEmailEntity objPatientEmail = new PatientEmailEntity()
            {
                Password = "",
                PatientEmailAddress = objCredentials.EmailAddress,
                PatientName = objCredentials.FirstName + " " + objCredentials.LastName,
                UserName = "",
                PracticeDisplayName = OauthToken.PracticeDisplayName,
                PracticeERP = OauthToken.PracticeAccountNumber,
                PracticeToken = objCredentials.PracticeToken,
                ExternalId = objCredentials.ExternalId
            };
            bool retStatus = false;
            string externalId = objCredentials.UserType == "Patient" ? objCredentials.ExternalId : "-" + objCredentials.ExternalId;
            retStatus=SendEmailViaAPI(objPatientEmail, externalId);
            return retStatus;
        }

        public bool SendCredentials(PatientEmailEntity objPatientEmail)
        {
            bool retStatus = false;
            retStatus = SendEmailViaAPI(objPatientEmail, objPatientEmail.ExternalId);
            return retStatus;
        }
        private string CreateHTMLTemplate(PatientEmailEntity objPatientEmail, string HTMLTemplate,string patientId)
        {
            string TempURL = string.Empty;
            if (objPatientEmail.PracticeToken == "B1738D8F-54A2-4D9E-B07B-A645DDB4F885")
            {
                TempURL = "https://preproductionportal.eyereachpatients.com/Patients/" + objPatientEmail.PracticeERP;
            }
            else if (objPatientEmail.PracticeToken == "988C4C26-BE2E-4D1A-A239-87F85DFD052C")
            {
                TempURL = "https://preproductionportal.eyereachpatients.com/Patients/" + objPatientEmail.PracticeERP;
            }
            else if (objPatientEmail.PracticeToken == "61BA936F-EDA3-497B-B7A3-238F20721BA0")
            {
                TempURL = "https://preproductionportal.eyereachpatients.com/Patients/" + objPatientEmail.PracticeERP;
            }
            else
            {
                TempURL = "https://portal.eyereachpatients.com/Patients/" + objPatientEmail.PracticeERP;
            }
            HTMLTemplate = HTMLTemplate.Replace("xxxPatientNamexxx", objPatientEmail.PatientName);
            HTMLTemplate = HTMLTemplate.Replace("xxxUserNamexxx", objPatientEmail.UserName);
            HTMLTemplate = HTMLTemplate.Replace("xxxPatientIdxxx",patientId.ToString());
            HTMLTemplate = HTMLTemplate.Replace("xxxPracticeNamexxx", objPatientEmail.PracticeDisplayName);
            HTMLTemplate = HTMLTemplate.Replace("xxxPasswordxxx", objPatientEmail.Password);
            HTMLTemplate = HTMLTemplate.Replace("xxxURLxxx", TempURL);
            return HTMLTemplate;
        }
    }
}