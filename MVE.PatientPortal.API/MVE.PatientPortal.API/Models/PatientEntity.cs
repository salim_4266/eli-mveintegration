﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class DefaultEntity
    {
        public string PracticeToken { get; set; }
    }

    public class CredentialEntity : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string UserType { get; set; }
    }

    public class PatientEntity : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string LocationID { get; set; }
        public string EmailAddresses { get; set; }
        public string PhoneNumbers { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string BirthDate { get; set; }
        public string Notes { get; set; }
        public string LanguageName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsSendEmailViaAPI { get; set; }
        public string Gender { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string MaritalStatus { get; set; }
        public PatientRepersentativeDetail RepersentativeDetail { get; set; }
    }

    public class PatientRepersentativeDetail
    {
        public string RepresentativeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class PatientEducationMaterial : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string PatientExternalId { get; set; }
        public string DoctorExternalId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }
        public string DocumentURL { get; set; }
        public string IsActive { get; set; }
    }

    public class PatientEmailEntity : DefaultEntity
    {
        public string PracticeDisplayName { get; set; }
        public string PracticeERP { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PatientName { get; set; }
        public string PatientEmailAddress { get; set; }
        public string ExternalId { get; set; }

        public PatientEmailEntity()
        {

        }

        public PatientEmailEntity(EmailEntity objEE)
        {
            this.Password = objEE.Password;
            this.PatientEmailAddress = objEE.PatientEmailAddress;
            this.PatientEmailAddress = objEE.PatientEmailAddress;
            this.PatientName = objEE.PatientName;
            this.UserName = objEE.UserName;
            this.PracticeToken = objEE.PracticeToken;
            string selectQry = "Select a.PracticeDisplayName, a.PracticeAccountNumber from [MVE].[PP_PracticeToken] a Where a.PracticeToken = '" + objEE.PracticeToken + "'";
            using (SqlConnection _connection = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
            {
                _connection.Open();
                using (SqlCommand cmd = new SqlCommand(selectQry, _connection))
                {
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        this.PracticeDisplayName = rdr["PracticeDisplayName"].ToString();
                        this.PracticeERP = rdr["PracticeAccountNumber"].ToString();
                    }
                }
                _connection.Close();
            }
        }

    }

    public class EmailEntity : DefaultEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PatientName { get; set; }
        public string PatientEmailAddress { get; set; }

        
    }

    public class PatientHealthSummary : DefaultEntity
    {
        public string PatientExternalId { get; set; }
        public string LocationExternalId { get; set; }
        public string DoctorExternalId { get; set; }
        public string CDAXml { get; set; }
        public string Type { get; set; }
    }

    public class AppointmentEntity : DefaultEntity
    {
        public string PatientExternalId { get; set; }
        public string LocationExternalId { get; set; }
        public string DoctorExternalId { get; set; }
        public string AppointmentStatusExternalId { get; set; }
        public string CancelReason { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string ExternalId { get; set; }
    }

    public class PatientLinkEntity : DefaultEntity
    {
        public string PatientExternalId { get; set; }
    }

    public class PatientPGHDFileId : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string PatientExternalId { get; set; }
    }

    public class PatientExternalLink
    {
        public string PatientId { get; set; }
        public string PatientExternalId { get; set; }
        public string PatientLink { get; set; }
        public string Comment { get; set; }
        public bool AlreadySent { get; set; }
    }

    public class PatientSharedFileEntity
    {
        public string Id { get; set; }
        public string PatientExternalId { get; set; }
        public string PortalCreated { get; set; }
        public string DisplayName { get; set; }
        public string FileName { get; set; }
        public string PatientComments { get; set; }
        public string Status { get; set; }
    }

    public class FileDetail
    {
        public byte[] Content { get; set; }
        public string FileName { get; set; }
    }
}