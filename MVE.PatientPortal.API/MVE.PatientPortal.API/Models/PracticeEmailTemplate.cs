﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class PracticeEmailTemplate : DefaultEntity
    {
        public string Email_To { get; set; }
        public string Email_CC { get; set; }
        public string Email_BCC { get; set; }
        public string Email_Subject { get; set; }
        public string Email_Body { get; set; }
        public string ReplyEmail { get; set; }

        public PracticeEmailTemplate(string _practiceToken, string _templateTypeId)
        {
            string selectQry = "Select PracticeToken, Email_To, Email_CC, Email_BCC, Email_Subject, Email_Body, ReplyEmail From MVE.PP_PracticeEmailTemplates with (nolock) Where Cast(PracticeToken as nvarchar(100)) = '" + _practiceToken + "' and TemplateTypeId = '" + _templateTypeId + "'";
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
            {
                Conn.Open();
                SqlCommand cmd = new SqlCommand(selectQry, Conn);
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    this.PracticeToken = rdr["PracticeToken"].ToString();
                    this.Email_To = rdr["Email_To"].ToString();
                    this.Email_CC = rdr["Email_CC"].ToString();
                    this.Email_BCC = rdr["Email_BCC"].ToString();
                    this.Email_Subject = rdr["Email_Subject"].ToString();
                    this.Email_Body = rdr["Email_Body"].ToString();
                    this.ReplyEmail = rdr["ReplyEmail"].ToString();
                }
                else
                {

                }
                Conn.Close();
            }
        }
    }
}