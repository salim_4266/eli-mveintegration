﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class ResponseEntity
    {
        public string ResponseType { get; set; }
        public string ExternalId { get; set; }
    }

    public class ResponseContentEntity
    {
        public object PatientItems { get; set; }
        public string ResponseType { get; set; }
    }

}