﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class Practice
    {
        public string PracticeToken { get; set; }
        public string PracticeAlias { get; set; }
    }

    public class Doctor
    {
        public string PracticeToken { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ExternalId { get; set; }
        public string LocationExternalId { get; set; }
        public string AliasName { get; set; }
    }
}