﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class RequestTokenEntity
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public string ProductKey { get; set; }
        public string PairKey { get; set; }
    }

    public class ResponseTokenEntity
    {
        public string TokenStatus { get; set; }
        public string TokenDetail { get; set; }
    }
}