﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class PortalQueue
    {
        public string PracticeToken { get; set; }
        public int ActionTypeLookupId { get; set; }
        public string Exception { get; set; }
        public string ExternalId { get; set; }
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string MessageData { get; set; }
        public string PatientId { get; set; }
        public DateTime ProcessedDate { get; set; }
    }
}