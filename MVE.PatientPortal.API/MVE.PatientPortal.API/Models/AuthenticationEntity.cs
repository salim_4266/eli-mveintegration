﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class AuthenticationEntity
    {
        public string SyncUserName { get; set; }
        public string SyncPassword { get; set; }
        public string RedirectionLink { get; set; }
        public string PracticeDisplayName { get; set; }
        public string PracticeAccountNumber { get; set; }
    }
}