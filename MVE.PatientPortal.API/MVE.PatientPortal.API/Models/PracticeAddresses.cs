﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class PracticeAddresses
    {
        public string PracticeToken { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zipcode { get; set; }

        public PracticeAddresses(string practiceToken)
        {
            string selectQry = "Select PracticeToken, AddressLine1, AddressLine2, City, State, Email_Body, Country, Zipcode From MVE.PP_PracticeAddresses with (nolock) Where Cast(PracticeToken as nvarchar(100)) = '" + practiceToken + "'";
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ToString()))
            {
                Conn.Open();
                SqlCommand cmd = new SqlCommand(selectQry, Conn);
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    this.PracticeToken = rdr["PracticeToken"].ToString();
                    this.AddressLine1 = rdr["AddressLine1"].ToString();
                    this.AddressLine2 = rdr["AddressLine2"].ToString();
                    this.City = rdr["City"].ToString();
                    this.State = rdr["State"].ToString();
                    this.Country = rdr["Country"].ToString();
                    this.Zipcode = rdr["Zipcode"].ToString();
                }
                else
                {

                }
                Conn.Close();
            }
        }

    }
}