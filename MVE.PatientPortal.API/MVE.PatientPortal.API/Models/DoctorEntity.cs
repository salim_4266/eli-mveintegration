﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVE.PatientPortal.API.Models
{
    public class DoctorEntity : DefaultEntity
    {
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Active { get; set; }
        public string InHouse { get; set; }
        public string LocationExternalId { get; set; }
        public string SecureRecipientExternalId { get; set; }
    }
}