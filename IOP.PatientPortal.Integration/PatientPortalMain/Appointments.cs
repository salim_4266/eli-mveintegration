﻿using System;
using System.Collections.Generic;
using log4net;
using PatientPortalFacade;
using GlobalPortal.Api.Models.AppointmentStatuses;
using GlobalPortal.Api.Models.AppointmentsModel;
using GlobalPortal.Api.Client.AppointmentStatuses;
using GlobalPortal.Api.Client.Appointments;
using System.Diagnostics;

namespace PatientPortalMain
{
    public class Appointments
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Appointments));
        #region AppointmentStatuses
        public void sendAppointmentStatusInformation()
        {
            PortalQueue pq = new PortalQueue();
            AppointmentFacade facade = new AppointmentFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<AppointmentStatusModel> list = facade.GetAppointmentStatuses();
                    if (list != null && list.Count > 0)
                    {
                        foreach (AppointmentStatusModel model in list)
                        {
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAppointmentStatusInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.ExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = "";
                            try
                            {
                                int requestId = Utility.InsertDataIntoPortalQueue(pq);
                                var success = AppointmentStatusProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.Id != null)
                                {
                                    pq.ExternalId = success.Id.ToString();
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("AppointmentStatusId - " + pq.ExternalId + " sent to MVE", EventLogEntryType.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending appt status's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                        }
                    }
                    else 
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Appointments", "SendAppointmentStatusInformation", "", ex.Message); }
            }
        }
        #endregion
        
        #region Appointments
        public void sendNewAppointmentInformation()
        {
            PortalQueue pq = new PortalQueue();
            AppointmentFacade facade = new AppointmentFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<AppointmentModel> list = facade.GetNewAppointment();
                    if (list != null && list.Count > 0)
                    {
                        foreach (AppointmentModel model in list)
                        {
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            if (Convert.ToInt16(model.AppointmentStatusExternalId) < 4)
                            {
                                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAppointmentInformation);
                                model.AppointmentStatusExternalId = "1";
                            }
                            else if (Convert.ToInt16(model.AppointmentStatusExternalId) < 8)
                            {
                                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.UpdateAppointmentInformation);
                                model.AppointmentStatusExternalId = "7";
                            }
                            else 
                            {
                                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAppointmentInformation);
                                model.AppointmentStatusExternalId = "8";
                            }
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.PatientExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = AppointmentProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.ExternalId.ToString() != "")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Appointment Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                            }
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendAllergiesInformation", "", ex.Message);
            }

        }
        public void sendCancelAppointmentInformation()
        {
            PortalQueue pq = new PortalQueue();
            AppointmentFacade facade = new AppointmentFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<AppointmentModel> list = facade.GetCancelAppointment();
                    if (list != null && list.Count > 0)
                    {
                        foreach (AppointmentModel model in list)
                        {
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteAppointmentInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.PatientExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = AppointmentProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.ExternalId.ToString() != "")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Appointment Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                            }
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Appointments", "sendCancelAppointmentInformation", "", ex.Message);
            }

        }
        public void sendDischargeAppointmentInformation()
        {
            PortalQueue pq = new PortalQueue();
            AppointmentFacade facade = new AppointmentFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<AppointmentModel> list = facade.GetDischargeAppointment();
                    if (list != null && list.Count > 0)
                    {
                        foreach (AppointmentModel model in list)
                        {
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.UpdateAppointmentInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.PatientExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = AppointmentProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.ExternalId.ToString() != "")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Appointment Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                EventLogger.Instance.WriteLog("Exception " + ex.InnerException.ToString(), EventLogEntryType.Error);
                                Utility.InsertDataIntoPortalQueueException(pq);
                            }
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Appointments", "sendDischargeAppointmentInformation", "", ex.Message);
            }

        }
        #endregion 
    }
}
