﻿using System;
using System.Collections.Generic;
using GlobalPortal.Api.Client.FamilyHistories;
using GlobalPortal.Api.Models.FamilyHistories;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientPortalMain
{
    public class FamilyHistories
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(FamilyHistories));
        public void sendFamilyHistoryInformation()
        {
            PortalQueue pq = new PortalQueue();

            FamilyHistoriesFacade facade = new FamilyHistoriesFacade();
            try
            {

                List<FamilyHistoryModel> list = facade.GetFamilyHistoryModel();
                if (list != null && list.Count > 0)
                {
                    foreach (FamilyHistoryModel model in list)
                    {

                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendFamilyHistory);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        var success = FamilyHistoryProvider.Save(Helpers.GetAuthenticator(), model);

                        if (success.ToString() == "True")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("FamilyHistories", "sendFamilyHistoryInformation", "", ex.Message); }

            }
        }
        public void DeleteFamilyInformation()
        {
            PortalQueue pq = null;

            FamilyHistoriesFacade facade = new FamilyHistoriesFacade();
            FamilyHistoryModel model = new FamilyHistoryModel();

            try
            {
                model.ExternalId = "101088";
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteFamilyInformation);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = model.ExternalId;
                pq.ProcessedDate = System.DateTime.Now;

                var success = FamilyHistoryProvider.Delete(Helpers.GetAuthenticator(), model.ExternalId);
                if (success != null)
                {
                   
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("FamilyHistories", "DeleteFamilyInformation", "", ex.Message); }

            }
        }
    }
}
