﻿using GlobalPortal.Api.Client.Allergies;
using GlobalPortal.Api.Models.Allergies;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
namespace PatientPortalMain
{
   public class Allergies
    {
       private static readonly ILog _logger = LogManager.GetLogger(typeof(Allergies));
        public void sendAllergiesInformation()
        {
            PortalQueue pq = new PortalQueue();

            AllergiesFacade facade = new AllergiesFacade();

            try
            {
                List<AllergyModel> list = facade.GetAllergyModel();
                if (list != null && list.Count > 0)
                {
                    foreach (AllergyModel model in list)
                    {

                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendUserInformation);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);

                        var success = AllergyProvider.Save(Helpers.GetAuthenticator(), model);

                        if (success.ToString() == "True")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Allergies", "sendAllergiesInformation", "", ex.Message); }

            }
        }
    }
}
