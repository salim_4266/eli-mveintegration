﻿using System;
using System.Collections.Generic;
using GlobalPortal.Api.Client.DosageForms;
using GlobalPortal.Api.Models.DosageForms;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PatientPortalMain
{
    public class DosageForms
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DosageForms));
        public void sendDosageFormsInformation()
        {
            PortalQueue pq = new PortalQueue();

            DosageFormsFacade facade = new DosageFormsFacade();
            try
            {

                List<DosageFormModel> list = facade.GetDosageFormModel();
                if (list != null && list.Count > 0)
                {
                    foreach (DosageFormModel model in list)
                    {

                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendFamilyHistory);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        var success = DosageFormProvider.Save(Helpers.GetAuthenticator(), model);

                        if (success.ToString() == "True")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("DosageForms", "sendDosageFormsInformation", "", ex.Message); }

            }
        }
    }
}
