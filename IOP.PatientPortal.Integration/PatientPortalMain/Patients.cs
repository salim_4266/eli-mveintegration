﻿using GlobalPortal.Api.Client.Hl7CDAs;
using GlobalPortal.Api.Client.PatientCommunications;
using GlobalPortal.Api.Client.Patients;
using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.MarkAsReceived;
using GlobalPortal.Api.Models.PatientCommunications;
using GlobalPortal.Api.Models.Patients;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;

namespace PatientPortalMain
{
    public class Patients
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Patients));
       
        #region Patients
        /// <summary>
        /// Creates a new patient or updates information for an existent patient.
        /// </summary>
        public void sendPatientInformation()
        {
            PortalQueue pq = new PortalQueue();
            PatientsFacade patientfacade = new PatientsFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    string _practiceToken = patientfacade.getPracticeToken();
                    if (_practiceToken != "")
                    {
                        DataTable PatientList = patientfacade.getPatientLog();
                        if (PatientList != null && PatientList.Rows.Count > 0)
                        {
                            string recordId = string.Empty;
                            string patientId = string.Empty;
                            string eventName = string.Empty;
                            bool IsEmailUpdated = false;
                            foreach (DataRow dr in PatientList.Rows)
                            {
                                eventName = dr["EventName"].ToString();
                                IsEmailUpdated = Convert.ToBoolean(dr["IsEmailUpdated"]);
                                patientId = dr["PatientId"].ToString();
                                recordId = dr["Id"].ToString();
                                PatientModel model = patientfacade.getPatientDetail(patientId);
                                if (eventName.ToLower() == "update")
                                {
                                    model.NextLogOnSecurityVerificationPatient = null;
                                    model.NextLogOnSecurityVerificationRepresentative = null;
                                    model.Username = "";
                                    model.Password = "";
                                }
                                var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendPatientInformation);
                                pq.IsActive = true;
                                pq.MessageData = jsonObject;
                                pq.PatientId = model.ExternalId;
                                pq.ProcessedDate = System.DateTime.Now;
                                pq.ExternalId = model.ExternalId;
                                int requestId = Utility.InsertDataIntoPortalQueue(pq);
                                try
                                {
                                    var success = PatientProvider.Save(Helpers.GetAuthenticator(), model);
                                    if (success != null)
                                    {
                                        pq.ExternalId = success.Id.ToString();
                                        Utility.InsertDataIntoPortalQueueResponse(pq);
                                        if (model.Contact.EmailAddresses.Count > 0)
                                        {
                                            if (eventName.ToLower() == "register" && model.Contact.EmailAddresses[0].Address.ToString() != "")
                                            {
                                                SendCredentials(_practiceToken, model);
                                            }
                                            else if (eventName.ToLower() == "update" && IsEmailUpdated && model.Contact.EmailAddresses[0].Address.ToString() != "")
                                            {
                                                SendCredentials(_practiceToken, model);
                                            }
                                        }
                                        EventLogger.Instance.WriteLog("Patient Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                        Utility.UpdatePatientLog(recordId);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    pq.Exception = ex.Message;
                                    Utility.InsertDataIntoPortalQueueException(pq);
                                    EventLogger.Instance.WriteLog("Ëxception raise while sending patient's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                                }
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                        else
                        {
                            status = false;
                        }
                    }
                    else
                    {
                        EventLogger.Instance.WriteLog("Missing Practice token from confgi file", EventLogEntryType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendPatientInformation", "", ex.Message);
            }
        }
        #endregion Patients

        #region PatientCommunication
        /// <summary>
        /// Returns a list of patient communications.
        /// </summary>
        public void GetPateintCommunicationList()
        {
            PortalQueue pq = null;
            PatientsFacade facade = new PatientsFacade();
            PatientModel model = new PatientModel();
            ListModel<PatientCommunicationModel> list = new ListModel<PatientCommunicationModel>();
            try
            {
                bool? alreadySent = null;
                string creationDateOperator = "";
                DateTime? utcCreationDate1 = null;
                DateTime? utcCreationDate2 = null;
                string patientExternalId = "";
                string appointmentExternalId = "";
                bool? outgoing = null;
                int page = 1;
                int itemsPerPage = 10;
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetPateintCommunicationList);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = patientExternalId;
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = patientExternalId;
                var success = PatientCommunicationProvider.Search(Helpers.GetAuthenticator(), alreadySent, creationDateOperator, utcCreationDate1, utcCreationDate2, patientExternalId, appointmentExternalId, outgoing, page, itemsPerPage);
                if (success != null)
                {
                    list = (ListModel<PatientCommunicationModel>)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Patients", "GetPateintCommunicationList", "", ex.Message); }
            }
        }
        #endregion PatientCommunication

        #region PatientUpdateSent
        /// <summary>
        /// Flags a patient update as sent to the integrating system.
        /// </summary>
        public void updatePatientInformation()
        {
            PortalQueue pq = null;
            PatientsFacade facade = new PatientsFacade();
            try
            {
                List<MarkAsReceivedModel> list = facade.getPatientModelForUpdation();
                if (list != null && list.Count > 0)
                {
                    foreach (MarkAsReceivedModel model in list)
                    {
                        pq = new PortalQueue();
                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.UpdatePatientInformation);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.ExternalId = model.InternalID.ToString();
                        pq.ProcessedDate = System.DateTime.Now;
                        pq.ExternalId = model.InternalID.ToString();
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        //var success = MarkAsReceivedProvider.Update(Helpers.GetAuthenticator(), model);
                        //if (success.ToString().ToLower() == "true")
                        //{
                        //    Utility.InsertDataIntoPortalQueueResponse(pq);
                        //}
                        pq = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Patients", "UpdatePatientInformation", "", ex.Message); }
            }
        }
        #endregion PatientUpdateSent

        #region PatientHealthInformation
        /// <summary>
        /// Creates a new patient or updates information for an existent patient.
        /// </summary>
        public void sendPatientHealthInformation()
        {
            PortalQueue pq = new PortalQueue();
            PatientsFacade patientfacade = new PatientsFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    EventLogger.Instance.WriteLog("Calling Start Health Information Procedure", EventLogEntryType.Information);
                    List<CDAModel> list = patientfacade.getPatientHealthModel();
                    EventLogger.Instance.WriteLog("Calling End Health Information Procedure", EventLogEntryType.Information);
                    if (list != null && list.Count > 0)
                    {
                        foreach (CDAModel model in list)
                        {
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model.CDAMessage);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendHealthReviewInformation);
                            pq.IsActive = true;
                            pq.MessageData = "PHI";
                            pq.PatientId = model.CDAMessage.PatientExternalId;
                            pq.ProcessedDate = DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = Hl7CDAProvider.Save(Helpers.GetAuthenticator(), model.CDAMessage);
                                if (success.Id.ToString() != "")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Message Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending PHI information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        EventLogger.Instance.WriteLog("No Data Found in Health Information Procedure", EventLogEntryType.Information);
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendPatientInformation", "", ex.Message);
            }
        }

        #endregion PatientHealthInformation

        #region Credentials 

        public bool SendCredentials(string _practiceToken, PatientModel objPModel)
        {
            bool retStatus = false;
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                EmailEntity objLinkEnity = new EmailEntity()
                {
                    PracticeToken = _practiceToken,
                    EmailAddress = objPModel.Contact.EmailAddresses[0].Address.ToString(),
                    FirstName = objPModel.Contact.FirstName,
                    LastName = objPModel.Contact.LastName,
                    ExternalId = objPModel.ExternalId,
                    UserType = "patient"
                };
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objLinkEnity);
                string result = webclient.UploadString("https://portal.iopracticeware.com/api/Patient/SendCredentials", "POST", jsonToBeSent);
                ResponseEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
                if (objResponse.ResponseType.ToLower().Contains("success"))
                {
                    retStatus = true;
                }
            }
            catch
            {

            }
            return retStatus;
        }

        #endregion

    }
}
