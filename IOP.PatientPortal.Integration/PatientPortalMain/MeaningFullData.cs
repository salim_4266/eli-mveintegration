﻿using GlobalPortal.Api.Client.OutgoingSecureMessages;
using GlobalPortal.Api.Models.OutgoingSecureMessages;
using PatientPortalFacade;
using System;
using System.Diagnostics;
using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.MarkAsReceived;

namespace PatientPortalMain
{
    public class MeaningFullData
    {
        public void GetSecureMessagesLogs()
        {
            ListModel<OutgoingSecureMessageInfoModel> model = new ListModel<OutgoingSecureMessageInfoModel>();
            try
            {
                model = OutgoingSecureMessageProvider.Search(Helpers.GetAuthenticator(), "", "", "", "", "", "", null, null, null, false, null, null, null, false, null, "", "", 1, 50);
                if (model.Rows.Count > 0)
                {
                    foreach (OutgoingSecureMessageInfoModel item in model.Rows)
                    {
                        EventLogger.Instance.WriteLog("New secure message log with id " + item.Id.ToString(), EventLogEntryType.Error);
                        InsertSecureMessageLogstoDB(item);
                    }
                }
            }
            catch (Exception ee)
            {
                EventLogger.Instance.WriteLog("GetSecureMessagesLogs - " + ee.ToString(), EventLogEntryType.Error);
            }
        }

        private bool InsertSecureMessageLogstoDB(OutgoingSecureMessageInfoModel item)
        {
            try
            {
                string insertQry = "Insert into MVE.PP_SecureMessagesLogs(LogId, RecipientId, PatientId, IsSent2Repersenative, RepersentativeId, MessageSubject, MessageSentTime, CreatedDateTime, IsResponseSent) Values (NEWID(),'" + item.FromSecureRecipientExternalId + "','" + item.PatientExternalId + "','" + (item.SentToRepresentative == false ? 0 : 1) + "', '" + (item.RepresentativeExternalId == "" ? "0" : item.RepresentativeExternalId) + "','" + item.Subject + "', '" + item.SentDate + "', GETDATE(), 0)";
                if (Utility.InsertRecord(insertQry))
                {
                    EventLogger.Instance.WriteLog("Log saved into database with id " + item.Id.ToString(), EventLogEntryType.Error);
                    SendMessageResponsetoERP(item);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void SendMessageResponsetoERP(OutgoingSecureMessageInfoModel item)
        {
            try
            {
                MarkAsReceivedModel inputmodel = new MarkAsReceivedModel()
                {
                    InternalID = item.Id,
                    Success = true,
                    ResultMessage = "Logs saved succesfully"
                };
                OutgoingSecureMessageProvider.MarkAsReceived(Helpers.GetAuthenticator(), inputmodel);
                Utility.UpdateRecord("Update MVE.PP_SecureMessagesLogs Set IsResponseSent = '1' Where LogId = '" + item.Id.ToString() + "'");
            }
            catch
            {

            }
        }
    }
}
