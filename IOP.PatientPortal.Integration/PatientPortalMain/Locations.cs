﻿using GlobalPortal.Api.Client.Locations;
using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.Locations;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PatientPortalMain
{
    public class Locations
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Locations));
        public void sendLocationInformation()
        {
            PortalQueue pq = null;
            LocationsFacade facade = new LocationsFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<LocationModel> list = facade.GetLocationModel();
                    if (list != null && list.Count > 0)
                    {
                        foreach (LocationModel model in list)
                        {
                            pq = new PortalQueue();
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendLocationInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.ExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = "";
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = LocationProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.Id.ToString() != null)
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Location Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                                pq = null;
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending location's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendLocationInformation", "", ex.Message);
            }
        }
        public void getLocationInformation()
        {
            PortalQueue pq = null;
            LocationsFacade facade = new LocationsFacade();
            LocationModel model = new LocationModel();
            try
            {
                model.ExternalId = "101088";
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetLocationInformation);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = model.ExternalId;
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = model.ExternalId.ToString();
                var success = LocationProvider.Get(Helpers.GetAuthenticator(), model.ExternalId);
                if (success.Id != null)
                {
                    model = (LocationModel)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Locations", "getLocationInformation", "", ex.Message); }
            }
        }
        public void deleteLocationInformation()
        {
            PortalQueue pq = null;
            LocationsFacade facade = new LocationsFacade();
            try
            {
                List<LocationModel> list = facade.getDeleteLocation();
                if (list != null && list.Count > 0)
                {
                    foreach (LocationModel model in list)
                    {
                        pq = new PortalQueue();
                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteLocationInformation);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        pq.ExternalId = model.ExternalId.ToString();
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        var success = LocationProvider.Delete(Helpers.GetAuthenticator(), model.ExternalId);
                        if (success.ToString().ToLower() == "true")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                        pq = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Locations", "DeleteLocationInformation", "", ex.Message); }
            }
        }
        public void getLocationList()
        {
            PortalQueue pq = null;
            UsersFacade facade = new UsersFacade();
            LocationModel model = new LocationModel();
            ListModel<LocationModel> list = new ListModel<LocationModel>();
            try
            {
                string name = "";
                string ID = "";
                bool? active = null;
                string city = "";
                string state = "";
                int page = 1;
                int itemsPerPage = 10;
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetLocationList);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = ID;
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = ID;
                var success = LocationProvider.Search(Helpers.GetAuthenticator(), name, ID, active, city, state, page, itemsPerPage);
                if (success != null)
                {
                    list = (ListModel<LocationModel>)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Locations", "GetLocationList", "", ex.Message); }
            }
        }
    }
}
