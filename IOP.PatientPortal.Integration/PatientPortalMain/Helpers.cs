﻿using GlobalPortal.Api.Client.ClientAuthentication;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace PatientPortalMain
{
    public static class Helpers
    {
        public static String SyncUserName { get; set; }
        public static String SyncPassword { get; set; }

        static Helpers() { }

        public static bool GetSyncUserDetails(string connectionString)
        {
            bool retStatus = false;
            try
            {
                string _Query = "Select SynchronizationUserName, SynchronizationPassword From MVE.PP_AccountDetails Where IsActive = 1";
                using (SqlConnection Con = new SqlConnection(connectionString))
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand(_Query, Con);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        retStatus = true;
                        SyncUserName = rdr["SynchronizationUserName"].ToString();
                        SyncPassword = rdr["SynchronizationPassword"].ToString();
                    }
                    rdr.Close();
                    Con.Close();
                }
            }
            catch 
            {
                SyncUserName = "";
                SyncPassword = "";
                retStatus = false;
            }
            return retStatus;
        }

        public static IServerAuthentication GetAuthenticator()
        {
            var username = SyncUserName; //ConfigurationManager.AppSettings["SyncUsername"];
            var password = SyncPassword; //ConfigurationManager.AppSettings["SyncPassword"];
            var WebApiAddress = ConfigurationManager.AppSettings["WebApiAddress"];
            return GlobalPortalRestClient.GetAuthenticator(username, password, WebApiAddress);
        }

        public static IServerAuthentication GetAuthenticator(string url)
        {
            var username = SyncUserName; //ConfigurationManager.AppSettings["SyncUsername"];
            var password = SyncPassword; //ConfigurationManager.AppSettings["SyncPassword"];
            return GlobalPortalRestClient.GetAuthenticator(username, password, url);
        }

        public static IServerAuthentication GetAuthenticator(string username, string password)
        {
            return GlobalPortalRestClient.GetAuthenticator(username, password);
        }

        public static IServerAuthentication GetAuthenticator(string username, string password, string url)
        {
            return GlobalPortalRestClient.GetAuthenticator(username, password, url);
        }
    }
}
