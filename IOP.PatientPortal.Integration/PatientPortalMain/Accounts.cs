﻿using GlobalPortal.Api.Client.Accounts;
using GlobalPortal.Api.Client.AccountSettings;
using GlobalPortal.Api.Models.Accounts;
using GlobalPortal.Api.Models.AccountSettings;
using log4net;
using PatientPortalFacade;
using System;
using System.Configuration;
using System.Diagnostics;

namespace PatientPortalMain
{
    public class Accounts
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Accounts));

        /// <summary>
        /// Creates a new account in Eye Reach Patients.
        /// </summary>
        public void sendAccountInformation()
        {
            PortalQueue pq = new PortalQueue();
            AccountsFacade accountfacade = new AccountsFacade();
            NewAccountResultModel result = null;
            try
            {
                NewAccountModel model = accountfacade.getNewAccountModel();
                if (model.AccountName != null)
                {
                    var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.NewAccountCreation);
                    pq.IsActive = true;
                    pq.MessageData = jsonObject;
                    pq.PatientId = "Account";
                    pq.ProcessedDate = System.DateTime.Now;
                    pq.ExternalId = model.AccountOwnerExternalId;
                    int requestId = Utility.InsertDataIntoPortalQueue(pq);
                    EventLogger.Instance.WriteLog("Account Information Sending to MVE Portal", EventLogEntryType.Information);
                    result = AccountProvider.Save(ConfigurationManager.AppSettings["WebApiAddress"], model);
                    if (result != null)
                    {
                        accountfacade.updateNewAccountDetails(model, result);
                        Utility.InsertDataIntoPortalQueueResponse(pq);
                        EventLogger.Instance.WriteLog("Account Information Sent to MVE Portal", EventLogEntryType.Information);
                        System.Threading.Thread.Sleep(5000);
                    }
                    else 
                    {
                        EventLogger.Instance.WriteLog("There is some issue while creating a new account at ERP Portal", EventLogEntryType.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                    EventLogger.Instance.WriteLog("Exception raise when sending account information to MVE Portal" + pq.Exception.ToString(), EventLogEntryType.Error);
                }
                else
                { Utility.saveErrorIntoTable("Accounts", "sendAccountInformation", "", ex.Message); }
            }
        }
        /// <summary>
        /// 	Sets account settings.
        /// </summary>
        public void sendAccountSettingsInformation()
        {
            PortalQueue pq = new PortalQueue();
            AccountsFacade accountfacade = new AccountsFacade();
            try
            {
                AccountSettingsModel model = accountfacade.getAccountSettingsModel();
                var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAccountSettingsInformation);
                pq.IsActive = true;
                pq.MessageData = jsonObject;
                pq.PatientId = model.AccountId.ToString();
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = model.AccountId.ToString();
                int requestId = Utility.InsertDataIntoPortalQueue(pq);
                var success = AccountSettingProvider.Save(Helpers.GetAuthenticator(), model);
                if (success.ToString().ToLower() == "true")
                {
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Accounts", "sendAccountSettingsInformation", "", ex.Message); }
            }
        }
        /// <summary>
        /// GET api/AccountSettings
        /// </summary>
        public void getAccountSettingsInformation()
        {
            PortalQueue pq = new PortalQueue();
            AccountSettingsModel model = new AccountSettingsModel();
            AccountsFacade accountfacade = new AccountsFacade();
            try
            {
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetAccountSettingsInformation);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = model.AccountId.ToString();
                pq.ProcessedDate = System.DateTime.Now;
                var success = AccountSettingProvider.Get(Helpers.GetAuthenticator());
                if (success != null)
                {
                    model = (AccountSettingsModel)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Accounts", "sendAccountSettingsInformation", "", ex.Message); }
            }
        }
    }
}
