﻿using GlobalPortal.Api.Client.DoctorAvailabiltySlots;
using GlobalPortal.Api.Client.Doctors;
using GlobalPortal.Api.Models.DoctorAvailabilitySlots;
using GlobalPortal.Api.Models.Doctors;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PatientPortalMain
{
    public class Doctors
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Doctors));
        #region Doctors
        /// <summary>
        /// Creates a new doctor or updates the information of an existent doctor.
        /// </summary>
        public void sendDoctorInformation()
        {
            PortalQueue pq = null;
            DoctorsFacade facade = new DoctorsFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<DoctorModel> list = facade.getDoctorModel();
                    if (list != null && list.Count > 0)
                    {
                        foreach (DoctorModel model in list)
                        {
                            pq = new PortalQueue();
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendDoctorInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.ExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = "";
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = DoctorProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.Id.ToString() != null)
                                {
                                    pq.ExternalId = success.Id.ToString();
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Doctor Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                                pq = null;
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending doctor's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendDoctorInformation", "", ex.Message);
            }
        }
        /// <summary>
        /// Deletes a doctor.
        /// </summary>
        public void deleteDoctor()
        {
            PortalQueue pq = null;
            DoctorsFacade facade = new DoctorsFacade();
            try
            {
                List<DoctorModel> list = facade.getDoctorModelForDeletion();
                if (list != null && list.Count > 0)
                {
                    foreach (DoctorModel model in list)
                    {
                        pq = new PortalQueue();
                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteDoctorInformation);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        pq.ExternalId = model.ExternalId;
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        var success = DoctorProvider.Delete(Helpers.GetAuthenticator(), model.ExternalId);
                        if (success.ToString().ToLower() == "true")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                        pq = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Doctors", "deleteDoctor", "", ex.Message); }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void getDoctorInformation()
        {
            PortalQueue pq = null;
            DoctorsFacade facade = new DoctorsFacade();
            DoctorModel model = new DoctorModel();
            try
            {
                model.ExternalId = "101088";
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetDoctorInformation);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = model.ExternalId;
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = model.ExternalId.ToString();
                //int requestId = Utility.InsertDataIntoPortalQueue(pq);
                var success = DoctorProvider.Get(Helpers.GetAuthenticator(), model.ExternalId);
                if (success != null)
                {
                    model = (DoctorModel)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Doctors", "getDoctorInformation", "", ex.Message); }
            }
        }
        /// <summary>
        /// 	Gets information about a doctor.
        /// </summary>
        #endregion Doctors
        #region DoctorAvailabiltySlots
        /// <summary>
        /// 		Creates a new doctor availabilty slots or updates an existent doctor availabilty slots' information.
        /// </summary>
        public void sendDoctorAvailabilitySlotInformation()
        {
            PortalQueue pq = null;
            DoctorsFacade facade = new DoctorsFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<DoctorAvailabilitySlotModel> list = facade.getDoctorAvailabiltySlotModel();
                    if (list != null && list.Count > 0)
                    {
                        foreach (DoctorAvailabilitySlotModel model in list)
                        {
                            pq = new PortalQueue();
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendDoctorAvailabilitySlotInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.ExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId.ToString();
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = DoctorAvailabiltySlotProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.ToString().ToLower() == "true")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                }
                                pq = null;
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                            }
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendDoctorAvailablitySlotInformation", "", ex.Message);
            }
        }
        /// <summary>
        /// Deletes a doctor availabilty slots.
        /// </summary>
        public void deleteDoctorAvailabilitySlotInformation()
        {
            PortalQueue pq = null;
            DoctorsFacade facade = new DoctorsFacade();
            try
            {
                List<DoctorAvailabilitySlotModel> list = facade.getDoctorAvailabiltySlotForDeletion();
                if (list != null && list.Count > 0)
                {
                    foreach (DoctorAvailabilitySlotModel model in list)
                    {
                        pq = new PortalQueue();
                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteDoctorAvailabilitySlotInformation);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        pq.ExternalId = model.ExternalId.ToString();
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        var success = DoctorAvailabiltySlotProvider.Delete(Helpers.GetAuthenticator(), model.ExternalId);
                        if (success.ToString().ToLower() == "true")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                        pq = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Doctors", "deleteDoctorAvailabilitySlotInformation", "", ex.Message); }
            }
        }
        #endregion DoctorAvailabiltySlots
    }
}
