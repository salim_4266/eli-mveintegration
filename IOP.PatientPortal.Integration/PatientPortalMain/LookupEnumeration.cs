﻿
namespace PatientPortalMain
{
    public enum ActionTypeLookUps
    {
        SecureMessagesrequestfromPhysiciantoPatient = 1,
        SecureMessageresponsetothePatientrequest = 2,
        AmendmentResponse = 3,
        ProtectedHealthInformation = 8,
        PatientActivation = 14,
        PatientRepresentativeActivation = 15,
        Appointments = 16,
        EducationResources = 17,
        AppointmentsResponsetothePatientsRequest = 19,
        OcularMedications = 20,
        DemographicsDump = 23,
        UpdateDemographics = 25,
        PatientEducationLink = 10
    }
}
