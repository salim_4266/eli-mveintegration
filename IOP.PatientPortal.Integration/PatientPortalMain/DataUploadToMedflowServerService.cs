﻿using log4net;
using PatientPortalFacade;
using System;
using System.Net;

namespace PatientPortalMain
{
    public class DataUploadToMedflowServerService
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DataUploadToMedflowServerService));
        #region  Appoinments
        public void PostPatientAppointments()
        {
            //PortalQueue pq = new PortalQueue();
            //string result = string.Empty;
            //try
            //{
            //    AppointmentFacade apptfacade = new AppointmentFacade();
            //    List<AppointmentModel> list = apptfacade.getPatientAppointments();
            //    if (list != null && list.Count > 0)
            //    {
            //        foreach (AppointmentModel obj in list)
            //        {
            //            if (obj != null)
            //            {
            //                obj.ClientKey = CommonObjects.STR_CLIENT_KEY;
            //                var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            //                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.OcularMedications);
            //                pq.IsActive = true;
            //                pq.MessageData = jsonObject;
            //                pq.PatientId = obj.PatientAccountId;
            //                pq.ProcessedDate = obj.ProcessedDate;
            //                int requestId = Utility.InsertDataIntoPortalQueue(pq);
            //                string url = CommonObjects.LINK_BASE_API_URL + CommonObjects.LINK_POST_PATIENT__APPOINTMENTS;
            //                obj.RequestId = requestId;
            //                pq.ExternalId = obj.RequestId;
            //                jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            //                result = makeCallToWebAPI(result, jsonObject, url);
            //                if (result.ToLower() == EnumTypes.ResponseStatus.Success.GetStringValue().ToLower())
            //                {
            //                    Utility.InsertDataIntoPortalQueueResponse(pq);
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (WebException ex)
            //{
            //    _logger.Error(ex.Message);
            //    string responsetext = string.Empty;
            //    if (ex.Response != null)
            //    {
            //        var responsstream = ex.Response.GetResponseStream();
            //        if (responsstream != null)
            //        {
            //            using (var reader = new StreamReader(responsstream))
            //            {
            //                responsetext = reader.ReadToEnd();
            //                pq.Exception = responsetext;
            //                Utility.InsertDataIntoPortalQueueException(pq);
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _logger.Error(ex.Message);
            //    Utility.saveErrorIntoTable("DataUploadToMedflowServerService", "POSTPatientOcularSystemicMedications", "", ex.Message);
            //}
        }
        //existing appointment..........rescheduling
        #endregion
        private string makeCallToWebAPI(string result, string jsonObject, string url)
        {
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                result = webclient.UploadString(url, "POST", jsonObject);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message + "JSON OBJECT : " + jsonObject);
                Utility.saveErrorIntoTable("DataUploadToMedflowServerService", "makeCallToWebAPI", jsonObject, ex.Message);
                throw;
            }
            return result;
        }
    }
}
