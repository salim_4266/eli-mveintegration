﻿
namespace PatientPortalMain
{
    public class ResponseEntity
    {
        public string ResponseType { get; set; }
        public string ExternalId { get; set; }
    }

    public class EmailEntity
    {
        public string PracticeToken { get; set; }
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string UserType { get; set; }
    }
    public class PatientEntity
    {
        public string PracticeToken { get; set; }
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public string LocationID { get; set; }
        public string EmailAddresses { get; set; }
        public string PhoneNumbers { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string BirthDate { get; set; }
        public string Notes { get; set; }
        public string LanguageName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsSendEmailViaAPI { get; set; }
    }
}
