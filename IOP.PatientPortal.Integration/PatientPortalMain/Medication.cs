﻿using GlobalPortal.Api.Models.PatientMedications;
using GlobalPortal.Api.Client.PatientMedications;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PatientPortalMain
{
    public class Medication
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Doctors));
        #region Medication
        /// <summary>
        /// Creates a new doctor or updates the information of an existent doctor.
        /// </summary>
        public void sendNewMedication()
        {
            PortalQueue pq = null;
            MedicationFacade facade = new MedicationFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<PatientMedicationUpdateModel> list = facade.getNewMedicationModel();
                    if (list != null && list.Count > 0)
                    {
                        foreach (PatientMedicationUpdateModel model in list)
                        {
                            pq = new PortalQueue();
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendMedicationInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.PatientExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = PatientMedicationsProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.ExternalId.ToLower() != "")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Clinical Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                                pq = null;
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending doctor's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("medication", "sendDiscontinueMedication", "", ex.Message);
            }
        }

        public void sendDiscontinueMedication()
        {
            PortalQueue pq = null;
            MedicationFacade facade = new MedicationFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<PatientMedicationUpdateModel> list = facade.getDiscontinueExistingMedicationModel();
                    if (list != null && list.Count > 0)
                    {
                        foreach (PatientMedicationUpdateModel model in list)
                        {
                            pq = new PortalQueue();
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model.ExternalId);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteMedicationInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = "";
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = PatientMedicationsProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.ToString().ToLower() == "true")
                                {
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("Clinical Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                                pq = null;
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending Medicine's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("medication", "sendDiscontinueMedication", "", ex.Message);
            }
        }
        
        #endregion
        
    }
}
