﻿using GlobalPortal.Api.Client.Users;
using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.Users;
using log4net;
using PatientPortalFacade;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PatientPortalMain
{
    public class Users
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Users));
        /// <summary>
        /// Adds a new user or updates information on an existing user.
        /// </summary>
        public void sendUserInformation()
        {
            PortalQueue pq = new PortalQueue();
            UsersFacade facade = new UsersFacade();
            try
            {
                bool status = true;
                while (status)
                {
                    List<UserModel> list = facade.GetUserModel();
                    if (list != null && list.Count > 0)
                    {
                        foreach (UserModel model in list)
                        {
                            var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendUserInformation);
                            pq.IsActive = true;
                            pq.MessageData = jsonObject;
                            pq.PatientId = model.ExternalId;
                            pq.ProcessedDate = System.DateTime.Now;
                            pq.ExternalId = model.ExternalId;
                            int requestId = Utility.InsertDataIntoPortalQueue(pq);
                            try
                            {
                                var success = UserProvider.Save(Helpers.GetAuthenticator(), model);
                                if (success.Id.ToString() != null)
                                {
                                    pq.ExternalId = success.Id.ToString();
                                    Utility.InsertDataIntoPortalQueueResponse(pq);
                                    EventLogger.Instance.WriteLog("User Id - " + pq.ExternalId + " Sent to MVE", EventLogEntryType.Information);
                                }
                            }
                            catch (Exception ex)
                            {
                                pq.Exception = ex.Message;
                                Utility.InsertDataIntoPortalQueueException(pq);
                                EventLogger.Instance.WriteLog("Ëxception raise while sending user's information to MVE, " + pq.Exception, EventLogEntryType.Error);
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("Forms", "sendUserInformation", "", ex.Message);
            }
        }
        /// <summary>
        /// 	Deletes a user.
        /// </summary>
        public void deleteUserInformation()
        {
            PortalQueue pq = null;
            UsersFacade facade = new UsersFacade();
            try
            {
                List<UserModel> list = facade.getDeleteUser();
                if (list != null && list.Count > 0)
                {
                    foreach (UserModel model in list)
                    {
                        pq = new PortalQueue();
                        var jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteUserInformation);
                        pq.IsActive = true;
                        pq.MessageData = jsonObject;
                        pq.PatientId = model.ExternalId;
                        pq.ProcessedDate = System.DateTime.Now;
                        pq.ExternalId = model.ExternalId.ToString();
                        int requestId = Utility.InsertDataIntoPortalQueue(pq);
                        var success = UserProvider.Delete(Helpers.GetAuthenticator(), model.ExternalId);
                        if (success.ToString().ToLower() == "true")
                        {
                            Utility.InsertDataIntoPortalQueueResponse(pq);
                        }
                        pq = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Users", "deleteUserInformation", "", ex.Message); }
            }
        }
        /// <summary>
        /// Gets information about an user.
        /// </summary>
        public void getUsersInformation()
        {
            PortalQueue pq = null;
            UsersFacade facade = new UsersFacade();
            UserModel model = new UserModel();
            try
            {
                model.ExternalId = "101088";
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetUserInformation);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = model.ExternalId;
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = model.ExternalId.ToString();
                var success = UserProvider.Get(Helpers.GetAuthenticator(), model.ExternalId);
                if (success != null)
                {
                    model = (UserModel)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Users", "getUsersInformation", "", ex.Message); }
            }
        }
        /// <summary>
        /// Returns a list of users.
        /// </summary>
        public void getUserList()
        {
            PortalQueue pq = null;
            UsersFacade facade = new UsersFacade();
            UserModel model = new UserModel();
            ListModel<UserModel> list = new ListModel<UserModel>();
            try
            {
                string username = "";
                bool? active = null;
                string firstName = "";
                string lastName = "";
                int page = 1;
                int itemsPerPage = 10;
                pq = new PortalQueue();
                pq.ActionTypeLookupId = Convert.ToInt32(EnumTypes.ActionTypeLookUps.GetUserList);
                pq.IsActive = true;
                pq.MessageData = "";
                pq.PatientId = username;
                pq.ProcessedDate = System.DateTime.Now;
                pq.ExternalId = username;
                var success = UserProvider.Search(Helpers.GetAuthenticator(), username, active, firstName, lastName, page, itemsPerPage);
                if (success != null)
                {
                    list = (ListModel<UserModel>)success;
                    Utility.InsertDataIntoPortalQueueResponse(pq);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                if (ex.Source == CommonObjects.STR_EXCEPTION_SOURCE)
                {
                    pq.Exception = ex.Message;
                    Utility.InsertDataIntoPortalQueueException(pq);
                }
                else
                { Utility.saveErrorIntoTable("Users", "getUserList", "", ex.Message); }
            }
        }
    }
}
