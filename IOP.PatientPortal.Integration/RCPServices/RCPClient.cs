﻿using Newtonsoft.Json;
using PatientPortalFacade;
using RCPServices.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace RCPServices
{
    public class RCPClient
    {
        private string path2Directory { get { return ConfigurationManager.AppSettings["FolderPath"].ToString(); } }

        public int PushBatch()
        {
            string jwtToken = string.Empty;
            string ready2Processjson = string.Empty;
            string path2jsonDirectory = string.Empty;
            string path2jsonProcessedDirectory = string.Empty;

            EventLogger.Instance.WriteLog("Batch processing start", EventLogEntryType.Information);

            if (!Directory.Exists(path2Directory))
            {
                EventLogger.Instance.WriteLog("Seems like directory is not exists with the path provided.", EventLogEntryType.Error);
                return 0;
            }

            path2jsonDirectory = path2Directory + @"\Queue\";
            path2jsonProcessedDirectory = path2Directory + @"\Processed\";
            if (!Directory.Exists(path2jsonDirectory))
                Directory.CreateDirectory(path2jsonDirectory);
            if (!Directory.Exists(path2jsonProcessedDirectory))
                Directory.CreateDirectory(path2jsonProcessedDirectory);

            ACIPushCredential RCPCredential = ACIDefault.GetPushCredential();

            if (RCPCredential.batchId == null && RCPCredential.batchId == "0")
            {
                EventLogger.Instance.WriteLog("ACI_defaults batchid should not be null or zero.", EventLogEntryType.Error);
                return 0;
            }
            if (String.IsNullOrEmpty(RCPCredential.username))
            {
                EventLogger.Instance.WriteLog("ACI_defaults username value is null.", EventLogEntryType.Error);
                return 0;
            }
            if (String.IsNullOrEmpty(RCPCredential.role))
            {
                EventLogger.Instance.WriteLog("ACI_defaults role value is null.", EventLogEntryType.Error);
                return 0;
            }
            if (String.IsNullOrEmpty(RCPCredential.productkey))
            {
                EventLogger.Instance.WriteLog("ACI_defaults productkey value is null.", EventLogEntryType.Error);
                return 0;
            }
            if (String.IsNullOrEmpty(RCPCredential.pairkey))
            {
                EventLogger.Instance.WriteLog("ACI_defaults pairkey value is null.", EventLogEntryType.Error);
                return 0;
            }
            System.Threading.Thread.Sleep(2000);
            EventLogger.Instance.WriteLog("ACI Json Default initialized", EventLogEntryType.Information);

            jwtToken = RCPToken.NewToken(RCPCredential.username, RCPCredential.role, RCPCredential.productkey, RCPCredential.pairkey);
            if (jwtToken == string.Empty)
            {
                EventLogger.Instance.WriteLog("there is some issue while creating jwt token", EventLogEntryType.Warning);
                return 0;
            }

            System.Threading.Thread.Sleep(2000);
            EventLogger.Instance.WriteLog("New JWT Token created", EventLogEntryType.Information);

            System.Threading.Thread.Sleep(2000);
            string _batchNumber = "";
            try
            {
                EventLogger.Instance.WriteLog("ACI_INSERT_ACI_JSON_MASTER Executing", EventLogEntryType.Information);

                using (SqlConnection sqlConnection = new SqlConnection(RCPUtility.connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand cmd_createJson = new SqlCommand("ACI_INSERT_ACI_JSON_MASTER", sqlConnection))
                    {
                        cmd_createJson.CommandType = CommandType.StoredProcedure;
                        cmd_createJson.Parameters.AddWithValue("@SortIns", 0);
                        cmd_createJson.ExecuteNonQuery();
                    }
                    sqlConnection.Close();
                }
                EventLogger.Instance.WriteLog("ACI_INSERT_ACI_JSON_MASTER Executed", EventLogEntryType.Information);

                DataTable aciJsonMasterTable = new DataTable();
                using (SqlConnection sqlConnection = new SqlConnection(RCPUtility.connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand cmd_validjson = new SqlCommand("ACI_Check_ISVALIDJSON", sqlConnection))
                    {
                        cmd_validjson.CommandType = CommandType.StoredProcedure;
                        cmd_validjson.Parameters.AddWithValue("@SortIns", 0);
                        SqlDataAdapter _dAdatper = new SqlDataAdapter(cmd_validjson);
                        _dAdatper.Fill(aciJsonMasterTable);
                    }
                    sqlConnection.Close();
                }
                System.Threading.Thread.Sleep(1000);
                string readyJson = "";
                string aciMasterkey = "";
                string aciJsonString = "";
                for (int i = 0; i < aciJsonMasterTable.Rows.Count; i++)
                {
                    aciMasterkey = aciJsonMasterTable.Rows[i]["ACIJSONMASTERKEY"].ToString();
                    aciJsonString = aciJsonMasterTable.Rows[i]["JSONNEW"].ToString();
                    _batchNumber = aciJsonMasterTable.Rows[i]["BATCHNO"].ToString();
                    bool test = RCPUtility.IsValidJson(aciJsonString);
                    if (test)
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(RCPUtility.connectionString))
                        {
                            sqlConnection.Open();
                            using (SqlCommand cmd_updatevalidjson = new SqlCommand("UPDATE ACI_JSON_MASTER SET ISVALIDJSON = 'T', BATCHNO = " + _batchNumber + " WHERE ACIJSONMASTERKEY ='" + aciMasterkey + "'", sqlConnection))
                            {
                                if (cmd_updatevalidjson.ExecuteNonQuery() > 0)
                                {
                                    if (String.IsNullOrEmpty(readyJson))
                                    {
                                        readyJson += aciJsonString;
                                    }
                                    else
                                    {
                                        readyJson += "," + aciJsonString;
                                    }
                                }
                            }
                            sqlConnection.Close();
                        }
                    }
                    else
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(RCPUtility.connectionString))
                        {
                            sqlConnection.Open();
                            using (SqlCommand cmd_updatevalidjson = new SqlCommand("UPDATE ACI_JSON_MASTER SET ISVALIDJSON = 'F', BATCHNO = " + _batchNumber + " WHERE ACIJSONMASTERKEY ='" + aciMasterkey + "'", sqlConnection))
                            {
                                cmd_updatevalidjson.ExecuteNonQuery();
                            }
                            sqlConnection.Close();
                        }
                    }
                }
                if (readyJson != "")
                {
                    ready2Processjson = "[" + readyJson + "]";
                }

                System.Threading.Thread.Sleep(3000);
                if (ready2Processjson == string.Empty)
                {
                    EventLogger.Instance.WriteLog("There is nothing remaining for processing 2 RCP.", EventLogEntryType.Warning);
                    return 0;
                }
                string date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff");
                string _serviceUrl = RCPCredential.serviceUrl + RCPCredential.batchId + "&RefBatchID=" + RCPCredential.batchId + "&Priority=N&BatchCreateDate=" + date;
                RCPUtility.CreateFile(path2jsonDirectory, ready2Processjson, _batchNumber);
                System.Threading.Thread.Sleep(2000);
                EventLogger.Instance.WriteLog(RCPCredential.batchUrl + _serviceUrl, EventLogEntryType.Information);
                EventLogger.Instance.WriteLog(path2jsonDirectory + "/" + _batchNumber + ".json", EventLogEntryType.Information);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(RCPCredential.batchUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("ProductKey", RCPCredential.productkey);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                    var content = new StringContent(ready2Processjson.ToString(), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(_serviceUrl, content).Result;
                    EventLogger.Instance.WriteLog(_batchNumber + " is processing to RCP", EventLogEntryType.Information);
                    if (response.IsSuccessStatusCode)
                    {
                        EventLogger.Instance.WriteLog("Batch processed successfully " + _batchNumber, EventLogEntryType.Information);
                        try
                        {
                            File.Move(path2jsonDirectory + _batchNumber + ".json", path2jsonProcessedDirectory + _batchNumber + ".json");
                        }
                        catch
                        { }
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        ResponseJson json = JsonConvert.DeserializeObject<ResponseJson>(responseString);
                        using (SqlConnection sqlConnection = new SqlConnection(RCPUtility.connectionString))
                        {
                            sqlConnection.Open();
                            using (SqlCommand cmd_insert_response = new SqlCommand("ACI_Insert_JSON_Response", sqlConnection))
                            {
                                cmd_insert_response.CommandType = CommandType.StoredProcedure;
                                cmd_insert_response.Parameters.AddWithValue("@JSONFileName", _batchNumber + ".json");
                                cmd_insert_response.Parameters.AddWithValue("@ResponseCode", json.responseCode);
                                cmd_insert_response.Parameters.AddWithValue("@ResponseMessage", json.responseMessage);
                                cmd_insert_response.Parameters.AddWithValue("@CompleteResponse", responseString);
                                cmd_insert_response.Parameters.AddWithValue("@BatchID", RCPCredential.batchId);
                                cmd_insert_response.ExecuteReader();
                            }
                            sqlConnection.Close();
                        }
                    }
                    else
                    {
                        EventLogger.Instance.WriteLog(_batchNumber + " - " + "response error #" + response.StatusCode, EventLogEntryType.Information);
                    }
                    System.Threading.Thread.Sleep(2000);
                    EventLogger.Instance.WriteLog(_batchNumber + " has processed to RCP", EventLogEntryType.Information);
                }
				return 1;
			}
			catch (Exception ee)
            {
                EventLogger.Instance.WriteLog("Post batch issue " + ee.Message.ToString() + "\n\n" + ee.InnerException.ToString(), EventLogEntryType.Error);
            }
			return 0;
        }

        public void PullBatch()
        {
            EventLogger.Instance.WriteLog("PullBackBatchStatus event start", EventLogEntryType.Information);
            ACIPullCredential RCPCredential = ACIDefault.GetPullCredential(); 

            DataTable rcpBatches = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(RCPUtility.connectionString))
            {
                using (SqlCommand cmd_getbatchid = new SqlCommand("ACI_Get_BatchID", sqlConnection))
                {
                    cmd_getbatchid.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd_getbatchid);
                    da.Fill(rcpBatches);
                }
            }

            if (rcpBatches.Rows.Count > 0)
            {
                EventLogger.Instance.WriteLog("PullBackBatchStatus start executing", EventLogEntryType.Information);
                string jwtToken = RCPToken.NewToken(RCPCredential.Username, RCPCredential.Role, RCPCredential.Productkey, RCPCredential.Pairkey);
                string serviceUrl = RCPCredential.BatchUrlGetResult;
                List<ACIBatch> batches = new List<ACIBatch>();
                List<string> batchList = new List<string>();
                foreach (DataRow dr in rcpBatches.Rows)
                {
                    batches.Add(new ACIBatch(Convert.ToString(dr["BatchID"]), (string)dr["JsonFileName"]));
                    batchList.Add(Convert.ToString(dr["BatchID"]));
                }


                if (batches.Count > 0)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(RCPCredential.BaseUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                        client.DefaultRequestHeaders.Add("ProductKey", RCPCredential.Productkey);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                        var content = new StringContent("[" + string.Join(",", batchList) + "]", Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(serviceUrl, content).Result;
                        string result = response.Content.ReadAsStringAsync().Result;
                        string jsonString = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1] { '"' });
                        ResponseJson resultResponse = JsonConvert.DeserializeObject<ResponseJson>(jsonString);
                        if (resultResponse.result != null && resultResponse.result.Any())
                        {
                            foreach (var itemResult in resultResponse.result)
                            {
                                DBOperation.InsertJsonResult(itemResult);
                            }
                        }
                        System.Threading.Thread.Sleep(3000);
                        EventLogger.Instance.WriteLog("PullBackBatchStatus start completed", EventLogEntryType.Information);
                    }
                }
            }
        }
    }
}
