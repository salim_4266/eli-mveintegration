﻿using System.Data.SqlClient;

namespace RCPServices
{
    public class ACI_Defaults
    {
        public string batchUrlGetResult = "v1/GetBatchResult/GetResult";

        public string batcURLRejectBatch = "v1/RejectBatch/RejectBatch/";
        public string batchUrl { get; set; }
        public string baseUrl { get; set; }
        public string baseUrl2 { get; set; }
        public string baseurl3 { get; set; }
        public string baseurl4 { get; set; }
        public string productkey { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public string aci_issuer { get; set; }
        public string expiretime { get; set; }
        public string pairkey { get; set; }
        public string aci_audience { get; set; }
        public string serviceUrl { get; set; }
        public string batchId { get; set; }
        public ACI_Defaults(string _connectionString)
        {
            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                if (_connection.State == System.Data.ConnectionState.Closed)
                {
                    _connection.Open();
                }
                using (SqlCommand cmd = new SqlCommand("ACI_GET_JSON_DEFAULTS", _connection))
                {
                    SqlDataReader _reader = cmd.ExecuteReader();
                    if (_reader.Read())
                    {
                        batchUrl = _reader["ACI_BaseURL"].ToString();
                        baseUrl = _reader["ACI_Baseurl_PublishDEP"].ToString();
                        baseUrl2 = _reader["ACI_Baseurl_GetPublishDEPResult"].ToString();
                        baseurl3 = _reader["ACI_Baseurl_GetMessageDeliveryNotification"].ToString();
                        baseurl4 = _reader["ACI_Baseurl_RetriveInboundDEPData"].ToString();
                        productkey = _reader["ACI_ProductKey"].ToString();
                        username = _reader["ACI_USERNAME"].ToString();
                        role = _reader["ACI_ROLE"].ToString();
                        pairkey = _reader["ACI_PairKey"].ToString();
                        aci_issuer = _reader["ACI_Issuer"].ToString();
                        aci_audience = _reader["ACI_Audience"].ToString();
                        expiretime = _reader["ACI_Expires"].ToString();
                        serviceUrl = _reader["ACI_ServiceURL"].ToString();
                        batchId = _reader["ACI_BatchID"].ToString();
                    }
                }
                _connection.Close();
            }
        }
    }
}
