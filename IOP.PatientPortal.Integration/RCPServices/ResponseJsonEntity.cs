﻿using System.Collections.Generic;

namespace RCPServices
{
    public class ResponseJson
    {
        public int? responseCode { get; set; }
        public string responseMessage { get; set; }
        public List<Result> result { get; set; }
    }

    public class ResourcesStatus
    {
        public string resourceId { get; set; }
        public int statusCode { get; set; }
        public string message { get; set; }
    }

    public class Result
    {
        public int batchId { get; set; }
        public int responseCode { get; set; }
        public string responseMessage { get; set; }
        public List<ResourcesStatus> resourcesStatus { get; set; }
    }


}
