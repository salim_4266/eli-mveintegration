﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Xml;

namespace RCPServices
{
    public class RCPToken
    {
        public string NewToken(string _userName, string _role, string _productKey, string _pairkey)
        {
            string _tokenDetail = "";
            try
            {
                string xmlString = _pairkey;
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048);
                RSAParameters rsaParameters = GetRsaParametersFromXmlString(xmlString);
                rsa.ImportParameters(rsaParameters);
                DateTime now = DateTime.UtcNow;
                Claim[] claims = new[]
                 {
                 new Claim("user_metadata","{\"Username\":\""+_userName+"\",\"Role\":\""+_role+"\",\"ProductKey\":\""+_productKey+"\"}")
             };
                JwtSecurityToken jwt = new JwtSecurityToken(
                    issuer: "http://www.emrxxxxxxsample.com",
                    audience: "http://localhost:54288/",
                    claims: claims,
                    expires: now.AddMinutes(600),
                    signingCredentials: new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256Signature)
                    );
                jwt.Payload["iat"] = now;
                _tokenDetail = new JwtSecurityTokenHandler().WriteToken(jwt);
            }
            catch
            {
                _tokenDetail = "";
            }
            return _tokenDetail;
        }

        private RSAParameters GetRsaParametersFromXmlString(string xmlString)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            XmlNode nodeRsaKeyValue = xmlDoc.DocumentElement;
            XmlNodeList xnList = nodeRsaKeyValue.ChildNodes;
            Dictionary<string, string> dicKeyData = new Dictionary<string, string>();
            foreach (XmlNode node in xnList)
            {
                dicKeyData.Add(node.Name, node.InnerText);
            }
            RSAParameters rsaParameters = new RSAParameters();
            string modulus = dicKeyData["Modulus"];
            string exponent = dicKeyData["Exponent"];
            rsaParameters.Modulus = Convert.FromBase64String(dicKeyData["Modulus"]);
            rsaParameters.Exponent = Convert.FromBase64String(dicKeyData["Exponent"]);
            rsaParameters.P = Convert.FromBase64String(dicKeyData["P"]);
            rsaParameters.Q = Convert.FromBase64String(dicKeyData["Q"]);
            rsaParameters.DP = Convert.FromBase64String(dicKeyData["DP"]);
            rsaParameters.DQ = Convert.FromBase64String(dicKeyData["DQ"]);
            rsaParameters.InverseQ = Convert.FromBase64String(dicKeyData["InverseQ"]);
            rsaParameters.D = Convert.FromBase64String(dicKeyData["D"]);
            return rsaParameters;
        }
    }
}
