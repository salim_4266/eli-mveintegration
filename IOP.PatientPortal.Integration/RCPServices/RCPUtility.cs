﻿using Newtonsoft.Json.Linq;
using System.IO;

namespace RCPServices
{
    public sealed class RCPUtility
    {
        private RCPUtility()
        {

        }

        public static string connectionString { get; set; }

        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || (strInput.StartsWith("[") && strInput.EndsWith("]")))
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static void CreateFile(string FolderPath, string Json, string BatchNumber)
        {
            try
            {
                string path = FolderPath + "\\" + BatchNumber + ".json";
                StreamWriter sr = new StreamWriter(path, true);
                sr.Write(Json);
                sr.Close();
            }
            catch { }
        }
    }
}
