﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.PatientMedications;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PatientPortalFacade
{
    public class MedicationFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(MedicationFacade));
        #region Doctors
        public List<PatientMedicationUpdateModel> getNewMedicationModel()
        {
            List<PatientMedicationUpdateModel> list = new List<PatientMedicationUpdateModel>();
            PatientMedicationUpdateModel model = null;
            string procedureName = "[MVE].[PP_GetNewMedication]";
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                int columnIndex = 0;
                parameters[columnIndex++] = new SqlParameter("@MedicationActionTypeId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendMedicationInformation));
                parameters[columnIndex++] = new SqlParameter("@EncounterActionTypeId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.UpdateAppointmentInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoMedicationObject(dr, true);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("MedicationFacade", "getNewMedicationModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<PatientMedicationUpdateModel> getDiscontinueExistingMedicationModel()
        {
            List<PatientMedicationUpdateModel> list = new List<PatientMedicationUpdateModel>();
            PatientMedicationUpdateModel model = null;
            string procedureName = "[MVE].[PP_GetDiscontinueExistingMedication]";
            try
            {
                SqlParameter[] parameters = new SqlParameter[2];
                int columnIndex = 0;
                parameters[columnIndex++] = new SqlParameter("@MedicationActionTypeId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendMedicationInformation));
                parameters[columnIndex++] = new SqlParameter("@EncounterActionTypeId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.UpdateAppointmentInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoMedicationObject(dr, false);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("MedicationFacade", "getNewMedicationModel", procedureName, ex.Message);
            }
            return list;
        }
        private PatientMedicationUpdateModel SetValueIntoMedicationObject(DataRow dtRow, bool isActive)
        {
            PatientMedicationUpdateModel model = new PatientMedicationUpdateModel();
            try
            {
                if (dtRow != null)
                {
                    model.Active = dtRow["Active"].ToString() == "0" ? false : true;
                    model.ExternalId = dtRow["ExternalId"].ToString();
                    model.PatientExternalId = dtRow["PatientExternalId"].ToString();
                    model.DoctorExternalId = dtRow["DoctorExternalId"].ToString();
                    model.Name = dtRow["Name"].ToString();
                    model.Instructions = dtRow["Instructions"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Doctors
    }
}
