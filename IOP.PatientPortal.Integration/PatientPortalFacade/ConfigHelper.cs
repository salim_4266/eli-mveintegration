﻿using System;

namespace PatientPortalFacade
{
    /// <summary>
    /// this class contains properties for all the keys specified in app.config file
    /// </summary>
    public class ConfigHelper
    {

        public static string ConnectionString
        {
            get
            {
                return GeneralHelper.ConnectionString(AppSettingsKeys.ConnectionString);
            }
        }

        public static string SyncInterval
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.SyncInterval);
            }
        }

        public static string MedFlowScheduledTime
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.MedFlowScheduledTime);
            }
        }

        public static string ACIServices
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.ACIServices);
            }
        }

        public static string ERPServices
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.ERPServices);
            }
        }

        public static string MedFlowScheduledEndTime
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.MedFlowScheduledEndTime);
            }
        }

        public static string IntervalMode
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.IntervalMode);
            }
        }

        public static bool CleanHealthInformation
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.CleanHealthInformation) != null ? Convert.ToBoolean(GeneralHelper.AppSetting(AppSettingsKeys.CleanHealthInformation)) : false;
            }
        }


        public static bool CleanEducationInformation
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.CleanEducationInformation) != null ? Convert.ToBoolean(GeneralHelper.AppSetting(AppSettingsKeys.CleanEducationInformation)) : false;
            }
        }

        public static bool IsPatientSync
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.IsPatientSync) != null ? Convert.ToBoolean(GeneralHelper.AppSetting(AppSettingsKeys.IsPatientSync)) : false;
            }
        }

    }
}
