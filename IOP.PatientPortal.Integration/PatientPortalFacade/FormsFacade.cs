﻿using GlobalPortal.Api.Models.ExamFieldMappingDetails;
using GlobalPortal.Api.Models.ExamFieldMappings;
using GlobalPortal.Api.Models.Insurances;
using GlobalPortal.Api.Models.Medications;
using GlobalPortal.Api.Models.Prefixes;
using GlobalPortal.Api.Models.Suffixes;
using GlobalPortal.Api.Models.Surgeries;
using GlobalPortal.Api.Models.SocialHistoryAlcohols;
using AppointmentAPI.DAO;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using GlobalPortal.Api.Models.DosageForms;
using GlobalPortal.Api.Models.FamilyHistories;
using GlobalPortal.Api.Models.HealthReviews;
using GlobalPortal.Api.Models.SocialHistoryDrugs;
using GlobalPortal.Api.Models.Allergies;
using GlobalPortal.Api.Models.PortalPatients;
using GlobalPortal.Api.Models.Forms;
using GlobalPortal.Api.Models.FamilyRelationships;
using GlobalPortal.Api.Models.FamilyMembers;
using GlobalPortal.Api.Models;
namespace PatientPortalFacade
{
    public class FormsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(FormsFacade));
        #region Allergies
        public List<AllergyModel> GetAllergyModel()
        {
            List<AllergyModel> list = new List<AllergyModel>();
            AllergyModel model = null;
            string procedureName = "pp_GetAllergydetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAllergiesInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAllergyObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "GetAllergyModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<AllergyModel> getAllergyModelForDeletion()
        {
            List<AllergyModel> list = new List<AllergyModel>();
            AllergyModel model = null;
            string procedureName = "pp_GetMedicationDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAllergyObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "GetAllergyModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private AllergyModel SetValueIntoAllergyObject(DataRow dtRow)
        {
            AllergyModel model = new AllergyModel();
            try
            {
                if (dtRow != null)
                {
                    model.Allergy = dtRow["Allergy"] == DBNull.Value ? "" : dtRow["Allergy"].ToString();
                    model.Type = dtRow["Type"] == DBNull.Value ? "" : dtRow["Type"].ToString();
                    model.Reaction = dtRow["Reaction"] == DBNull.Value ? "" : dtRow["Reaction"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Allergies
        #region DosageForms
        public List<DosageFormModel> GetDosageFormModel()
        {
            List<DosageFormModel> list = new List<DosageFormModel>();
            DosageFormModel model = null;
            string procedureName = "PP_GetDosageFormDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendDosageFormInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDosageFormObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "GetDosageFormModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<DosageFormModel> getDosageFormModelForDeletion()
        {
            List<DosageFormModel> list = new List<DosageFormModel>();
            DosageFormModel model = null;
            string procedureName = "pp_getDosageFormdetails";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDosageFormObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getDosageFormModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private DosageFormModel SetValueIntoDosageFormObject(DataRow dtRow)
        {
            DosageFormModel model = new DosageFormModel();
            try
            {
                if (dtRow != null)
                {
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = Convert.ToInt32(dtRow["SortOrder"].ToString());
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion DosageForms
        #region FamilyHistory/FamilyRelationship/FamilyMember
        public List<FamilyHistoryModel> getFamilyHistoryModel()
        {
            List<FamilyHistoryModel> list = new List<FamilyHistoryModel>();
            FamilyHistoryModel model = null;
            string procedureName = "PP_GetFamilyHistoryDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendFamilyHistoryInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyHistoryObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "GetFamilyHistoryModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<FamilyHistoryModel> getFamilyHistoryModelForDeletion()
        {
            List<FamilyHistoryModel> list = new List<FamilyHistoryModel>();
            FamilyHistoryModel model = null;
            string procedureName = "pp_getFamilyHistorydetails";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyHistoryObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "GetFamilyHistoryModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private FamilyHistoryModel SetValueIntoFamilyHistoryObject(DataRow dtRow)
        {
            FamilyHistoryModel model = new FamilyHistoryModel();
            try
            {
                if (dtRow != null)
                {
                    model.Disease = dtRow["Disease"] == DBNull.Value ? "" : dtRow["Disease"].ToString();
                    model.SortOrder = dtRow["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(dtRow["SortOrder"].ToString());
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.IsDefault = dtRow["IsDefault"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsDefault"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        public List<FamilyRelationshipModel> getFamilyRelationshipModel()
        {
            List<FamilyRelationshipModel> list = new List<FamilyRelationshipModel>();
            FamilyRelationshipModel model = null;
            string procedureName = "PP_GetFamilyRelationshipDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendFamilyRelationshipInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyRelationshipObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getFamilyRelationshipModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<FamilyRelationshipModel> getFamilyRelationshipModelForDeletion()
        {
            List<FamilyRelationshipModel> list = new List<FamilyRelationshipModel>();
            FamilyRelationshipModel model = null;
            string procedureName = "pp_getFamilyRelationDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyRelationshipObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getFamilyRelationshipModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private FamilyRelationshipModel SetValueIntoFamilyRelationshipObject(DataRow dtRow)
        {
            FamilyRelationshipModel model = new FamilyRelationshipModel();
            try
            {
                if (dtRow != null)
                {
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = Convert.ToInt32(dtRow["SortOrder"].ToString());
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.IsDefault = dtRow["IsDefault"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsDefault"]);
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        public List<FamilyMemberModel> getFamilyMemberModel()
        {
            List<FamilyMemberModel> list = new List<FamilyMemberModel>();
            FamilyMemberModel model = null;
            string procedureName = "pp_getFamilyMemberDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendFamilyMemberInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyMemberObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getFamilyMemberModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<FamilyMemberModel> getFamilyMemberModelForDeletion()
        {
            List<FamilyMemberModel> list = new List<FamilyMemberModel>();
            FamilyMemberModel model = null;
            string procedureName = "pp_getFamilyMemberDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyMemberObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getFamilyMemberModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private FamilyMemberModel SetValueIntoFamilyMemberObject(DataRow dtRow)
        {
            FamilyMemberModel model = new FamilyMemberModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.PatientExternalId = dtRow["PatientExternalId"] == DBNull.Value ? "" : dtRow["PatientExternalId"].ToString();
                    model.RelativeExternalId = dtRow["RelativeExternalId"] == DBNull.Value ? "" : dtRow["RelativeExternalId"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion FamilyHistory/FamilyRelationship/FamilyMember
        #region HealthReviews
        public List<HealthReviewModel> getHealthReviewModel()
        {
            List<HealthReviewModel> list = new List<HealthReviewModel>();
            HealthReviewModel model = null;
            string procedureName = "pp_GetHealthReviewDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendHealthReviewInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoHealthReviewObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getHealthReviewModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<HealthReviewModel> getHealthReviewModelForDeletion()
        {
            List<HealthReviewModel> list = new List<HealthReviewModel>();
            HealthReviewModel model = null;
            string procedureName = "pp_GetHealthReviewDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoHealthReviewObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getHealthReviewModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private HealthReviewModel SetValueIntoHealthReviewObject(DataRow dtRow)
        {
            HealthReviewModel model = new HealthReviewModel();
            try
            {
                if (dtRow != null)
                {
                    model.Category = dtRow["Category"] == DBNull.Value ? "" : dtRow["Category"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.IsDefault = dtRow["IsDefault"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsDefault"]);
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = dtRow["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(dtRow["SortOrder"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion HealthReviews
        #region Insurance
        public List<InsuranceModel> getInsuranceModel()
        {
            List<InsuranceModel> list = new List<InsuranceModel>();
            InsuranceModel model = null;
            string procedureName = "PP_GetInsuranceDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendInsuranceInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoInsuranceObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getInsuranceModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<InsuranceModel> getInsuranceModelForDeletion()
        {
            List<InsuranceModel> list = new List<InsuranceModel>();
            InsuranceModel model = null;
            string procedureName = "pp_GetInsuranceDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoInsuranceObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getInsuranceModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private InsuranceModel SetValueIntoInsuranceObject(DataRow dtRow)
        {
            InsuranceModel model = new InsuranceModel();
            try
            {
                if (dtRow != null)
                {
                    model.Active = dtRow["InActive"] == DBNull.Value ? false : !Convert.ToBoolean(dtRow["InActive"]);
                    model.CompanyName = dtRow["CompanyName"] == DBNull.Value ? "" : dtRow["CompanyName"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.InsuranceType = dtRow["InsuranceType"] == DBNull.Value ? "" : dtRow["InsuranceType"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Insurance
        #region Medication
        public List<MedicationModel> getMedicationModel()
        {
            List<MedicationModel> list = new List<MedicationModel>();
            MedicationModel model = null;
            string procedureName = "PP_GetMedicationDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendMedicationInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoMedicationObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getMedicationModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<MedicationModel> getMedicationModelForDeletion()
        {
            List<MedicationModel> list = new List<MedicationModel>();
            MedicationModel model = null;
            string procedureName = "pp_GetMedicationDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoMedicationObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getMedicationModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private MedicationModel SetValueIntoMedicationObject(DataRow dtRow)
        {
            MedicationModel model = new MedicationModel();
            try
            {
                if (dtRow != null)
                {
                    model.Medication = dtRow["Medication"] == DBNull.Value ? "" : dtRow["Medication"].ToString();
                    model.ExternalId = dtRow["ExtenalID"] == DBNull.Value ? "" : dtRow["ExtenalID"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Medication
        #region Prefix
        public List<PrefixModel> getPrefixModel()
        {
            List<PrefixModel> list = new List<PrefixModel>();
            PrefixModel model = null;
            string procedureName = "PP_GetPrifixDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendPrefixInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoPrefixObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getPrefixModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<PrefixModel> getPrefixModelForDeletion()
        {
            List<PrefixModel> list = new List<PrefixModel>();
            PrefixModel model = null;
            string procedureName = "PP_GetPrifixDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoPrefixObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getPrefixModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private PrefixModel SetValueIntoPrefixObject(DataRow dtRow)
        {
            PrefixModel model = new PrefixModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Prefix
        #region Suffix
        public List<SuffixModel> getSuffixModel()
        {
            List<SuffixModel> list = new List<SuffixModel>();
            SuffixModel model = null;
            string procedureName = "PP_GetSuffixDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendSuffixInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSuffixObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSuffixModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<SuffixModel> getSuffixModelForDeletion()
        {
            List<SuffixModel> list = new List<SuffixModel>();
            SuffixModel model = null;
            string procedureName = "PP_GetSuffixDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSuffixObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSuffixModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private SuffixModel SetValueIntoSuffixObject(DataRow dtRow)
        {
            SuffixModel model = new SuffixModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Suffix
        #region Surgery
        public List<SurgeryModel> getSurgeryModel()
        {
            List<SurgeryModel> list = new List<SurgeryModel>();
            SurgeryModel model = null;
            string procedureName = "PP_GetSurgeryDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendSurgeryInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSurgeryObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSurgeryModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<SurgeryModel> getSurgeryModelForDeletion()
        {
            List<SurgeryModel> list = new List<SurgeryModel>();
            SurgeryModel model = null;
            string procedureName = "PP_GetSurgeryDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSurgeryObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSurgeryModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private SurgeryModel SetValueIntoSurgeryObject(DataRow dtRow)
        {
            SurgeryModel model = new SurgeryModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = dtRow["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(dtRow["SortOrder"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Surgery
        #region ExamFieldMapping
        public List<ExamFieldMappingModel> getExamFieldMappingModel()
        {
            List<ExamFieldMappingModel> list = new List<ExamFieldMappingModel>();
            ExamFieldMappingModel model = null;
            string procedureName = "PP_GetExamFieldMappingDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendExamFieldMappingInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoExamFieldMappingModelObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getExamFieldMappingModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<ExamFieldMappingModel> getExamFieldMappingModelForDeletion()
        {
            List<ExamFieldMappingModel> list = new List<ExamFieldMappingModel>();
            ExamFieldMappingModel model = null;
            string procedureName = "PP_GetExamFieldMappingDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoExamFieldMappingModelObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getExamFieldMappingModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private ExamFieldMappingModel SetValueIntoExamFieldMappingModelObject(DataRow dtRow)
        {
            ExamFieldMappingModel model = new ExamFieldMappingModel();
            try
            {
                if (dtRow != null)
                {
                    model.ColumnName = dtRow["ColumnName"] == DBNull.Value ? "" : dtRow["ColumnName"].ToString();
                    model.DataType = dtRow["DataType"] == DBNull.Value ? "" : dtRow["DataType"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.DisplayName = dtRow["DisplayName"] == DBNull.Value ? "" : dtRow["DisplayName"].ToString();
                    model.Format = dtRow["Format"] == DBNull.Value ? "" : dtRow["Format"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion ExamFieldMapping
        #region ExamFieldMappingDetails
        public List<ExamFieldMappingDetailModel> getExamFieldMappingDetailModel()
        {
            List<ExamFieldMappingDetailModel> list = new List<ExamFieldMappingDetailModel>();
            ExamFieldMappingDetailModel model = null;
            string procedureName = "PP_GetExamFieldMappingDetailsList";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendExamFieldMappingModelDetailInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoExamFieldMappingDetailObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getExamFieldMappingDetailModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<ExamFieldMappingDetailModel> getExamFieldMappingDetailModelForDeletion()
        {
            List<ExamFieldMappingDetailModel> list = new List<ExamFieldMappingDetailModel>();
            ExamFieldMappingDetailModel model = null;
            string procedureName = "PP_GetExamFieldMappingDetailDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoExamFieldMappingDetailObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getExamFieldMappingDetailModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private ExamFieldMappingDetailModel SetValueIntoExamFieldMappingDetailObject(DataRow dtRow)
        {
            ExamFieldMappingDetailModel model = new ExamFieldMappingDetailModel();
            try
            {
                if (dtRow != null)
                {
                    model.ColumnName = dtRow["ColumnName"] == DBNull.Value ? "" : dtRow["ColumnName"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.ListValue = dtRow["ListValue"] == DBNull.Value ? "" : dtRow["ListValue"].ToString();
                    model.SortOrder = dtRow["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(dtRow["SortOrder"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion ExamFieldMapping
        #region SocialHistoryAlcohols
        public List<SocialHistoryAlcoholModel> getSocialHistoryAlcoholModel()
        {
            List<SocialHistoryAlcoholModel> list = new List<SocialHistoryAlcoholModel>();
            SocialHistoryAlcoholModel model = null;
            string procedureName = "PP_GetSocialHistoryAlcoholDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendSocialHistoryAlcoholInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSocialHistoryAlcoholObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSocialHistoryAlcoholModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<SocialHistoryAlcoholModel> getSocialHistoryAlcoholForDeletion()
        {
            List<SocialHistoryAlcoholModel> list = new List<SocialHistoryAlcoholModel>();
            SocialHistoryAlcoholModel model = null;
            string procedureName = "PP_GetSocialHistoryAlcoholDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSocialHistoryAlcoholObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSocialHistoryAlcoholForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private SocialHistoryAlcoholModel SetValueIntoSocialHistoryAlcoholObject(DataRow dtRow)
        {
            SocialHistoryAlcoholModel model = new SocialHistoryAlcoholModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.IsDefault = dtRow["IsDefault"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsDefault"]);
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = dtRow["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(dtRow["SortOrder"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion SocialHistoryAlcohols
        #region SocialHistoryDrug
        public List<SocialHistoryDrugModel> getSocialHistoryDrugModel()
        {
            List<SocialHistoryDrugModel> list = new List<SocialHistoryDrugModel>();
            SocialHistoryDrugModel model = null;
            string procedureName = "PP_GetSocialHistoryDrugDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendSocialHistoryDrugInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSocialHistoryDrugObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSocialHistoryDrugModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<SocialHistoryDrugModel> getSocialHistoryDrugForDeletion()
        {
            List<SocialHistoryDrugModel> list = new List<SocialHistoryDrugModel>();
            SocialHistoryDrugModel model = null;
            string procedureName = "PP_GetSocialHistoryDrugDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoSocialHistoryDrugObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getSocialHistoryDrugForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private SocialHistoryDrugModel SetValueIntoSocialHistoryDrugObject(DataRow dtRow)
        {
            SocialHistoryDrugModel model = new SocialHistoryDrugModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.IsDefault = dtRow["IsDefault"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsDefault"]);
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = dtRow["SortOrder"] == DBNull.Value ? 0 : Convert.ToInt32(dtRow["SortOrder"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion SocialHistoryDrug
        #region PortalPatients / PortalPatientUpdate
        public List<PortalPatientUpdateModel> getPortalPatientUpdateModelForUpdation()
        {
            List<PortalPatientUpdateModel> list = new List<PortalPatientUpdateModel>();
            PortalPatientUpdateModel model = null;
            string procedureName = "PP_GetPortalPatientUpdateModelList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoPortalPatientUpdateObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getPortalPatientUpdateModelForUpdation", procedureName, ex.Message);
            }
            return list;
        }
        private PortalPatientUpdateModel SetValueIntoPortalPatientUpdateObject(DataRow dtRow)
        {
            PortalPatientUpdateModel model = new PortalPatientUpdateModel();
            try
            {
                if (dtRow != null)
                {
                    model.ExternalID = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    if (dtRow["PatientID"] != null)
                        model.PatientID = (Guid)dtRow["PatientID"];
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion PortalPatients / PortalPatientUpdate
        #region Forms/OpenForms/ClosedForms
        public int insertFormInformation(ListModel<FormModel> list)
        {
            int rowaffected = 0;
            string procedureName = "[PP_InsertFormModelDetails]";
            try
            {
                if (list != null && list.Rows.Count > 0)
                {
                    for (int i = 0; i < list.Rows.Count; i++)
                    {
                        FormModel model = list.Rows[i];
                        if (model != null)
                        {
                            SqlParameter[] parameters = GetParametersForFormModel(model, EnumTypes.OperationType.Insertion);
                            rowaffected = SaveUserData(procedureName, parameters);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "[insertFormInformation]", procedureName, ex.Message, 0);
            }
            return rowaffected;
        }
        private SqlParameter[] GetParametersForFormModel(FormModel model, EnumTypes.OperationType operationType)
        {
            SqlParameter[] parameters = null;
            try
            {
                int counter = 0;
                if (EnumTypes.OperationType.Deletion == operationType)
                {
                    parameters = new SqlParameter[1];
                    parameters[counter] = new SqlParameter("@ID", model.ID);
                }
                else if (EnumTypes.OperationType.Insertion == operationType)
                {
                    parameters = new SqlParameter[6];
                    parameters[counter] = new SqlParameter("@CreationDate", model.CreationDate);
                    parameters[++counter] = new SqlParameter("@FormLayoutName", model.FormLayoutName);
                    parameters[++counter] = new SqlParameter("@ID", model.ID);
                    parameters[++counter] = new SqlParameter("@PatientExternalId", model.PatientExternalId);
                    parameters[++counter] = new SqlParameter("@PatientFullName", model.PatientFullName);
                    parameters[++counter] = new SqlParameter("@Status", model.Status);
                }
                else if (EnumTypes.OperationType.Updation == operationType)
                {
                    parameters = new SqlParameter[6];
                    parameters[counter] = new SqlParameter("@CreationDate", model.CreationDate);
                    parameters[++counter] = new SqlParameter("@FormLayoutName", model.FormLayoutName);
                    parameters[++counter] = new SqlParameter("@ID", model.ID);
                    parameters[++counter] = new SqlParameter("@PatientExternalId", model.PatientExternalId);
                    parameters[++counter] = new SqlParameter("@PatientFullName", model.PatientFullName);
                    parameters[++counter] = new SqlParameter("@Status", model.Status);
                }
                return parameters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int SaveUserData(String procedureName, SqlParameter[] parameters)
        {
            try
            {
                int status = -1;
                status = DataAccessObjects.ExecuteInsertUpdateCommand(procedureName, System.Data.CommandType.StoredProcedure, parameters);
                return status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FormToBeClosedModel> getOpenFormModelToClose()
        {
            List<FormToBeClosedModel> list = new List<FormToBeClosedModel>();
            FormToBeClosedModel model = null;
            string procedureName = "pp_GetOpenFormModelToClose";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@Status", Convert.ToInt32(EnumTypes.FormStatus.Open.GetStringValue()));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr["ID"] != null)
                        {
                            model.ID = (Guid)dr["ID"];
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getOpenFormModelToClose", procedureName, ex.Message);
            }
            return list;
        }
        private OpenFormModel SetValueIntoFormModelObject(DataRow dtRow)
        {
            OpenFormModel model = new OpenFormModel();
            try
            {
                if (dtRow != null)
                {
                    if (dtRow["CreationDate"] != null)
                        model.CreationDate = (DateTime)dtRow["CreationDate"];
                    model.FormLayoutName = dtRow["FormLayoutName"] == DBNull.Value ? "" : dtRow["FormLayoutName"].ToString();
                    if (dtRow["Id"] != null)
                        model.ID = (Guid)dtRow["Id"];
                    model.PatientExternalId = dtRow["PatientExternalId"] == DBNull.Value ? "" : dtRow["PatientExternalId"].ToString();
                    model.PatientFullName = dtRow["PatientFullName"] == DBNull.Value ? "" : dtRow["PatientFullName"].ToString();
                    model.Status = dtRow["Status"] == DBNull.Value ? "" : dtRow["Status"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        public List<OpenFormModel> getOpenFormModelList()
        {
            List<OpenFormModel> list = new List<OpenFormModel>();
            OpenFormModel model = null;
            string procedureName = "pp_GetOpenFormModelToClose";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@Status", Convert.ToInt32(EnumTypes.FormStatus.Open.GetStringValue()));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr["ID"] != null)
                        {
                            model.ID = (Guid)dr["ID"];
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "getOpenFormModelList", procedureName, ex.Message);
            }
            return list;
        }
        public int insertPatientPortalDataIntoEMR(OpenFormModel model)
        {
            int rowaffected = 0;
            string procedureName = "PP_InsertPatientPortalDataIntoEMR";
            try
            {
                if (model != null)
                {
                    SqlParameter[] parameters = GetParametersForOpemFormModel(model, EnumTypes.OperationType.Insertion);
                    rowaffected = SaveUserData(procedureName, parameters);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FormsFacade", "[insertPatientPortalDataIntoEMR]", procedureName, ex.Message, 0);
            }
            return rowaffected;
        }
        private SqlParameter[] GetParametersForOpemFormModel(OpenFormModel model, EnumTypes.OperationType operationType)
        {
            string attribute = "";
            SqlParameter[] parameters = null;
            try
            {
                int counter = 0;
                if (EnumTypes.OperationType.Deletion == operationType)
                {
                    parameters = new SqlParameter[1];
                    parameters[counter] = new SqlParameter("@ID", model.ID);
                }
                else if (EnumTypes.OperationType.Insertion == operationType)
                {
                    parameters = new SqlParameter[120];
                    parameters[counter] = new SqlParameter("@CreationDate", model.CreationDate);
                    parameters[++counter] = new SqlParameter("@FormLayoutName", model.FormLayoutName);
                    parameters[++counter] = new SqlParameter("@ID", model.ID);
                    parameters[++counter] = new SqlParameter("@PatientExternalId", model.PatientExternalId);
                    foreach (OpenFormQuestionModel obj in model.OpenFormQuestions)
                    {
                        //parameters[++counter] = new SqlParameter("@FormLayoutName", obj.AnswerOptionOfOpenFormQuestions);
                        //parameters[++counter] = new SqlParameter("@BooleanAnswer", obj.BooleanAnswer);
                        //parameters[++counter] = new SqlParameter("@DateAnswer", obj.DateAnswer.ToString());
                        //parameters[++counter] = new SqlParameter("@Hint", obj.Hint);
                        //parameters[++counter] = new SqlParameter("@Label", obj.Label);
                        //parameters[++counter] = new SqlParameter("@QuestionItemAnswerType", obj.QuestionItemAnswerType);
                        //parameters[++counter] = new SqlParameter("@QuestionItemGroupName", obj.QuestionItemGroupName);
                        //parameters[++counter] = new SqlParameter("@QuestionItemName", obj.QuestionItemName);
                        //parameters[++counter] = new SqlParameter("@TextAnswer", obj.TextAnswer);
                        //parameters[++counter] = new SqlParameter("@Type", obj.Type);
                        switch (obj.QuestionItemName.ToLower())
                        {
                            case "index":
                                parameters[++counter] = new SqlParameter("@index", obj.TextAnswer);
                                break;
                            case "siteid":
                                parameters[++counter] = new SqlParameter("@SiteID", obj.TextAnswer);
                                break;
                            case "patient date of birth":
                                parameters[++counter] = new SqlParameter("@BirthDate", obj.TextAnswer);
                                break;
                            case "patient last name":
                                parameters[++counter] = new SqlParameter("@LastName", obj.TextAnswer);
                                break;
                            case "patient first name":
                                parameters[++counter] = new SqlParameter("@FirstName", obj.TextAnswer);
                                break;
                            case "middlename":
                                parameters[++counter] = new SqlParameter("@MiddleName", obj.TextAnswer);
                                break;
                            case "patient suffix":
                                parameters[++counter] = new SqlParameter("@NameSuffix", obj.TextAnswer);
                                break;
                            case "title":
                                parameters[++counter] = new SqlParameter("@Title", obj.TextAnswer);
                                break;
                            case "language":
                                parameters[++counter] = new SqlParameter("@Language", obj.TextAnswer);
                                break;
                            case "ethnicity":
                                parameters[++counter] = new SqlParameter("@Ethnicity", obj.TextAnswer);
                                break;
                            case "race":
                                parameters[++counter] = new SqlParameter("@Race", obj.TextAnswer);
                                break;
                            case "gender":
                                parameters[++counter] = new SqlParameter("@Gender", obj.TextAnswer);
                                break;
                            case "ConfidentialCommunication":
                                parameters[++counter] = new SqlParameter("@ConfidentialCommunication", obj.TextAnswer);
                                break;
                            case "patient home phone":
                                parameters[++counter] = new SqlParameter("@HomePhone", obj.TextAnswer);
                                break;
                            case "patient cell phone":
                                parameters[++counter] = new SqlParameter("@CellPhone", obj.TextAnswer);
                                break;
                            case "patient address 1":
                                parameters[++counter] = new SqlParameter("@Address1", obj.TextAnswer);
                                break;
                            case "address2":
                                parameters[++counter] = new SqlParameter("@Address2", obj.TextAnswer);
                                break;
                            case "patient city":
                                parameters[++counter] = new SqlParameter("@City", obj.TextAnswer);
                                break;
                            case "patient state":
                                parameters[++counter] = new SqlParameter("@State", obj.TextAnswer);
                                break;
                            case "patient zip":
                                parameters[++counter] = new SqlParameter("@Zip", obj.TextAnswer);
                                break;
                            case "patient email":
                                parameters[++counter] = new SqlParameter("@Email", obj.TextAnswer);
                                break;
                            case "marital status":
                                parameters[++counter] = new SqlParameter("@MaritalStatus", obj.TextAnswer);
                                break;
                            case "":
                                parameters[++counter] = new SqlParameter("@Spouse", obj.TextAnswer);
                                break;
                            case "family doctor":
                                parameters[++counter] = new SqlParameter("@FamilyDoctor", obj.TextAnswer);
                                break;
                            case "refering doctor":
                                parameters[++counter] = new SqlParameter("@ReferringDoctor", obj.TextAnswer);
                                break;
                            case "employment status":
                                parameters[++counter] = new SqlParameter("@EmploymentStatus", obj.TextAnswer);
                                break;
                            case "employer":
                                parameters[++counter] = new SqlParameter("@Employer", obj.TextAnswer);
                                break;
                            case "patient work phone":
                                parameters[++counter] = new SqlParameter("@WorkPhone", obj.TextAnswer);
                                break;
                            case "emergency contact":
                                parameters[++counter] = new SqlParameter("@EmergencyContact", obj.TextAnswer);
                                break;
                            //     case "":
                            //         parameters[++counter] = new SqlParameter("@EmergencyPhone", obj.TextAnswer);
                            //         break;
                            case "drivers license #":
                                parameters[++counter] = new SqlParameter("@DriverLicenseNumber", obj.TextAnswer);
                                break;
                            case "insurance name":
                                parameters[++counter] = new SqlParameter("@Insurance1InsuranceName", obj.TextAnswer);
                                break;
                            case "member id":
                                parameters[++counter] = new SqlParameter("@Insurance1MemberID", obj.TextAnswer);
                                break;
                            case "policy number":
                                parameters[++counter] = new SqlParameter("@Insurance1PolicyNumber", obj.TextAnswer);
                                break;
                            case "expiration date":
                                parameters[++counter] = new SqlParameter("@Insurance1ExpirationDate", obj.TextAnswer);
                                break;
                            case "co-pay amount":
                                parameters[++counter] = new SqlParameter("@Insurance1Copay", obj.TextAnswer);
                                break;
                            case "relationship to insured":
                                parameters[++counter] = new SqlParameter("@Insurance1Relationship", obj.TextAnswer);
                                break;
                            //           case "":
                            //   parameters[++counter] = new SqlParameter("@Insurance1InsuredLastName", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //    parameters[++counter] = new SqlParameter("@Insurance1InsuredFirstName", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Insurance1InsuredNameSuffix", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //   parameters[++counter] = new SqlParameter("@Insurance1InsuredBirthDate", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Insurance1InsuredGender", obj.TextAnswer);
                            //         break;
                            //  case "":
                            // parameters[++counter] = new SqlParameter("@Insurance1InsuredZip", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //   parameters[++counter] = new SqlParameter("@Insurance1InsuredState", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //parameters[++counter] = new SqlParameter("@Insurance1InsuredCity", obj.TextAnswer);
                            //         break;
                            //   parameters[++counter] = new SqlParameter("@Insurance1InsuredAddress2", obj.TextAnswer);
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance1InsuredAddress1", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Insurance1InsurendHomePhone", obj.TextAnswer);
                            //         break;
                            //      case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance1InsuredEmployer", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Insurance2InsuranceName", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Insurance2MemberID", obj.TextAnswer);
                            //         break;
                            //          case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2PolicyNumber", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2ExpirationDate", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //     parameters[++counter] = new SqlParameter("@Insurance2Copay", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //   parameters[++counter] = new SqlParameter("@Insurance2Relationship", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredLastName", obj.TextAnswer);
                            //         break;
                            //         case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredFirstName", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredMiddleName", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Insurance2InsuredNameSuffix", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredBirthDate", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredGender", obj.TextAnswer);
                            //         break;
                            //  case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsurendHomePhone", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredAddress1", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //   parameters[++counter] = new SqlParameter("@Insurance2InsuredAddress2", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //    parameters[++counter] = new SqlParameter("@Insurance2InsuredCity", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Insurance2InsuredState", obj.TextAnswer);
                            //         break;
                            //   case "":
                            //        parameters[++counter] = new SqlParameter("@Insurance2InsuredZip", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //            parameters[++counter] = new SqlParameter("@Insurance2InsuredEmployer", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //      parameters[++counter] = new SqlParameter("@CC", obj.TextAnswer);
                            //         break;
                            case "ocular history":
                                parameters[++counter] = new SqlParameter("@OcularHistory", obj.TextAnswer);
                                break;
                            case "medical history":
                                parameters[++counter] = new SqlParameter("@MedicalHistory", obj.TextAnswer);
                                break;
                            case "surgical history":
                                parameters[++counter] = new SqlParameter("@SurgicalHistory", obj.TextAnswer);
                                break;
                            case "allergy name":
                                parameters[++counter] = new SqlParameter("@Allergies", obj.TextAnswer);
                                break;
                            //     case "":
                            //     parameters[++counter] = new SqlParameter("@OcularMeds", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //    parameters[++counter] = new SqlParameter("@SystemicMeds", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Constitutional", obj.TextAnswer);
                            //         break;
                            //  case "":
                            //  parameters[++counter] = new SqlParameter("@HEENT", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //    parameters[++counter] = new SqlParameter("@Cardiovascular", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //   parameters[++counter] = new SqlParameter("@Respiratory", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Gastrointestinal", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Genitourinary", obj.TextAnswer);
                            //         break;
                            //           case "":
                            //  parameters[++counter] = new SqlParameter("@Musculoskeletal", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@Integumentary", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Neurological", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@Psychiatric", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //    parameters[++counter] = new SqlParameter("@Endocrine", obj.TextAnswer);
                            //         break;
                            // case "";
                            // parameters[++counter] = new SqlParameter("@HematologicLymphatic", obj.TextAnswer);
                            // break;
                            //    case "":
                            //  parameters[++counter] = new SqlParameter("@AllergicImmunologic", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //     parameters[++counter] = new SqlParameter("@FamilyHistory", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //    parameters[++counter] = new SqlParameter("@SmokingStatus", obj.TextAnswer);
                            //         break;
                            case "alcohol":
                                parameters[++counter] = new SqlParameter("@Alcohol", obj.TextAnswer);
                                break;
                            case "recreational drugs":
                                parameters[++counter] = new SqlParameter("@RecreationalDrugs", obj.TextAnswer);
                                break;
                            case "occupation":
                                parameters[++counter] = new SqlParameter("@Occupation", obj.TextAnswer);
                                break;
                            case "driving status":
                                parameters[++counter] = new SqlParameter("@DrivingStatus", obj.TextAnswer);
                                break;
                            case "living arrangement":
                                parameters[++counter] = new SqlParameter("@LivingArrangement", obj.TextAnswer);
                                break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@SocialHxOther", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@SocialMaritalStatus", obj.TextAnswer);
                            //         break;
                            //       case "":
                            //  parameters[++counter] = new SqlParameter("@SmokingStatusid", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@IsAllergiesNone", obj.TextAnswer);
                            //         break;
                            //     case "":
                            // parameters[++counter] = new SqlParameter("@IsMedicationsNone", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@VitalsWeight", obj.TextAnswer);
                            //         break;
                            //     case "":
                            //  parameters[++counter] = new SqlParameter("@VitalsHeight", obj.TextAnswer);
                            //         break;
                            case "social security number":
                                parameters[++counter] = new SqlParameter("SocialSecurityNumber", obj.TextAnswer);
                                     break;
                            default:
                                     if (attribute == "") {
                                         attribute = obj.QuestionItemName.ToString();
                                     }else{
                                         attribute = attribute + "  ,  "+ obj.QuestionItemName.ToString();
                                     }
                                break;
                        }
                        //Social security number ,Ethnicity,patient work phone,Allergy reaction,Allergy severity,Allergy type
                        //Medication name,
                        //foreach (AnswerOptionOfOpenFormQuestionModel answer in obj.AnswerOptionOfOpenFormQuestions)
                        //{
                        //    parameters[++counter] = new SqlParameter("@PatientExternalId", answer.Category);
                        //    parameters[++counter] = new SqlParameter("@PatientExternalId", answer.Name);
                        //}
                    }
                }
                else if (EnumTypes.OperationType.Updation == operationType)
                {
                    parameters = new SqlParameter[6];
                    parameters[counter] = new SqlParameter("@CreationDate", model.CreationDate);
                    parameters[++counter] = new SqlParameter("@FormLayoutName", model.FormLayoutName);
                    parameters[++counter] = new SqlParameter("@ID", model.ID);
                    parameters[++counter] = new SqlParameter("@PatientExternalId", model.PatientExternalId);
                }
                return parameters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Forms/OpenForms/ClosedForms
    }
}
