﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientPortalFacade
{
    /// <summary>
    /// this class contains constants for all the keys specified in app.config file
    /// </summary>
    public class AppSettingsKeys
    {
        // These constants must match with their config -> appSettings -> keys
        public const string ApiClientVersion = "ApiClientVersion";
        public const string ApiCustomerKey = "ApiCustomerKey";
        public const string ApiUsername = "ApiUsername";
        public const string ApiPassword = "ApiPassword";
        public const string ConnectionString = "PracticeRepository";
        public const string TestMode = "TestMode";
        public const string SyncInterval = "SyncInterval";
        public const string KareoApiTimeZone = "KareoApiTimeZone";
        public const string CustomerTimeZone = "CustomerTimeZone";
        public const string DefaultProviderLastName = "DefaultProviderLastName";
        public const string NumberOfFutureDaysToSync = "NumberOfFutureDaysToSync";
        public const string MedFlowScheduledTime = "MedFlowScheduledTime";
        public const string MedFlowScheduledEndTime = "MedFlowScheduledEndTime";
        public const string IntervalMode = "IntervalMode";
        public const string CleanHealthInformation = "CleanHealthInformation";
        public const string CleanEducationInformation = "CleanEducationInformation";
        public const string IsPatientSync = "IsPatientSync";
        public const string ACIServices = "IsRCPOn";
        public const string ERPServices = "IsERPOn";
    }
}
