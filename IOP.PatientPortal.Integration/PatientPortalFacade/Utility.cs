﻿using System;
using log4net;
using System.Data.SqlClient;
using System.Data;
using AppointmentAPI.DAO;

namespace PatientPortalFacade
{
    public class Utility
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Utility));
        public Utility() { }
        public static void saveErrorIntoTable(string classname, string methodname, string procedurename, string errormsg, int createdBy)
        {
            try
            {
                int status;
                string query = "[MVE].PP_InsertIntoErrorLog";
                SqlParameter[] parameter = new SqlParameter[5];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@PageName", classname);
                parameter[++columnIndex] = new SqlParameter("@MethodName", methodname);
                parameter[++columnIndex] = new SqlParameter("@ErrDescription", procedurename);
                parameter[++columnIndex] = new SqlParameter("@ErrorMessage", errormsg);
                parameter[++columnIndex] = new SqlParameter("@createdBy", createdBy);
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch 
            {
            }
        }
        public static void saveErrorIntoTable(string classname, string methodname, string procedurename, string errormsg)
        {
            try
            {
                int status;
                string query = "[MVE].PP_InsertIntoErrorLog";
                SqlParameter[] parameter = new SqlParameter[4];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@PageName", classname);
                parameter[++columnIndex] = new SqlParameter("@MethodName", methodname);
                parameter[++columnIndex] = new SqlParameter("@ErrDescription", procedurename);
                parameter[++columnIndex] = new SqlParameter("@ErrorMessage", errormsg);
                // parameter[++columnIndex] = new SqlParameter("@createdBy", createdBy);
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch { }
        }
        public static string validateDate(DateTime startdate, DateTime enddate, DateTime todaydate)
        {
            string msg = String.Empty;
            if (startdate > todaydate)
            {
                if (msg == "")
                    msg = "Start Date can't be future date.";
                else
                    msg = msg + "\n Start Date can't be future date.";
            }
            if (enddate > todaydate)
            {
                if (msg == "")
                    msg = "End Date can't be future date.";
                else
                    msg = msg + "\n End Date can't be future date.";
            }
            if (startdate > enddate)
            {
                if (msg == "")
                    msg = "Start Date can't be greater than End Date.";
                else
                    msg = msg + "\n Start Date can't be greater than End Date.";
            }
            return msg;
        }
        public static int InsertDataIntoPortalQueue(PortalQueue pq)
        {
            int status = -1;
            try
            {
                string query = "[MVE].PP_InsertPortalQueue";
                SqlParameter[] parameter = new SqlParameter[6];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@MessageData", pq.MessageData.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@ActionTypeLookupId", pq.ActionTypeLookupId);
                parameter[++columnIndex] = new SqlParameter("@ProcessedDate", pq.ProcessedDate);
                parameter[++columnIndex] = new SqlParameter("@PatientNo", pq.PatientId);
                parameter[++columnIndex] = new SqlParameter("@IsActive", pq.IsActive);
                parameter[++columnIndex] = new SqlParameter("@RequestId", 0);
                parameter[columnIndex].Direction = ParameterDirection.Output; ;
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
                if (status > 0)
                {
                    status = Convert.ToInt32(parameter[5].SqlValue.ToString());
                }
            }
            catch { }
            return status;
        }
        public static int InsertDataIntoPortalQueueResponse(PortalQueue pq)
        {
            int status = -1;
            try
            {
                string query = "[MVE].PP_InsertPortalQueueResponse";
                SqlParameter[] parameter = new SqlParameter[6];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@MessageData", pq.MessageData.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@ActionTypeLookupId", pq.ActionTypeLookupId);
                parameter[++columnIndex] = new SqlParameter("@ProcessedDate", pq.ProcessedDate);
                parameter[++columnIndex] = new SqlParameter("@PatientNo", pq.PatientId);
                parameter[++columnIndex] = new SqlParameter("@IsActive", pq.IsActive);
                parameter[++columnIndex] = new SqlParameter("@ExternalId", pq.ExternalId);
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch
            {

            }
            return status;
        }
        public static int InsertDataIntoPortalQueueException(PortalQueue pq)
        {
            int status = -1;
            try
            {
                string query = "[MVE].PP_InsertPortalQueueException";
                SqlParameter[] parameter = new SqlParameter[7];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@MessageData", pq.MessageData.Replace("'", "''"));
                parameter[++columnIndex] = new SqlParameter("@ActionTypeLookupId", pq.ActionTypeLookupId);
                parameter[++columnIndex] = new SqlParameter("@ProcessedDate", pq.ProcessedDate);
                parameter[++columnIndex] = new SqlParameter("@PatientNo", pq.PatientId);
                parameter[++columnIndex] = new SqlParameter("@IsActive", pq.IsActive);
                parameter[++columnIndex] = new SqlParameter("@ExternalId", pq.ExternalId);
                parameter[++columnIndex] = new SqlParameter("@Exception", pq.Exception);
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch { }
            return status;
        }
        public static void DeleteRecord(string _deleteQry)
        {
            try
            {
                DataAccessObjects.ExecuteInsertUpdateCommand(_deleteQry, CommandType.Text, null);
            }
            catch { }
        }

        public static void UpdateRecord(string _updateQry)
        {
            try
            {
                DataAccessObjects.ExecuteInsertUpdateCommand(_updateQry, CommandType.Text, null);
            }
            catch { }
        }

        public static bool InsertRecord(string _insertQry)
        {
            bool retStatus = false;
            try
            {
                DataAccessObjects.ExecuteInsertUpdateCommand(_insertQry, CommandType.Text, null);
                retStatus = true;
            }
            catch(Exception ee)
            {
                retStatus = false;
            }
            return retStatus;
        }

        public static void UpdatePatientLog(string recordId)
        {
            try
            {
                string query = "Update MVE.PP_ERPInputLog Set Status = 'C', ProcessedDate = GETDATE() Where Id = '" + recordId + "'";
                DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.Text, null);
            }
            catch
            {

            }
        }

    }
}
