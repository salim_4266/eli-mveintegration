﻿using System;

namespace PatientPortalFacade
{
   public class PortalQueue
    {
        public int Id { get; set; }
        public int ActionTypeLookupId { get; set; }
        public DateTime ProcessedDate { get; set; }
        public string PatientId { get; set; }
        public Boolean IsActive { get; set; }
        public string ExternalId { get; set; }
        public string Exception { get; set; }
        public string  MessageData { get; set; }
    }
}
