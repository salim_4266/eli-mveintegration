﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.Users;
using GlobalPortal.Api.Models.Contacts;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PatientPortalFacade
{
    public class UsersFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(UsersFacade));
        public List<UserModel> GetUserModel()
        {
            List<UserModel> list = new List<UserModel>();
            UserModel model = null;
            string procedureName = "MVE.PP_GetUserDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendUserInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoUsersObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("UsersFacade", "GetUserModel", procedureName, ex.Message);
            }
            return list;
        }

        public List<UserModel> getDeleteUser()
        {
            List<UserModel> list = new List<UserModel>();
            UserModel model = null;
            string procedureName = "MVE.PP_GetUserDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoUsersObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("UsersFacade", "getDeleteUser", procedureName, ex.Message);
            }
            return list;
        }

        private UserModel SetValueIntoUsersObject(DataRow dtRow)
        {
            UserModel model = new UserModel();
            try
            {
                string Password = Guid.NewGuid().ToString().Substring(0, 9);
                if (dtRow != null)
                {
                    ContactModel objContact = new ContactModel();
                    objContact.Id = Guid.NewGuid();
                    objContact.FirstName = dtRow["FirstName"] == DBNull.Value ? "" : dtRow["FirstName"].ToString();
                    objContact.LastName = dtRow["LastName"] == DBNull.Value ? "" : dtRow["LastName"].ToString();
                    model.Username = dtRow["Username"] == DBNull.Value ? "" : dtRow["Username"].ToString();
                    model.Password = Password;
                    model.PasswordConfirmation = Password;
                    model.Active = dtRow["Active"].ToString() == "1" ? true : false;
                    model.Notes = dtRow["Notes"] == DBNull.Value ? "" : dtRow["Notes"].ToString();
                    model.UpDoxId = dtRow["UpDoxId"] == DBNull.Value ? "" : dtRow["UpDoxId"].ToString();
                    model.Contact = objContact;
                    model.Locations = null;
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
