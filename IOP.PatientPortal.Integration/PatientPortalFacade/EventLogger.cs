﻿using System.Diagnostics;

namespace PatientPortalFacade
{

    public class EventLogger
    {
        private static EventLogger instance;
        private static EventLog eventLog;

        private string eventSourceName;
        private string eventLogName;

        private EventLogger()
        {
            eventLog = new EventLog();

            eventSourceName = "IO Sync Service";
            eventLogName =  "IO Sync Service";
            
            string logName = EventLog.LogNameFromSourceName(eventSourceName, ".");
            if (logName != eventLogName && logName != string.Empty)
            {
                EventLog.DeleteEventSource(eventSourceName, ".");
            }

            if (!EventLog.SourceExists(eventSourceName))
            {
                EventLog.CreateEventSource(eventSourceName, eventLogName);
            }
            eventLog.Source = eventSourceName;
            eventLog.Log = eventLogName;
        }

        public void WriteLog(string message, EventLogEntryType type)
        {
            eventLog.WriteEntry(message, type);
        }

        public static EventLogger Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventLogger();
                }
                return instance;
            }
        }

        public bool IsWindowsService()
        {
            string entyPointAssembly = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            return (entyPointAssembly == "IO.Practiceware.Integration.WinClient") ? false : true;
        }

        public void DeleteSourceAndLog(string source, string log)
        {
            if (EventLog.SourceExists(source))
            {
                EventLog.DeleteEventSource(source);
            }
            if (EventLog.Exists(log))
            {
                EventLog.Delete(log);
            }
        }
    }
}
