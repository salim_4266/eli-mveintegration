﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.DoctorAvailabilitySlots;
using GlobalPortal.Api.Models.Doctors;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PatientPortalFacade
{
    public class DoctorsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DoctorsFacade));
        #region Doctors
        public List<DoctorModel> getDoctorModel()
        {
            List<DoctorModel> list = new List<DoctorModel>();
            DoctorModel model = null;
            string procedureName = "[MVE].[PP_GetDoctorDetails]";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendDoctorInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDoctorObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("DoctorsFacade", "getDoctorModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<DoctorModel> getDoctorModelForDeletion()
        {
            List<DoctorModel> list = new List<DoctorModel>();
            DoctorModel model = null;
            string procedureName = "pp_GetDoctorDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDoctorObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("DoctorsFacade", "getDoctorModelForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private DoctorModel SetValueIntoDoctorObject(DataRow dtRow)
        {
            DoctorModel model = new DoctorModel();
            try
            {
                if (dtRow != null)
                {
                    model.Active = dtRow["Active"].ToString() == "1" ? true : false;
                    if (dtRow.Table.Columns.Contains("AliasName"))
                        model.Alias = dtRow["AliasName"] == DBNull.Value ? "" : dtRow["AliasName"].ToString();
                    else
                        model.Alias = "";
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.FirstName = dtRow["FirstName"] == DBNull.Value ? "" : dtRow["FirstName"].ToString();
                    model.LastName = dtRow["LastName"] == DBNull.Value ? "" : dtRow["LastName"].ToString();
                    if (dtRow.Table.Columns.Contains("InHouse"))
                        model.InHouse = dtRow["InHouse"].ToString() == "1" ? true : false;
                    else
                        model.InHouse = true;
                    if (model.Active)
                        model.SecureRecipientExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    else
                        model.SecureRecipientExternalId = null;
                    model.LocationExternalId = dtRow["location"] == DBNull.Value ? "" : dtRow["location"].ToString(); ;
                    List<string> location = new List<string>(new string[] { "" });
                    model.Locations = null; //location;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion Doctors
        #region DoctorAvailabiltySlots
        public List<DoctorAvailabilitySlotModel> getDoctorAvailabiltySlotModel()
        {
            List<DoctorAvailabilitySlotModel> list = new List<DoctorAvailabilitySlotModel>();
            DoctorAvailabilitySlotModel model = null;
            string procedureName = "pp_GetDoctorAvailabiltySlotDetails";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDoctorAvailabilitySlotObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("DoctorsFacade", "getDoctorAvailabiltySlotModel", procedureName, ex.Message);
            }
            return list;
        }
        public List<DoctorAvailabilitySlotModel> getDoctorAvailabiltySlotForDeletion()
        {
            List<DoctorAvailabilitySlotModel> list = new List<DoctorAvailabilitySlotModel>();
            DoctorAvailabilitySlotModel model = null;
            string procedureName = "pp_GetDoctorAvailabiltySlotDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDoctorAvailabilitySlotObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("DoctorsFacade", "getDoctorAvailabiltySlotForDeletion", procedureName, ex.Message);
            }
            return list;
        }
        private DoctorAvailabilitySlotModel SetValueIntoDoctorAvailabilitySlotObject(DataRow dtRow)
        {
            DoctorAvailabilitySlotModel model = new DoctorAvailabilitySlotModel();
            try
            {
                if (dtRow != null)
                {
                    if (dtRow["AppointmentRequestReasonId"] != null)
                        model.AppointmentRequestReasonId = (Guid)dtRow["AppointmentRequestReasonId"];
                    model.Available = dtRow["Available"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["Available"]);
                    List<DoctorAvailabilitySlotModel.DoctorAvailabilityDate> list = new List<DoctorAvailabilitySlotModel.DoctorAvailabilityDate>();
                    DoctorAvailabilitySlotModel.DoctorAvailabilityDate date = new DoctorAvailabilitySlotModel.DoctorAvailabilityDate();
                    date.End = System.DateTime.Now;
                    date.Start = System.DateTime.Now;
                    list.Add(date);
                    model.DoctorAvailabilityDates = list;
                    model.DoctorExternalId = dtRow["DoctorExternalId"] == DBNull.Value ? "" : dtRow["InDoctorExternalIdd"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.LocationExternalId = dtRow["LocationExternalId"] == DBNull.Value ? "" : dtRow["LocationExternalId"].ToString();
                    model.Notes = dtRow["Notes"] == DBNull.Value ? "" : dtRow["Notes"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion DoctorAvailabiltySlots
    }
}
