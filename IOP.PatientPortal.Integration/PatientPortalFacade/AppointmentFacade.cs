﻿using System;
using System.Collections.Generic;
using log4net;
using GlobalPortal.Api.Models.AppointmentStatuses;
using GlobalPortal.Api.Models.AppointmentsModel;
using System.Data.SqlClient;
using System.Data;
using AppointmentAPI.DAO;
namespace PatientPortalFacade
{
    public class AppointmentFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(AppointmentFacade));
       
        #region AppointmentStatuses
        public List<AppointmentStatusModel> GetAppointmentStatuses()
        {
            List<AppointmentStatusModel> list = new List<AppointmentStatusModel>();
            AppointmentStatusModel model = null;
            string procedureName = "MVE.PP_GetAppointmentStatus";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAppointmentStatusInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAppointmentStatusObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AppointmentFacade", "GetAppointmentStatuses", procedureName, ex.Message);
            }
            return list;
        }
        private AppointmentStatusModel SetValueIntoAppointmentStatusObject(DataRow dtRow)
        {
            AppointmentStatusModel model = new AppointmentStatusModel();
            try
            {
                if (dtRow != null)
                {
                    model.IsCanceled = dtRow["IsCanceled"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsCanceled"]);
                    model.IsCheckedOut = dtRow["IsCheckedOut"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsCheckedOut"]);
                    model.IsConfirmed = dtRow["IsConfirmed"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsConfirmed"]);
                    model.IsRescheduled = dtRow["IsRescheduled"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["IsRescheduled"]);
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        #endregion
        
        #region Appointments
        public List<AppointmentModel> GetNewAppointment()
        {
            List<AppointmentModel> list = new List<AppointmentModel>();
            AppointmentModel model = null;
            string procedureName = "MVE.PP_GetNewAppointments";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@PatientActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendPatientInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAppointmentObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AppointmentFacade", "GetAppointment", procedureName, ex.Message);
            }
            return list;
        }
        public List<AppointmentModel> GetDischargeAppointment()
        {
            List<AppointmentModel> list = new List<AppointmentModel>();
            AppointmentModel model = null;
            string procedureName = "MVE.PP_GetDischargeAppointments";
            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@NewAppointmentActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAppointmentInformation));
                parameters[1] = new SqlParameter("@DischargeAppointmentActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.UpdateAppointmentInformation));
                parameters[2] = new SqlParameter("@PatientActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendPatientInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAppointmentObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AppointmentFacade", "GetAppointment", procedureName, ex.Message);
            }
            return list;
        }
        public List<AppointmentModel> GetCancelAppointment()
        {
            List<AppointmentModel> list = new List<AppointmentModel>();
            AppointmentModel model = null;
            string procedureName = "MVE.PP_GetCancelAppointments";
            try
            {
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@NewAppointmentActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendAppointmentInformation));
                parameters[1] = new SqlParameter("@PatientActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendPatientInformation));
                parameters[2] = new SqlParameter("@CancelAppointmentActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.DeleteAppointmentInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAppointmentObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AppointmentFacade", "GetAppointment", procedureName, ex.Message);
            }
            return list;
        }
        private AppointmentModel SetValueIntoAppointmentObject(DataRow dtRow)
        {
            AppointmentModel model = new AppointmentModel();
            try
            {
                if (dtRow != null)
                {
                    model.AppointmentStatusExternalId = dtRow["AppointmentStatusExternalId"] == DBNull.Value ? "" : dtRow["AppointmentStatusExternalId"].ToString();
                    model.CancelReason = dtRow["CancelReason"] == DBNull.Value ? "" : dtRow["CancelReason"].ToString();
                    model.DoctorExternalId = dtRow["DoctorExternalId"] == DBNull.Value ? "" : dtRow["DoctorExternalId"].ToString();
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                    model.LocationExternalId = dtRow["LocationExternalId"] == DBNull.Value ? "" : dtRow["LocationExternalId"].ToString();
                    model.PatientExternalId = dtRow["PatientExternalId"] == DBNull.Value ? "" : dtRow["PatientExternalId"].ToString();
                    model.End = (DateTime)dtRow["End"];
                    model.Start = (DateTime)dtRow["Start"];
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        
        #endregion
    }
}
