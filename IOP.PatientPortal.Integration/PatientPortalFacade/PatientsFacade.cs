﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.Patients;
using GlobalPortal.Api.Models.Contacts;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using GlobalPortal.Api.Models.MarkAsReceived;
using System.Diagnostics;

namespace PatientPortalFacade
{
    public class PatientsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(PatientsFacade));
        public List<PatientModel> getPatientModel()
        {
            List<PatientModel> List = new List<PatientModel>();
            PatientModel model = null;
            string procedureName = "MVE.PP_GetPatientDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", EnumTypes.ActionTypeLookUps.SendPatientInformation);
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoNewPatientObject(dr);
                        List.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("PatientsFacade", "getPatientModel", procedureName, ex.Message);
            }
            return List;
        }


        public string getPracticeToken()
        {
            string _practiceToken = "";
            string _qryStatement = "Select Top(1) Value From model.ApplicationSettings Where Name = 'PracticeAuthToken'";
            try
            {
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(_qryStatement, CommandType.Text, null);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        _practiceToken = dr["Value"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return _practiceToken;
        }

        public List<PatientModel> getDeletePatient()
        {
            List<PatientModel> List = new List<PatientModel>();
            PatientModel model = null;
            string procedureName = "MVE.PP_GetPatientDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoNewPatientObject(dr);
                        List.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("PatientsFacade", "getDeletePatient", procedureName, ex.Message);
            }
            return List;
        }
        public List<CDAModel> getPatientHealthModel()
        {
            List<CDAModel> List = new List<CDAModel>();
            CDAModel model = null;
            string procedureName = "MVE.PP_GetPatientHealthInformation";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@ActionId", EnumTypes.ActionTypeLookUps.SendHealthReviewInformation);
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoNewCDAObject(dr);
                        List.Add(model);
                    }
                }
                else
                {
                    EventLogger.Instance.WriteLog("No data found for PHI", EventLogEntryType.Information);
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Error while reading data from SP - " + ex.Message, EventLogEntryType.Error);
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("PatientsFacade", "getPatientModel", procedureName, ex.Message);
            }
            return List;
        }

        //MVE.Patient_InputLog
        public DataTable getPatientLog()
        {
            DataTable returnList = new DataTable();
            string procedureName = "MVE.Patient_InputLog";
            try
            {
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, null);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    returnList = ds.Tables[0];
                }
                else
                {
                    EventLogger.Instance.WriteLog("No data found in Inputlog for processing", EventLogEntryType.Information);
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Error while reading data from SP - " + ex.Message, EventLogEntryType.Error);
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("PatientsFacade", "getPatientLog", procedureName, ex.Message);
            }
            return returnList;
        }

        public PatientModel getPatientDetail(string patientId)
        {
            PatientModel patientModel = new PatientModel();
            string procedureName = "MVE.PP_GetPatientDetailById";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                int columnIndex = 0;
                parameters[columnIndex] = new SqlParameter("@PatientId", patientId);
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    patientModel = SetValueIntoNewPatientObject(ds.Tables[0].Rows[0]);
                }
                else
                {
                    EventLogger.Instance.WriteLog("No patient found with specific patientId " + patientId, EventLogEntryType.Information);
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Error while reading data from SP - " + ex.Message, EventLogEntryType.Error);
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("PatientsFacade", "getPatientDetail", procedureName, ex.Message);
            }
            return patientModel;
        }

        private PatientModel SetValueIntoNewPatientObject(DataRow dtRow)
        {
            PatientModel model = new PatientModel();
            ContactModel contactmodel = new ContactModel();
            EmailAddressModel EAModelitem = new EmailAddressModel();
            PhoneNumberModel PhoneModelNos = new PhoneNumberModel();
            PostalAddressModel PostalAddressitem = new PostalAddressModel();
            List<string> listLocations = new List<string>();
            List<EmailAddressModel> EAModel = new List<EmailAddressModel>();
            List<PhoneNumberModel> PhoneModel = new List<PhoneNumberModel>();
            List<PostalAddressModel> PostalAddress = new List<PostalAddressModel>();
            try
            {
                if (dtRow != null)
                {
                    model.NextLogOnSecurityVerificationPatient = true;
                    model.NextLogOnSecurityVerificationRepresentative = true;
                    contactmodel.Id = Guid.NewGuid();
                    contactmodel.FirstName = dtRow["FirstName"].ToString();
                    contactmodel.LastName = dtRow["LastName"].ToString();
                    if (dtRow["MiddleName"].ToString() != "")
                    { contactmodel.MiddleName = dtRow["MiddleName"].ToString(); }
                    if (dtRow["Suffix"].ToString() != "")
                    { contactmodel.Suffix = dtRow["Suffix"].ToString(); }
                    if (dtRow["Prefix"].ToString() != "")
                    { contactmodel.Prefix = dtRow["Prefix"].ToString(); }
                    if (dtRow["FullName"].ToString() != "")
                    { contactmodel.FullName = dtRow["FullName"].ToString(); }
                    if (dtRow["CompanyName"].ToString() != "")
                    { contactmodel.CompanyName = dtRow["CompanyName"].ToString(); }
                    if (dtRow["JobTitle"].ToString() != "")
                    { contactmodel.JobTitle = dtRow["JobTitle"].ToString(); }

                    model.Contact = contactmodel;
                    listLocations.Add(dtRow["LocationID"].ToString());
                    model.Locations = listLocations;
                    model.Active = true;

                    if (dtRow["EmailAddresses"].ToString() != "")
                    {
                        EAModelitem.Address = dtRow["EmailAddresses"].ToString();
                        EAModelitem.Default = true;
                        EAModel.Add(EAModelitem);
                        model.Contact.EmailAddresses = EAModel;
                    }

                    if (dtRow["PhoneNumbers"].ToString() != "")
                    {
                        PhoneModelNos.Number = dtRow["PhoneNumbers"].ToString();
                        PhoneModel.Add(PhoneModelNos);
                        model.Contact.PhoneNumbers = PhoneModel;
                    }

                    if (dtRow["Address1"].ToString() != "")
                    { PostalAddressitem.Address1 = dtRow["Address1"].ToString(); }
                    if (dtRow["Address2"].ToString() != "")
                    { PostalAddressitem.Address2 = dtRow["Address2"].ToString(); }
                    if (dtRow["City"].ToString() != "")
                    { PostalAddressitem.City = dtRow["City"].ToString(); }
                    if (dtRow["State"].ToString() != "")
                    { PostalAddressitem.State = dtRow["State"].ToString(); }
                    if (dtRow["Country"].ToString() != "")
                    { PostalAddressitem.CountryName = dtRow["Country"].ToString(); }
                    if (dtRow["Zip"].ToString() != "")
                    { PostalAddressitem.Zip = dtRow["Zip"].ToString(); }
                    PostalAddressitem.Default = true;
                    PostalAddress.Add(PostalAddressitem);
                    model.Contact.PostalAddresses = PostalAddress;

                    model.Username = dtRow["Username"].ToString();
                    model.Password = dtRow["Password"].ToString();
                    model.Birthday = Convert.ToDateTime(dtRow["Birthdate"] == DBNull.Value ? "" : dtRow["Birthdate"]);
                    if (dtRow["Notes"].ToString() != "")
                    {
                        model.Notes = dtRow["Notes"] == DBNull.Value ? "" : dtRow["Notes"].ToString();
                    }

                    model.Active = true;

                    if (dtRow["LanguageName"].ToString() != "")
                    {
                        model.LanguageName = Convert.ToString(dtRow["LanguageName"] == DBNull.Value ? "" : dtRow["LanguageName"]);
                    }

                    model.ExternalId = Convert.ToString(dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"]); ;
                    model.ContactLensesReorderDate = null;
                    model.ContactLensesReorderName = "";
                    model.RecallDate1 = null;
                    model.RecallDate2 = null;
                    model.AllowEmailReminders = false;
                    model.AllowEmailBirthday = false;
                    model.AllowEmailThankYou = false;
                    model.AllowEmailEyeWear = false;
                    model.AllowEmailPromotions = false;
                    model.AllowSmsReminders = false;
                    model.AllowSmsBirthday = false;
                    model.AllowSmsThankYou = false;
                    model.AllowSmsEyewear = false;
                    model.AllowSmsPromotions = false;
                    model.AllowVoiceReminders = false;
                    model.AllowVoiceBirthday = false;
                    model.AllowVoiceThankYou = false;
                    model.AllowVoiceEyewear = false;
                    model.AllowVoicePromotions = false;
                    model.InvitedToPortal = true;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }

        private CDAModel SetValueIntoNewCDAObject(DataRow dtRow)
        {
            CDAModel objCDAModel = new CDAModel();
            try
            {
                if (dtRow != null)
                {
                    objCDAModel.ExternalId = dtRow["ExternalId"].ToString();
                    objCDAModel.CDAMessage.PatientExternalId = dtRow["PatientExternalId"].ToString();
                    objCDAModel.CDAMessage.LocationExternalId = dtRow["LocationExternalId"].ToString();
                    objCDAModel.CDAMessage.CDAXml = dtRow["CDAXml"].ToString();
                    objCDAModel.CDAMessage.Type = "AmbulatorySummary";
                    objCDAModel.CDAMessage.DoctorExternalId = dtRow["DoctorExternalId"].ToString();
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Error while reading data from SP - " + ex.Message, EventLogEntryType.Error);
                objCDAModel = null;
                throw ex;
            }
            return objCDAModel;
        }

        public List<MarkAsReceivedModel> getPatientModelForUpdation()
        {
            List<MarkAsReceivedModel> list = new List<MarkAsReceivedModel>();
            MarkAsReceivedModel model = null;
            string procedureName = "MVE.PP_GetPatientUpdateList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoPatientUpdateObject(dr);
                        if (model != null)
                        {
                            list.Add(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("PatientFacade", "getPatientModelForUpdation", procedureName, ex.Message);
            }
            return list;
        }
        private MarkAsReceivedModel SetValueIntoPatientUpdateObject(DataRow dtRow)
        {
            MarkAsReceivedModel model = new MarkAsReceivedModel();
            try
            {
                if (dtRow != null)
                {
                    model.Success = dtRow["Active"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["Success"]);
                    model.ResultMessage = dtRow["ResultMessage"] == DBNull.Value ? "" : dtRow["ResultMessage"].ToString();
                    if (dtRow["InternalID"] != null)
                        model.InternalID = (Guid)dtRow["InternalID"];
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
