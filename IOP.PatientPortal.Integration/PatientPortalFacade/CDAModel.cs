﻿using GlobalPortal.Api.Models;
using GlobalPortal.Api.Models.Hl7CDAs;

namespace PatientPortalFacade
{
    public class CDAModel : ExternalModel
    {
        public Hl7CDAModel CDAMessage = new Hl7CDAModel();
    }
}
