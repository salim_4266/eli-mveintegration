﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.Locations;
using GlobalPortal.Api.Models.Contacts;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PatientPortalFacade
{
    public class LocationsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(LocationsFacade));
        public List<LocationModel>  GetLocationModel()
        {
            List<LocationModel> List = new List<LocationModel>();
            LocationModel model = null;
            string procedureName = "MVE.PP_GetLocationDetails";
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ActionId", Convert.ToInt32(EnumTypes.ActionTypeLookUps.SendLocationInformation));
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoLocationsObject(dr);
                        List.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("LocationsFacade", "PostPatientAppointments", procedureName, ex.Message);
            }
            return List;
        }
        public List<LocationModel> getDeleteLocation()
        {
            List<LocationModel> List = new List<LocationModel>();
            LocationModel model = null;
            string procedureName = "pp_GetLocationDeleteList";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoLocationsObject(dr);
                        List.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("LocationsFacade", "getDeleteLocation", procedureName, ex.Message);
            }
            return List;
        }
        private LocationModel SetValueIntoLocationsObject(DataRow dtRow)
        {
            LocationModel model = new LocationModel();
            try
            {
                if (dtRow != null)
                {
                    ContactModel objContact = new ContactModel();
                    objContact.FirstName = dtRow["ContactName"] == DBNull.Value ? "" : dtRow["ContactName"].ToString();
                    objContact.LastName = "";
                    objContact.Id = Guid.NewGuid();
                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.Active = Convert.ToBoolean(dtRow["Active"]);
                    model.ExternalId = dtRow["ExternalId"].ToString();
                    model.Contact = objContact;
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
