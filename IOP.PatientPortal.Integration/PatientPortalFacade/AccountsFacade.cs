﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.Accounts;
using GlobalPortal.Api.Models.AccountSettings;
using log4net;
using System;
using System.Data;
using System.Data.SqlClient;

namespace PatientPortalFacade
{
    public class AccountsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(AccountsFacade));
        public NewAccountModel getNewAccountModel()
        {
            NewAccountModel model = new NewAccountModel();
            string procedureName = "MVE.PP_GetAccountDetails";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoNewAccountObject(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AccountsFacade", "getNewAccountModel", procedureName, ex.Message);
            }
            return model;
        }
        private NewAccountModel SetValueIntoNewAccountObject(DataRow dtRow)
        {
            NewAccountModel model = new NewAccountModel();
            try
            {
                if (dtRow != null)
                {
                    model.AccountName = dtRow["AccountName"] == DBNull.Value ? "" : dtRow["AccountName"].ToString();
                    model.AccountOwnerExternalId = dtRow["AccountOwnerExternalId"] == DBNull.Value ? "" : dtRow["AccountOwnerExternalId"].ToString();
                    model.AccountOwnerFirstName = dtRow["AccountOwnerFirstName"] == DBNull.Value ? "" : dtRow["AccountOwnerFirstName"].ToString();
                    model.AccountOwnerMiddleName = dtRow["AccountOwnerMiddleName"] == DBNull.Value ? "" : dtRow["AccountOwnerMiddleName"].ToString();
                    model.AccountOwnerLastName = dtRow["AccountOwnerLastName"] == DBNull.Value ? "" : dtRow["AccountOwnerLastName"].ToString();
                    model.Username = dtRow["Username"] == DBNull.Value ? "" : dtRow["Username"].ToString();
                    model.Password = dtRow["Password"] == DBNull.Value ? "" : dtRow["Password"].ToString();
                    model.AccountOwnerEmailAddress = dtRow["AccountOwnerEmailAddress"] == DBNull.Value ? "" : dtRow["AccountOwnerEmailAddress"].ToString();
                    model.PartnerKey = dtRow["PartnerKey"] == DBNull.Value ? "" : dtRow["PartnerKey"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        private NewAccountResultModel SetValueIntoNewAccountResultObject(DataRow dtRow)
        {
            NewAccountResultModel model = new NewAccountResultModel();
            try
            {
                if (dtRow != null)
                {
                    model.AccountNumber = dtRow["AccountNumber"] == DBNull.Value ? "" : dtRow["AccountNumber"].ToString();
                    model.SynchronizationUserName = dtRow["SynchronizationUserName"] == DBNull.Value ? "" : dtRow["SynchronizationUserName"].ToString();
                    model.SynchronizationPassword = dtRow["SynchronizationPassword"] == DBNull.Value ? "" : dtRow["SynchronizationPassword"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
        public void updateNewAccountDetails(NewAccountModel model, NewAccountResultModel result)
        {
            try
            {
                int status;
                string query = "MVE.PP_UpdateAccountDetails";
                SqlParameter[] parameter = new SqlParameter[5];
                int columnIndex = 0;
                parameter[columnIndex] = new SqlParameter("@AccountId", result.AccountId);
                parameter[++columnIndex] = new SqlParameter("@AccountNumber", result.AccountNumber);
                parameter[++columnIndex] = new SqlParameter("@SynchronizationUserName", result.SynchronizationUserName);
                parameter[++columnIndex] = new SqlParameter("@SynchronizationPassword", result.SynchronizationPassword);
                parameter[++columnIndex] = new SqlParameter("@IsActive", 1);
                status = DataAccessObjects.ExecuteInsertUpdateCommand(query, System.Data.CommandType.StoredProcedure, parameter);
            }
            catch { }
        }
        public AccountSettingsModel getAccountSettingsModel()
        {
            AccountSettingsModel model = new AccountSettingsModel();
            string procedureName = "MVE.PP_Getaccountdetails";
            try
            {
                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAccountsSettingObject(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AccountsFacade", "getAccountSettingsModel", procedureName, ex.Message);
            }
            return model;
        }
        private AccountSettingsModel SetValueIntoAccountsSettingObject(DataRow dtRow)
        {
            AccountSettingsModel model = new AccountSettingsModel();
            try
            {
                if (dtRow != null)
                {
                    model.AccountId = Guid.Parse(dtRow["AccountId"] == DBNull.Value ? "" : dtRow["AccountId"].ToString());
                    model.LinkedInUrl = dtRow["LinkedInUrl"] == DBNull.Value ? "" : dtRow["LinkedInUrl"].ToString();
                    model.YahooUrl = dtRow["YahooUrl"] == DBNull.Value ? "" : dtRow["YahooUrl"].ToString();
                    model.GooglePlusUrl = dtRow["GooglePlusUrl"] == DBNull.Value ? "" : dtRow["GooglePlusUrl"].ToString();
                    model.FacebookUrl = dtRow["FacebookUrl"] == DBNull.Value ? "" : dtRow["FacebookUrl"].ToString();
                    //if (dtRow["AppointmentAllowedTimeInAdvance"]!=null)
                    //model.AppointmentAllowedTimeInAdvance = (TimeSpan)dtRow["AppointmentAllowedTimeInAdvance"];
                    model.UsesForms = dtRow["UsesForms"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["UsesForms"].ToString());
                    model.OnlineAppointmentsActive = dtRow["OnlineAppointmentsActive"] == DBNull.Value ? false : Convert.ToBoolean(dtRow["OnlineAppointmentsActive"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
