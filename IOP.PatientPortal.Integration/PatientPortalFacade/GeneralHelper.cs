﻿using System.Collections.Generic;
using System.Configuration;

namespace PatientPortalFacade
{
    public class GeneralHelper
    {
        /// <summary>
        /// returns App Settings value for a key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key]!=null ? ConfigurationManager.AppSettings[key].ToString() : string.Empty;
        }

        /// <summary>
        /// retruns Connection String value for a key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ToString();
        }


        /// <summary>
        /// split a phone number of US format in it's code and number
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static Dictionary<string, string> SplitPhoneNumber(string phone)
        {
            Dictionary<string, string> phoneNum = null;

            try
            {
                if (phone.Length == 14 && phone[0] == '(' && phone[4] == ')')
                {
                    phoneNum = new Dictionary<string, string>();
                    var key = phone.Substring(1, 3);
                    var value = phone.Substring(6, 8);
                    phoneNum.Add(key, value);
                }
            }
            catch
            {

            }
            return phoneNum;
        }
    }
}

