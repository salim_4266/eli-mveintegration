﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;
using PatientPortalFacade;
using GlobalPortal.Api.Models.AppointmentStatuses;
using GlobalPortal.Api.Models.AppointmentsModel;
using GlobalPortal.Api.Models.OrderStatuses;
using GlobalPortal.Api.Models.Orders;
using GlobalPortal.Api.Models.PatientCommunications;
using GlobalPortal.Api.Models.MarkAsReceived;

using GlobalPortal.Api.Client.AppointmentStatuses;
using GlobalPortal.Api.Client.Appointments;
using GlobalPortal.Api.Client.Orders;
using GlobalPortal.Api.Client.OrderStatuses;
using GlobalPortal.Api.Client.PatientCommunications;
using System.Data.SqlClient;
using System.Data;
using AppointmentAPI.DAO;


namespace PatientPortalFacade
{
    public class PatientcommunicationsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(PatientcommunicationsFacade));


       

   

        #region OrderStatuses
        #endregion

        #region Orders
        #endregion

        #region PatientCommunications
        #endregion

        #region CommunicationUpdatesSent
        #endregion

        #region CommunicationUpdatesAppointments
        #endregion

        #region AppointmentUpdatesSent
        #endregion

        #region CommunicationUpdatesOrders
        #endregion

        #region OrderUpdatesSent
        #endregion

        #region CommunicationUpdatesPatients
        #endregion

        #region PatientUpdatesSent
        #endregion


        #region OneOffMessages
        #endregion


    }
}
