﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.DosageForms;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientPortalFacade
{
    public class DosageFormsFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DosageFormsFacade));
        public List<DosageFormModel> GetDosageFormModel()
        {

            List<DosageFormModel> list = new List<DosageFormModel>();
            DosageFormModel model = null;
            string procedureName = "pp_getDosageFormdetails";
            try
            {

                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoDosageFormObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("DosageFormsFacade", "GetDosageFormModel", procedureName, ex.Message);
            }
            return list;
        }
        private DosageFormModel SetValueIntoDosageFormObject(DataRow dtRow)
        {
            DosageFormModel model = new DosageFormModel();

            try
            {

                if (dtRow != null)
                {


                    model.Name = dtRow["Name"] == DBNull.Value ? "" : dtRow["Name"].ToString();
                    model.SortOrder = Convert.ToInt32(dtRow["SortOrder"].ToString());
                    model.ExternalId = dtRow["ExternalId"] == DBNull.Value ? "" : dtRow["ExternalId"].ToString();
                   
                }

            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
