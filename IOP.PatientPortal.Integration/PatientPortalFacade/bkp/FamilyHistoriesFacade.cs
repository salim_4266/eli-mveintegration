﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.FamilyHistories;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientPortalFacade
{
    public class FamilyHistoriesFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(FamilyHistoriesFacade));
        public List<FamilyHistoryModel> GetFamilyHistoryModel()
        {
            List<FamilyHistoryModel> list = new List<FamilyHistoryModel>();
            FamilyHistoryModel model = null;
            string procedureName = "pp_getFamilyHistorydetails";
            try
            {

                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoFamilyHistoryObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("FamilyHistoriesFacade", "GetFamilyHistoryModel", procedureName, ex.Message);
            }
            return list;
        }
        private FamilyHistoryModel SetValueIntoFamilyHistoryObject(DataRow dtRow)
        {
            FamilyHistoryModel model = new FamilyHistoryModel();
            try
            {

                if (dtRow != null)
                {
                    model.Disease = dtRow["value"] == DBNull.Value ? "" : dtRow["value"].ToString();
                    model.SortOrder = Convert.ToInt32(dtRow["Order"].ToString());
                    model.ExternalId = dtRow["value"] == DBNull.Value ? "" : dtRow["value"].ToString();
                    model.IsDefault = true;
                }

            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
