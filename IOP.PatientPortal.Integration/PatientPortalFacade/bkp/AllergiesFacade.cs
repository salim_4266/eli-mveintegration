﻿using AppointmentAPI.DAO;
using GlobalPortal.Api.Models.Allergies;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientPortalFacade
{
    public class AllergiesFacade
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(AllergiesFacade));

        public List<AllergyModel> GetAllergyModel()
        {
            List<AllergyModel> list = new List<AllergyModel>();
            AllergyModel model = null;
            string procedureName = "pp_getAllergydetails";
            try
            {

                SqlParameter[] parameters = null;
                DataSet ds = DataAccessObjects.GetDataSetFromDataReader(procedureName, CommandType.StoredProcedure, parameters);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = SetValueIntoAllergyObject(dr);
                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Utility.saveErrorIntoTable("AllergiesFacade", "GetAllergyModel", procedureName, ex.Message);
            }
            return list;
        }
        private AllergyModel SetValueIntoAllergyObject(DataRow dtRow)
        {
            AllergyModel model = new AllergyModel();

            try
            {

                if (dtRow != null)
                {
                    model.Allergy = dtRow["name"] == DBNull.Value ? "" : dtRow["name"].ToString();
                    model.Type = dtRow["type"] == DBNull.Value ? "" : dtRow["type"].ToString();
                    model.Reaction = dtRow["reaction"] == DBNull.Value ? "" : dtRow["reaction"].ToString();
                    model.ExternalId = dtRow["id"] == DBNull.Value ? "" : dtRow["id"].ToString();
                }

            }
            catch (Exception ex)
            {
                model = null;
                throw ex;
            }
            return model;
        }
    }
}
