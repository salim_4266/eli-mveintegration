﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PatientPortalFacade
{
    public static class ExtensionMethods
    {

        /// <summary>
        /// truncate string to specified length
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }


        /// <summary>
        /// finds or extracts numbers from a string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetNumericValue(this string value)
        {
            //Source: http://stackoverflow.com/questions/4734116/find-and-extract-numbers-from-a-string

            string resultString = Regex.Match(value, @"\d+").Value;
            return Int32.Parse(resultString);
        }

        /// <summary>
        /// convert string of format M/d/yyyy h:mm:ss tt to Date object
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static DateTime ToMMDDYYYYHHMMSSTT(this string dateString)
        {
            return DateTime.ParseExact(dateString, "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"));
        }

        /// <summary>
        ///  convert string of format M/d/yyyy  to Date object
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static DateTime ToMMDDYYYY(this string dateString)
        {
            return DateTime.ParseExact(dateString, "M/d/yyyy", new CultureInfo("en-US"));
        }

        // Note:
        //  Timezone Id values supported by .net 
        //  http://stackoverflow.com/questions/7908343/list-of-timezone-ids-for-use-with-findtimezonebyid-in-c#answer-7908482

        /// <summary>
        /// convert given time to local time assuming that given time is in different time zone (specified in app.config)
        /// </summary>
        /// <param name="kareoServerTime"></param>
        /// <returns></returns>
        //public static DateTime KareoToLocalTime(this DateTime kareoServerTime)
        //{          
        //    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(kareoServerTime, ConfigHelper.KareoApiTimeZone, ConfigHelper.CustomerTimeZone);
        //}

        
    }
}
