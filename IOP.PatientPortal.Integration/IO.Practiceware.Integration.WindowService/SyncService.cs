﻿using System;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using PatientPortalFacade;
using PatientPortalMain;
using System.Configuration;
using AppointmentAPI.DAO;
using RCPServices;

namespace IO.Practiceware.Integration.WindowService
{
    public partial class SyncService : ServiceBase
    {
        System.Timers.Timer timer;

        public SyncService()
        {
            InitializeComponent();
            this.AutoLog = false;
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            EventLogger.Instance.WriteLog("Sync Service Started.", EventLogEntryType.Information);
            timer = new System.Timers.Timer();
            timer.Interval = 5000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Enabled = true;
            timer.Start();
        }

        private void OnTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                var intervalMode = ConfigHelper.IntervalMode != null ? ConfigHelper.IntervalMode.ToString() : "Daily";
                timer.Enabled = false;
                if (intervalMode.ToLower() == "daily")
                {
                    StartSyncronization();
                }
                else
                {
                    StartSyncronization();
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Failed to Start the Service " + ex.Message, EventLogEntryType.Error);
            }
            finally
            {
                timer.Enabled = true;
            }
        }

        public void StartSyncronization()
        {
            int practiceCount = ConfigurationManager.ConnectionStrings.Count;
            int CurrentMinute = DateTime.Now.Minute;

            var currentDateTime = DateTime.Now;
            DateTime scheduledStartTime = Convert.ToDateTime(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + ConfigHelper.MedFlowScheduledTime);
            DateTime scheduledEndTime = Convert.ToDateTime(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + ConfigHelper.MedFlowScheduledEndTime);
            if (scheduledStartTime <= currentDateTime && scheduledEndTime >= currentDateTime)
            {
                if (ConfigHelper.ACIServices.ToLower() == "true")
                {
                    for (int practicenumber = 1; practicenumber < practiceCount; practicenumber++)
                    {
                        string _pathDirectory = ConfigurationManager.AppSettings["FolderPath"].ToString();
                        RCPUtility.connectionString = ConfigurationManager.ConnectionStrings[practicenumber].ConnectionString;
                        RCPClient objService = new RCPClient();
                        int counter = 1;
                        while (counter <= 10)
                        {
                            counter++;
                            objService.PushBatch();
                            System.Threading.Thread.Sleep(5000);
                        }
                        objService.PullBatch();
                    }
                }

                if (ConfigHelper.ERPServices.ToLower() == "true")
                {
                    EventLogger.Instance.WriteLog("ERP Service start", EventLogEntryType.Information);
                    for (int practicenumber = 1; practicenumber < practiceCount; practicenumber++)
                    {
                        string lcconnstring = ConfigurationManager.ConnectionStrings[practicenumber].ConnectionString;
                        DataAccessObjects.SetConnectionString(lcconnstring);

                        string VersionInfo = string.Empty;
                        string PracticeOnHL7 = string.Empty;
                        string slctQry = "Select Value, Name from Model.ApplicationSettings with(nolock) Where Name in ('PracticeVersion', 'PracticeOnHL7')";
                        DataTable dtable = DataAccessObjects.GetDataTableFromDataReader(slctQry, System.Data.CommandType.Text, null);

                        if (dtable.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtable.Rows)
                            {
                                if (dr["Name"].ToString().ToLower() == "practiceversion")
                                {
                                    VersionInfo = dr["Value"].ToString().ToLower();
                                }
                                if (dr["Name"].ToString().ToLower() == "practiceonhl7")
                                {
                                    PracticeOnHL7 = dr["Value"].ToString().ToLower();
                                }
                            }
                        }
                        if (Helpers.GetSyncUserDetails(lcconnstring))
                        {
                            StartERPIntegration(VersionInfo, PracticeOnHL7);
                        }
                    }
                    EventLogger.Instance.WriteLog("ERP Service end", EventLogEntryType.Information);
                }
            }
        }

        private void StartERPIntegration(string VersionInfo, string PracticeOnHL7)
        {
            EventLogger.Instance.WriteLog("ERP Account Sync Event Started", EventLogEntryType.Information);
            new Accounts().sendAccountInformation();
            EventLogger.Instance.WriteLog("ERP Account Sync Event Started", EventLogEntryType.Information);
            System.Threading.Thread.Sleep(3000);
            EventLogger.Instance.WriteLog("Secure Message Log Information Sync Event Started", EventLogEntryType.Information);
            new MeaningFullData().GetSecureMessagesLogs();
            EventLogger.Instance.WriteLog("Secure Message Log Information Sync Event Completed", EventLogEntryType.Information);
            System.Threading.Thread.Sleep(3000);
            EventLogger.Instance.WriteLog("Locations Sync Event Started", EventLogEntryType.Information);
            new Locations().sendLocationInformation();
            EventLogger.Instance.WriteLog("Locations Sync Event Completed", EventLogEntryType.Information);
            System.Threading.Thread.Sleep(3000);
            if (VersionInfo != "" && PracticeOnHL7 != "")
            {
                if (PracticeOnHL7.ToLower() == "yes")
                {
                    EventLogger.Instance.WriteLog("Patient Information Sync Event Started", EventLogEntryType.Information);
                    new Patients().sendPatientInformation();
                    EventLogger.Instance.WriteLog("Patient Information Sync Event Completed", EventLogEntryType.Information);
                }
                System.Threading.Thread.Sleep(3000);
                if (VersionInfo == "2014")
                {
                    EventLogger.Instance.WriteLog("Patient Health Information Sync Event Started", EventLogEntryType.Information);
                    new Patients().sendPatientHealthInformation();
                    EventLogger.Instance.WriteLog("Patient Health Information Sync Event Completed", EventLogEntryType.Information);
                }
            }
            else
            {
                EventLogger.Instance.WriteLog("There is some issue while reading practice version detail from Database", EventLogEntryType.Error);
            }
        }
    }
}
